/********************************************************************************
*                                                                               *
* kWookaTypedResourceInfo.hpp -- some inner resources                           *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaTypedResourceInfo_hpp
#define kWookaTypedResourceInfo_hpp

#include <stdio.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "kWookaProjectInfo.hpp"

bool DirectoryExist(const wxString& fileName, bool mkdir = false);

bool FileExists(const wxString& fileName, bool mkdir = true);

bool DirectoryRemove(const wxString& dir, bool recur = false);

/*
 * According the AppItem to write the nginx config 
 */
wxString WriteNginxConfig(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem);

/*
 * According the AppItem to write the nginx service config 
 */
wxString WriteNginxServiceConfig(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem);

wxString WriteRedisConfig(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem);

wxString WriteMySQLConfig(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem);

/*
 * According the AppItem to format the mavnen command 
 */
wxString FormatMavenCommand(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem);

#endif
