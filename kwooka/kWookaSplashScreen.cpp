/********************************************************************************
*                                                                               *
* kPosSplashScreen.hpp --       SplashScreen                                    *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#include "kWookaSplashScreen.hpp"
#include "kWooka.h"
#include "kWooka_perferences.hpp"
#include "kWooka_frame.hpp"
#include <wx/gdicmn.h>
#include <wx/xrc/xmlres.h>
#include <wx/xrc/xh_all.h>
#include "enum_process.hpp"
#include "resource.h"


BEGIN_EVENT_TABLE(kWookaSplashScreenFrame, wxFrame)
	EVT_BUTTON(ID_COMMAND_INIT_OK, kWookaSplashScreenFrame::OnInitializedCompleted)
    EVT_PAINT(kWookaSplashScreenFrame::OnPaint)
#ifdef __WXGTK__
    EVT_WINDOW_CREATE(kPosSplashScreenFrame::OnWindowCreate)
#endif
END_EVENT_TABLE()


kWookaSplashScreenFrame::kWookaSplashScreenFrame()
    : wxFrame((wxFrame*)NULL, wxID_ANY, wxEmptyString,wxDefaultPosition,wxSize(250,300),\
              wxFRAME_SHAPED | wxSIMPLE_BORDER | wxFRAME_NO_TASKBAR )
{
    //wxImage::AddHandler(new wxPNGHandler);
    // wxXmlResource::Get()->load;
    m_hasShape = false;
#ifdef WIN32
    
    m_bmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_SLASH_SCREEN"));
    //m_bmp.LoadFile(wxT(".\\kpos_splash.png"),wxBITMAP_TYPE_PNG);
	// m_bmp.LoadFile(wxT("images\\kpos_splash.jpg"), wxBITMAP_TYPE_JPEG);
    // m_bmp = mm;
        
#else
    //m_bmp.LoadFile(wxT("./kpos_splash.png"),wxBITMAP_TYPE_PNG);
    m_bmp.LoadFile(wxT("images/kpos_splash.jpg"),wxBITMAP_TYPE_JPEG);
    //m_bmp.LoadFile(wxT("./kpos_splash1.bmp"),wxBITMAP_TYPE_BMP);
    //m_bmp.SaveFile(wxT("./kpos_splash2.png"),wxBITMAP_TYPE_PNG);
#endif
    if(!m_bmp.Ok()){
        wxMessageBox(_("sorry, could not load file."));
    }
    wxSize sz = wxSize(m_bmp.GetWidth(),m_bmp.GetHeight());
    SetSize(sz);
    //SetBackgroundColour(wxColour(0, 0, 0, 0));
    SetWindowShape();
    Center();
    Refresh();
    kWookaInitialModuleTask* task = new kWookaInitialModuleTask(this);
	task->Run();
}

kWookaSplashScreenFrame::~kWookaSplashScreenFrame()
{
    wxLogDebug(wxT("kPosSplashScreenFrame was destroyed."));
}


#ifdef __WXGTK__
void kPosSplashScreenFrame::OnWindowCreate(wxWindowCreateEvent& event){
    SetWindowShape();
}
#endif

void kWookaSplashScreenFrame::SetWindowShape(){
    wxRegion region(m_bmp, wxColour(0,0,0,0));
    m_hasShape = SetShape(region);
}

void kWookaSplashScreenFrame::OnPaint(wxPaintEvent& WXUNUSED(event)){
    wxPaintDC dc(this);
    //m_bmp.UseAlpha(false);
    if (m_bmp.HasAlpha()){
        wxImage image = m_bmp.ConvertToImage();
        image.InitAlpha();
        dc.DrawBitmap(wxBitmap(image), 0, 0);
    } else {
        wxImage image = m_bmp.ConvertToImage();
        image.InitAlpha();
        dc.DrawBitmap(wxBitmap(image), 0, 0);
        //dc.DrawBitmap(m_bmp, 0, 0);
    }
    
    //dc.Blit(0, 0, m_bmp.GetWidth(), m_bmp.GetHeight(), &mdc, 0, 0);
    
    //dc.DrawRotatedText(_("Good morning, people, stay and flash"), 100, 20, 270.0);
}

void kWookaSplashScreenFrame::OnInitializedCompleted(wxCommandEvent& WXUNUSED(event)) {
	// kPosShapeFrame* shape = new kPosShapeFrame();
	// shape->Show(true)
    wxGetApp().ShowMainFrame();
	Close(true);
}


kWookaInitialModuleTask::kWookaInitialModuleTask(wxEvtHandler* hh) : wxThread(wxTHREAD_DETACHED) {
	m_handler = hh;
}

kWookaInitialModuleTask::~kWookaInitialModuleTask() {
    wxLogDebug(wxT("kPosInitialModuleTask was finished and destroy."));
}

wxThread::ExitCode kWookaInitialModuleTask::Entry() {
	wxThread::Sleep(500);
#ifdef WIN32
	InitialHookModule();
#endif
	if (m_handler != NULL) {
		wxCommandEvent newVersionEvent(wxEVT_BUTTON, kWookaSplashScreenFrame::ID_COMMAND_INIT_OK);
		m_handler->AddPendingEvent(newVersionEvent);
	}
	return (wxThread::ExitCode) 0;
}
