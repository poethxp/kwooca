/********************************************************************************
*                                                                               *
* kwooka_perferences.hpp --       Some tools for perferences function           *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kwooka_perferences_hpp
#define kwooka_perferences_hpp

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include <list>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>


#include "wx/preferences.h"

#include "wx/config.h"
#include "wx/panel.h"
#include "wx/statbox.h"
#include "wx/scopedptr.h"
#include "wx/menu.h"
#include "wx/checkbox.h"
#include "wx/listbox.h"
#include "wx/stattext.h"
#include "wx/sizer.h"
#include "wx/artprov.h"
#include "wx/textctrl.h"
#include "wx/choice.h"
#include "wx/thread.h"
#include "wx/msgdlg.h"
#include "wx/colordlg.h"
#include "wx/bmpbuttn.h"
#include <wx/button.h>
#include "kWooka_ids.h"
#include "wx/combobox.h"
#include "kWooka_perferences_utils.hpp"
#include "kWookaApplicationJavaVMPanel.hpp"
#include "kWookaProjectInfo.hpp"

class PrefsPageGeneralPanel : public wxPanel
{
private:
    kWookaJvmConfigInfo* m_jvmconf;
public:
    PrefsPageGeneralPanel(wxWindow *parent, kWookaJvmConfigInfo* cfg) : wxPanel(parent)
    {
        m_jvmconf = cfg;
        wxStaticBox* kPluginBox = new wxStaticBox(this, wxID_ANY, _(""), wxDefaultPosition, wxSize(480, 100));
        wxSizer *sizerPanel = new wxBoxSizer(wxVERTICAL);
        wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        wxSizer* sizer3 = new wxBoxSizer(wxVERTICAL);
        wxSizer *sizer2 = new wxBoxSizer(wxHORIZONTAL);
        
        m_btnPosBinPath = new wxButton(kPluginBox, wxID_ANY, _("Browse"), wxDefaultPosition, wxSize(60, 20), 0);
        
        wxStaticText* lblListBinns = new wxStaticText(kPluginBox, wxID_STATIC, _("JDK Home:"), wxDefaultPosition, wxSize(80, 20), 0 );
        
        m_cmbProcess = new wxComboBox(kPluginBox, wxID_ANY);
		
        sizer2->Add(lblListBinns, 0, wxEXPAND | wxALL, 0);
        sizer2->Add(m_cmbProcess, 1, wxEXPAND | wxALL, 0);
        sizer2->Add(m_btnPosBinPath, 0, wxEXPAND | wxALL, 0);

        sizer->Add(sizer2, 0, wxEXPAND | wxALL, 20);
        kPluginBox->SetSizerAndFit(sizer);
        
        wxStaticBox* kPluginJvmBox = new wxStaticBox(this, wxID_ANY, _(""), wxDefaultPosition, wxSize(400, 100));
        kWookaApplicationJavaVMPanel* vmpanel = new kWookaApplicationJavaVMPanel(kPluginJvmBox, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(400, 20));
        vmpanel->UpdateJvmConfig(m_jvmconf);
        m_cmbProcess->SetValue(m_jvmconf->jdkHome);
        m_jvmPanel = vmpanel;
        sizer3->Add(vmpanel, 0, wxEXPAND | wxALL, 20);
        kPluginJvmBox->SetSizerAndFit(sizer3);
        //sizer->Add(itemProductBox, wxSizerFlags().Border().Expand());
        
        //wxSizer *hsizer = new wxBoxSizer(wxHORIZONTAL);
        //itemProductBox->SetSizer(hsizer);
        //hsizer->Add(m_threads,  wxSizerFlags().Border());
        //hsizer->Add(itemStaticText1,  wxSizerFlags().Border());
        
       
        sizerPanel->Add(kPluginBox, 0, wxEXPAND | wxALL, 5);
        sizerPanel->Add(kPluginJvmBox, 0, wxEXPAND | wxALL, 5);
        
        SetSizerAndFit(sizerPanel);
        
        // On some platforms (OS X, GNOME), changes to preferences are applied
        // immediately rather than after the OK or Apply button is pressed.
        if ( wxPreferencesEditor::ShouldApplyChangesImmediately() )
        {
            
            m_btnPosBinPath->Connect(wxEVT_BUTTON,
                                       wxCommandEventHandler(PrefsPageGeneralPanel::OnBrowseButtonClick),
                                       NULL, this);
            
            //m_cmbProcess->Connect(wxEVT_COMBOBOX,
            //                           wxCommandEventHandler(PrefsPageGeneralPanel::OnSelectProcessClick),
            //                           NULL, this);
        }
    }
    
    virtual bool TransferDataToWindow()
    {
        wxArrayString items = GetJavaHomeList();// kPosGetProcessList();
        m_cmbProcess->Set(items);
        m_cmbProcess->Disconnect(wxEVT_COMBOBOX, wxCommandEventHandler(PrefsPageGeneralPanel::OnSelectProcessClick));
        m_cmbProcess->SetValue(m_jvmconf->jdkHome);
        m_cmbProcess->Connect(wxEVT_COMBOBOX,
                                   wxCommandEventHandler(PrefsPageGeneralPanel::OnSelectProcessClick),
                                   NULL, this);
        
        m_jvmPanel->UpdateJvmConfig(m_jvmconf);

        m_btnPosBinPath->Connect(wxEVT_BUTTON,
            wxCommandEventHandler(PrefsPageGeneralPanel::OnBrowseButtonClick),
            NULL, this);

        return true;
    }
    
    virtual bool TransferDataFromWindow()
    {
        // Called on platforms with modal preferences dialog to save and apply
        // the changes.
        wxCommandEvent dummy;
        OnSelectProcessClick(dummy);
        return true;
    }
    
    bool UpdateDataFromWindow()
    {
        // Called on platforms with modal preferences dialog to save and apply
        // the changes.
        return true;
    }
    
    
private:

    void OnSelectProcessClick(wxCommandEvent& WXUNUSED(e)){
        wxString plugFullName = m_cmbProcess->GetValue();
        wxString plugFileName = "";

#ifdef __WXWIN32__
        plugFileName = plugFullName.AfterLast('\\');
#else
        plugFileName = plugFullName.AfterLast('/');
#endif
        
        // kPosSetPluggedInExecutableFile(plugFullName, plugFileName);
        this->m_jvmconf->jdkHome = plugFullName;
    }
    
    void OnBrowseButtonClick(wxCommandEvent& WXUNUSED(e)){
        m_cmbProcess->Disconnect(wxEVT_COMBOBOX, wxCommandEventHandler(PrefsPageGeneralPanel::OnSelectProcessClick));
        wxDirDialog  dirdlg(this, _("Select a JDK Home directory"));
        if (dirdlg.ShowModal() == wxID_OK) {
            wxString filePath = dirdlg.GetPath();
            m_cmbProcess->SetValue(filePath);
            UpdateDataFromWindow();
        }
        m_cmbProcess->Connect(wxEVT_COMBOBOX,
            wxCommandEventHandler(PrefsPageGeneralPanel::OnSelectProcessClick),
            NULL, this);
    }
    
    wxString    m_pluginShortName;
    wxString    m_pluginFullpath;
    
    wxButton*   m_btnPosBinPath;
    wxComboBox* m_cmbProcess;
    kWookaApplicationJavaVMPanel* m_jvmPanel;
};

class PrefsPageGeneral : public wxStockPreferencesPage
{
private:
    kWookaJvmConfigInfo* m_jvmconf;
public:
    PrefsPageGeneral(kWookaJvmConfigInfo* jvmConfig) : wxStockPreferencesPage(Kind_General) {
        m_jvmconf = jvmConfig;
    }
public:
    // PrefsPageGeneral() : wxStockPreferencesPage(Kind_General) {}
    virtual wxString GetName() const { return _("Java Home"); }
    virtual wxWindow *CreateWindow(wxWindow *parent)
    { return new PrefsPageGeneralPanel(parent, m_jvmconf); }
};

class PrefsPageOperatorPanel : public wxPanel
{
private:
    kWookaAppConfigArray* m_appconfs;
public:
    PrefsPageOperatorPanel(wxWindow *parent, kWookaAppConfigArray* cfgs) : wxPanel(parent)
    {
        m_appconfs = cfgs;
        //wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* sizer1 = new wxBoxSizer(wxHORIZONTAL);
        wxBoxSizer* sizer2 = new wxBoxSizer(wxHORIZONTAL);
        wxBoxSizer* sizer3 = new wxBoxSizer(wxHORIZONTAL);
        
        m_lblOperatorName = new wxStaticText(this, wxID_ANY, _("Config Key:"));
        m_lblOperatorPhone = new wxStaticText(this, wxID_ANY, _("Value:"));
        m_lblEnvVariable = new wxStaticText(this, wxID_ANY, _("Env Variable:"));
        m_txtOperatorName  = new wxTextCtrl(this, wxID_ANY, _T(""));
        m_txtOperatorPhone  = new wxTextCtrl(this, wxID_ANY, _T(""));
        m_chkEnvVariable = new wxCheckBox(this, wxID_ANY, _T(""));
        m_gridOperators = new wxGrid(this, wxID_ANY, wxDefaultPosition, wxSize(460, 340));
        wxPanel* spacePanel = new wxPanel(this, wxID_ANY);
        wxPanel* spacePanel2 = new wxPanel(this, wxID_ANY);
        m_saveOperatorBtn = new wxButton(this, wxID_ANY, _("Save"), wxDefaultPosition, wxSize(120, 20));
        m_delOperatorBtn = new wxButton(this, wxID_ANY, _("Delete"), wxDefaultPosition, wxSize(120, 20));
        
        sizer1->Add(m_lblOperatorName, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_txtOperatorName, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_lblOperatorPhone, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_txtOperatorPhone, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_lblEnvVariable, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_chkEnvVariable, 1, wxEXPAND | wxALL, 5);
        
        sizer->Add(sizer1, 0, wxEXPAND | wxALL, 5);
        sizer2->Add(spacePanel, 1, wxEXPAND | wxALL, 5);
        sizer2->Add(m_saveOperatorBtn, 0, wxEXPAND | wxALL, 5);
        sizer3->Add(spacePanel2, 1, wxEXPAND | wxALL, 5);
        sizer3->Add(m_delOperatorBtn, 0, wxEXPAND | wxALL, 5);
        sizer->Add(sizer2, 0, wxEXPAND | wxALL, 5);
        sizer->Add(m_gridOperators, 0, wxEXPAND | wxALL, 5);
        sizer->Add(sizer3, 0, wxEXPAND | wxALL, 5);
        
        m_gridOperators->SetDefaultColSize(100);
        m_gridOperators->SetDefaultRowSize(25);
        m_gridOperators->SetRowLabelSize(50);
        m_gridOperators->SetColLabelSize(25);
        m_gridOperators->CreateGrid(0, 3, wxGrid::wxGridSelectRows);
        m_gridOperators->SetColLabelValue(0, _("CONFIG KEY"));
        m_gridOperators->SetColLabelValue(1, _("CONFIG VALUE"));
        m_gridOperators->SetColLabelValue(2, _("ENV VARIABLE"));
        
        m_gridOperators->SetColSize(0, 240);
        m_gridOperators->SetColSize(1, 240);
        m_gridOperators->SetColSize(2, 120);

        
        SetSizerAndFit(sizer);
        
        //if ( wxPreferencesEditor::ShouldApplyChangesImmediately() )
        {
            m_saveOperatorBtn->Connect(wxEVT_BUTTON,
                                       wxCommandEventHandler(PrefsPageOperatorPanel::OnSaveButtonClick),
                                       NULL, this);
            
            m_delOperatorBtn->Connect(wxEVT_BUTTON,
                                       wxCommandEventHandler(PrefsPageOperatorPanel::OnDeleteButtonClick),
                                       NULL, this);
            
        }
    }
    
    virtual bool TransferDataToWindow()
    {
        // This is the place where you can initialize values, e.g. from wxConfig.
        // For demonstration purposes, we just set hardcoded values.
        //m_fulltext->SetValue(true);
        for (size_t s = 0; s < m_appconfs->Count(); s ++) {
            kWookaAppConfigItemInfo& it = m_appconfs->Item(s);
            m_gridOperators->InsertRows();
            m_gridOperators->SetCellValue(0, 0, it.configKey);
            m_gridOperators->SetCellValue(0, 1, it.configValue);
            m_gridOperators->SetCellValue(0, 2, it.configType == wxT("env") ? wxT("1") : wxT(""));
            m_gridOperators->SetCellEditor(0, 2, new wxGridCellBoolEditor());
            m_gridOperators->SetCellRenderer(0, 2, new wxGridCellBoolRenderer());
        }
        return true;
    }
    
    virtual bool TransferDataFromWindow()
    {
        // Called on platforms with modal preferences dialog to save and apply
        // the changes.
        wxGridTableBase* table = m_gridOperators->GetTable();
        int nums = table->GetRowsCount();
        m_appconfs->Clear();

        for (int i = 0; i < nums; i ++) {
            kWookaAppConfigItemInfo it;
            it.configKey = table->GetValue(i, 0);
            it.configValue = table->GetValue(i, 1);
            it.configType = table->GetValue(i, 2) == wxT("1") ? wxT("env") : wxT("other");
            m_appconfs->Add(it);
        }
        return true;
    }
    
private:
    
    void OnSaveButtonClick(wxCommandEvent& WXUNUSED(e))
    {
        // save new m_fulltext value and apply the change to the app
        
        wxString name = m_txtOperatorName->GetValue();
        wxString phone = m_txtOperatorPhone->GetValue();
        wxString env = m_chkEnvVariable->IsChecked() ? wxT("1") : wxT("");
        
        if (name.Length() <= 0) {
            wxMessageBox(_("Config Key should not be blank."));
            return;
        }
        
        int rows = m_gridOperators->GetNumberRows();
        m_gridOperators->InsertRows(rows);
        
        m_gridOperators->SetCellValue(rows, 0, name);
        m_gridOperators->SetCellValue(rows, 1, phone);
        m_gridOperators->SetCellValue(rows, 2, env);
        m_gridOperators->SetCellEditor(rows, 2, new wxGridCellBoolEditor());
        m_gridOperators->SetCellRenderer(rows, 2, new wxGridCellBoolRenderer());
        TransferDataFromWindow();
    }
    
    void OnDeleteButtonClick(wxCommandEvent& WXUNUSED(e))
    {
        wxArrayInt rows = m_gridOperators->GetSelectedRows();
        for(int i = rows.Count() - 1; i >= 0; i --) {
            m_gridOperators->DeleteRows(rows[i]);
        }
        TransferDataFromWindow();
    }

    void OnBrowse(wxCommandEvent& WXUNUSED(e))
    {
        wxArrayInt rows = m_gridOperators->GetSelectedRows();
        for (int i = rows.Count() - 1; i >= 0; i--) {
            m_gridOperators->DeleteRows(rows[i]);
        }
        TransferDataFromWindow();
    }
    
    wxStaticText    *m_lblOperatorId;
    wxStaticText    *m_lblOperatorName;
    wxStaticText    *m_lblOperatorPhone;
    wxStaticText    *m_lblEnvVariable;
    wxTextCtrl      *m_txtOperatorId;
    wxTextCtrl      *m_txtOperatorName;
    wxTextCtrl      *m_txtOperatorPhone;
    wxCheckBox      *m_chkEnvVariable;
    wxGrid          *m_gridOperators;
    wxButton        *m_saveOperatorBtn;
    wxButton        *m_delOperatorBtn;
};

class PrefsPageOperator : public wxPreferencesPage
{
private:
    kWookaAppConfigArray* m_appconf;
public:
    PrefsPageOperator(kWookaAppConfigArray* cfgs): wxPreferencesPage() {
        m_appconf = cfgs;
    }
public:
    virtual wxString GetName() const { return _("Environment"); }
    virtual wxBitmap GetLargeIcon() const
    { return wxArtProvider::GetBitmap(wxART_CDROM, wxART_TOOLBAR); }
    virtual wxWindow *CreateWindow(wxWindow *parent)
    { return new PrefsPageOperatorPanel(parent, m_appconf); }
};

class PrefsPageMavenPanel : public wxPanel
{
private:
    kWookaProjectInfo* m_proj;
public:
    PrefsPageMavenPanel(wxWindow* parent, kWookaProjectInfo* cfgs) : wxPanel(parent)
    {
        m_proj = cfgs;
        //wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer* sizer0 = new wxBoxSizer(wxHORIZONTAL);
        wxBoxSizer* sizer1 = new wxBoxSizer(wxHORIZONTAL);
        wxBoxSizer* sizer2 = new wxBoxSizer(wxHORIZONTAL);
        wxBoxSizer* sizer3 = new wxBoxSizer(wxHORIZONTAL);
        m_lblMavenHome = new wxStaticText(this, wxID_ANY, _("Maven Home:"));
        m_lblOperatorName = new wxStaticText(this, wxID_ANY, _("Repo Id:"));
        m_lblOperatorPhone = new wxStaticText(this, wxID_ANY, _("Address:"));
        m_lblEnvVariable = new wxStaticText(this, wxID_ANY, _("Snapshot:"));
        m_txtMavenHome = new wxTextCtrl(this, wxID_ANY, _T(""));
        m_txtOperatorName = new wxTextCtrl(this, wxID_ANY, _T(""));
        m_txtOperatorPhone = new wxTextCtrl(this, wxID_ANY, _T(""));
        m_chkEnvVariable = new wxCheckBox(this, wxID_ANY, _T(""));
        m_gridOperators = new wxGrid(this, wxID_ANY, wxDefaultPosition, wxSize(460, 340));
        wxPanel* spacePanel = new wxPanel(this, wxID_ANY);
        wxPanel* spacePanel2 = new wxPanel(this, wxID_ANY);
        m_saveOperatorBtn = new wxButton(this, wxID_ANY, _("Save"), wxDefaultPosition, wxSize(120, 20));
        m_delOperatorBtn = new wxButton(this, wxID_ANY, _("Delete"), wxDefaultPosition, wxSize(120, 20));
        m_btnBrowse = new wxButton(this, wxID_ANY, _("Browse"), wxDefaultPosition, wxSize(60, 20), 0);


        sizer0->Add(m_lblMavenHome, 0, wxEXPAND | wxALL, 5);
        sizer0->Add(m_txtMavenHome, 1, wxEXPAND | wxALL, 5);
        sizer0->Add(m_btnBrowse, 0, wxEXPAND | wxALL, 5);

        sizer1->Add(m_lblOperatorName, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_txtOperatorName, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_lblOperatorPhone, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_txtOperatorPhone, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_lblEnvVariable, 1, wxEXPAND | wxALL, 5);
        sizer1->Add(m_chkEnvVariable, 1, wxEXPAND | wxALL, 5);

        sizer->Add(sizer0, 0, wxEXPAND | wxALL, 5);
        sizer->Add(sizer1, 0, wxEXPAND | wxALL, 5);
        sizer2->Add(spacePanel, 1, wxEXPAND | wxALL, 5);
        sizer2->Add(m_saveOperatorBtn, 0, wxEXPAND | wxALL, 5);
        sizer3->Add(spacePanel2, 1, wxEXPAND | wxALL, 5);
        sizer3->Add(m_delOperatorBtn, 0, wxEXPAND | wxALL, 5);
        sizer->Add(sizer2, 0, wxEXPAND | wxALL, 5);
        sizer->Add(m_gridOperators, 0, wxEXPAND | wxALL, 5);
        sizer->Add(sizer3, 0, wxEXPAND | wxALL, 5);

        m_gridOperators->SetDefaultColSize(100);
        m_gridOperators->SetDefaultRowSize(25);
        m_gridOperators->SetRowLabelSize(50);
        m_gridOperators->SetColLabelSize(25);
        m_gridOperators->CreateGrid(0, 3, wxGrid::wxGridSelectRows);
        m_gridOperators->SetColLabelValue(0, _("REPO ID"));
        m_gridOperators->SetColLabelValue(1, _("ADDRESS"));
        m_gridOperators->SetColLabelValue(2, _("SNAPSHOT"));

        m_gridOperators->SetColSize(0, 240);
        m_gridOperators->SetColSize(1, 240);
        m_gridOperators->SetColSize(2, 120);


        SetSizerAndFit(sizer);

        //if ( wxPreferencesEditor::ShouldApplyChangesImmediately() )
        {
            m_saveOperatorBtn->Connect(wxEVT_BUTTON,
                wxCommandEventHandler(PrefsPageMavenPanel::OnSaveButtonClick),
                NULL, this);

            m_delOperatorBtn->Connect(wxEVT_BUTTON,
                wxCommandEventHandler(PrefsPageMavenPanel::OnDeleteButtonClick),
                NULL, this);

            m_btnBrowse->Connect(wxEVT_BUTTON,
                wxCommandEventHandler(PrefsPageMavenPanel::OnBrowse),
                NULL, this);

        }
    }

    virtual bool TransferDataToWindow()
    {
        // This is the place where you can initialize values, e.g. from wxConfig.
        // For demonstration purposes, we just set hardcoded values.
        //m_fulltext->SetValue(true);
        m_txtMavenHome->SetValue(m_proj->mavenHome);
        for (size_t s = 0; s < m_proj->mavenRepositories.Count(); s++) {
            kWookaMavenRepoInfo& it = m_proj->mavenRepositories.Item(s);
            m_gridOperators->InsertRows();
            m_gridOperators->SetCellValue(0, 0, it.repoId);
            m_gridOperators->SetCellValue(0, 1, it.address);
            m_gridOperators->SetCellValue(0, 2, it.snapshot == wxT("snapshot") ? wxT("1") : wxT(""));
            m_gridOperators->SetCellEditor(0, 2, new wxGridCellBoolEditor());
            m_gridOperators->SetCellRenderer(0, 2, new wxGridCellBoolRenderer());
        }
        return true;
    }

    virtual bool TransferDataFromWindow()
    {
        // Called on platforms with modal preferences dialog to save and apply
        // the changes.
        wxGridTableBase* table = m_gridOperators->GetTable();

        m_proj->mavenHome = m_txtMavenHome->GetValue();

        int nums = table->GetRowsCount();
        m_proj->mavenRepositories.Clear();

        for (int i = 0; i < nums; i++) {
            kWookaMavenRepoInfo it;
            it.repoId = table->GetValue(i, 0);
            it.address = table->GetValue(i, 1);
            it.snapshot = table->GetValue(i, 2) == wxT("1") ? wxT("snapshot") : wxT("release");
            m_proj->mavenRepositories.Add(it);
        }
        return true;
    }

private:

    void OnSaveButtonClick(wxCommandEvent& WXUNUSED(e))
    {
        // save new m_fulltext value and apply the change to the app

        wxString name = m_txtOperatorName->GetValue();
        wxString phone = m_txtOperatorPhone->GetValue();
        wxString env = m_chkEnvVariable->IsChecked() ? wxT("1") : wxT("");

        if (name.Length() <= 0) {
            wxMessageBox(_("Config Key should not be blank."));
            return;
        }

        int rows = m_gridOperators->GetNumberRows();
        m_gridOperators->InsertRows(rows);

        m_gridOperators->SetCellValue(rows, 0, name);
        m_gridOperators->SetCellValue(rows, 1, phone);
        m_gridOperators->SetCellValue(rows, 2, env);
        m_gridOperators->SetCellEditor(rows, 2, new wxGridCellBoolEditor());
        m_gridOperators->SetCellRenderer(rows, 2, new wxGridCellBoolRenderer());
        TransferDataFromWindow();
    }

    void OnDeleteButtonClick(wxCommandEvent& WXUNUSED(e))
    {
        wxArrayInt rows = m_gridOperators->GetSelectedRows();
        for (int i = rows.Count() - 1; i >= 0; i--) {
            m_gridOperators->DeleteRows(rows[i]);
        }
        TransferDataFromWindow();
    }


    void OnBrowse(wxCommandEvent& WXUNUSED(e))
    {
        wxDirDialog  dirdlg(this, _("Select a Maven Home directory"));
        if (dirdlg.ShowModal() == wxID_OK) {
            wxString filePath = dirdlg.GetPath();
            m_txtMavenHome->SetValue(filePath);
        }
        TransferDataFromWindow();
    }

    wxTextCtrl* m_txtMavenHome;
    wxStaticText* m_lblMavenHome;

    wxStaticText* m_lblOperatorId;
    wxStaticText* m_lblOperatorName;
    wxStaticText* m_lblOperatorPhone;
    wxStaticText* m_lblEnvVariable;
    wxTextCtrl* m_txtOperatorId;
    wxTextCtrl* m_txtOperatorName;
    wxTextCtrl* m_txtOperatorPhone;
    wxCheckBox* m_chkEnvVariable;
    wxGrid* m_gridOperators;
    wxButton* m_saveOperatorBtn;
    wxButton* m_delOperatorBtn;
    wxButton* m_btnBrowse;
};

class PrefsPageMaven : public wxPreferencesPage
{
private:
    kWookaProjectInfo* m_proj;
public:
    PrefsPageMaven(kWookaProjectInfo* cfgs) : wxPreferencesPage() {
        m_proj = cfgs;
    }
public:
    virtual wxString GetName() const { return _("Maven"); }
    virtual wxBitmap GetLargeIcon() const
    {
        return wxArtProvider::GetBitmap(wxART_CDROM, wxART_TOOLBAR);
    }
    virtual wxWindow* CreateWindow(wxWindow* parent)
    {
        return new PrefsPageMavenPanel(parent, m_proj);
    }
};

#endif /* life_perferences_hpp */
