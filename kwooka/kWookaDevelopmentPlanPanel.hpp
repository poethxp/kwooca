/********************************************************************************
*                                                                               *
* kWookaDevelopementPlanPanel.hpp -- Show development plan                      *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaDevelopmentPlanPanel_hpp
#define kWookaDevelopmentPlanPanel_hpp


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

#include "kWooka_ids.h"
//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>

class wxHtmlWindow;
class kWookaApplicationViewPanel;

////Dialog Style Start
#undef kWookaApplicationMonitorPanel_STYLE
#define kWookaApplicationMonitorPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

#define ID_EVENT_THREAD_MONITOR  11098


class kWookaDevelopmentPlanPanel : public wxPanel
{
    private:
        DECLARE_EVENT_TABLE();
        
    public:
        kWookaDevelopmentPlanPanel(wxWindow *parent, wxWindowID id = ID_PANEL_DONATE, const wxString &title = wxT("Donate"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationMonitorPanel_STYLE);
        virtual ~kWookaDevelopmentPlanPanel();
        
        void SetViewPanel(kWookaApplicationViewPanel* vp);
        kWookaApplicationViewPanel* GetViewPanel();

    private:
        //Do not add custom control declarations between
        //GUI Control Declaration Start and GUI Control Declaration End.
        //wxDev-C++ will remove them. Add custom code after the block.
        ////GUI Control Declaration Start
    
        ////GUI Control Declaration End
                
    private:
        //Note: if you receive any error with these enum IDs, then you need to
        //change your old form code that are based on the #define control IDs.
        //#defines may replace a numeric value for the enum names.
        //Try copy and pasting the below block in your old form header files.
        enum
        {
            ////GUI Enum Control ID Start
            ID_WXBUTTON_DELETE_RECORD = 2039,
            ID_WXEDIT8 = 2029,
            ID_WXSTATICTEXT_SPACE = 2020,
            ID_WXEDIT7 = 2019,
            ID_WXSTATICTEXT_MEMBER_NAME = 2018,
            ID_WXEDIT6 = 2017,
            ID_WXSTATICTEXT_MEMBER_CARD = 2016,
            ID_WXEDIT5 = 2015,
            ID_WXSTATICTEXT_ORDERNAME = 2013,
            ID_WXEDIT4 = 2012,
            ID_WXSTATICLABEL_DATE = 2011,
            ID_WXEDIT3 = 2010,
            ID_WXEDIT2 = 2009,
            ID_WXSTATICLABEL_STATUS = 2008,
            ID_WXBUTTON_ADDTOCART = 2005,
            ID_WXEDIT1 = 2004,
            ID_WXSTATICLABEL_ORDERNO = 2003,
            ID_WXSTATICBOX1 = 2002,
            ID_WXGRID1 = 2001,
            ////GUI Enum Control ID End
            ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
        };
        
    private:
        void OnKeyCharEvent(wxKeyEvent& event);
        void OnClose(wxCloseEvent& event);
        void CreateGUIControls();
    
        void CreateContentPanel();


        wxStaticBitmap* m_donateImage;
        wxHtmlWindow* m_htmlPanel;
        
        kWookaApplicationViewPanel* m_viewpanel;
};


#endif /* kPosAccountingPanel_hpp */
