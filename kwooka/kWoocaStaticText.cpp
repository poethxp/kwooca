/********************************************************************************
*                                                                               *
* kWookaStaticTex.cpp -- a custom implementation of static text for transparent *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWoocaStaticText.hpp"
#include "wx/stattext.h"

#ifndef WX_PRECOMP
#include "wx/event.h"
#include "wx/app.h"
#include "wx/brush.h"
#include "wx/dcclient.h"
#include "wx/settings.h"
#endif
#include <wx/dcbuffer.h>
#include <wx/control.h>
#include <wx/graphics.h>

BEGIN_EVENT_TABLE(kWoocaStaticText, wxStaticText)
    EVT_PAINT(kWoocaStaticText::OnPaint)
    // EVT_SIZE(kWoocaStaticText::OnSize)
    EVT_LEFT_DCLICK(kWoocaStaticText::OnDblClick)
    EVT_RIGHT_DCLICK(kWoocaStaticText::OnDblClick)
    EVT_MOTION(kWoocaStaticText::OnMouseOver)
END_EVENT_TABLE()


void kWoocaStaticText::OnPaint(wxPaintEvent& WXUNUSED(event)) {
    wxAutoBufferedPaintDC dc(this);
    dc.Clear();

    wxGraphicsContext* gc = wxGraphicsContext::Create(dc);
    if (gc)
    {
        wxSize sz = this->GetSize();
        
        wxGraphicsPath path = gc->CreatePath();
        path.AddRectangle(0, 0, sz.x, sz.y);

        wxBrush brush(m_parent->GetBackgroundColour());
        gc->SetBrush(brush);
        gc->FillPath(path);

        gc->SetFont(this->GetFont(), this->GetForegroundColour());
        if (this->IsEllipsized()) {
            gc->DrawText(this->GetEllipsizedLabel(), 0, 0);
        }
        else {
            // gc->DrawText(this->GetLabel(), 0, 0);
            const wxString lbl = this->GetLabel();
            wxDouble width = sz.x;
            wxDouble height = sz.y;
            wxDouble desent = 0, leading  = 0;
            gc->GetTextExtent(lbl, &width, &height, &desent, &leading);
            if (width > sz.x) {
                wxDouble echWidth = width / lbl.Length();
                wxDouble st = sz.x / echWidth;

                wxArrayString texts;
                for (size_t s = 0; s < lbl.Length(); s += st) {
                    wxString sttr = lbl.SubString(s, s + st);
                    texts.Add(sttr);
                }

                // gc->DrawText(lbl, 0, 0);
                for (size_t m = 0; m < texts.size(); m++) {
                    wxString line = texts[m];
                    gc->DrawText(line, 0, height * m);
                }

                if (sz.y < height * texts.size()) {
                    SetSize(sz.x, height * texts.size());
                }
            }
            else {
                gc->DrawText(lbl, 0, 0);
            }
        }
        delete gc;
    }
}

void kWoocaStaticText::OnMouseOver(wxMouseEvent& evt)
{
    evt.Skip(true);
}

void kWoocaStaticText::OnDblClick(wxMouseEvent& evt) {
    wxQueueEvent(this->m_parent, evt.Clone());
}