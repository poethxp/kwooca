﻿/********************************************************************************
*                                                                               *
* kWookaApplicationMySQLPanel.cpp -- a custom config for mysql database         *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationMySQLPanel.hpp"

#include "kWooka_ids.h"
#include "kWookaProjectInfo.hpp"
#include "kWookaExecuteProcess.hpp"

BEGIN_EVENT_TABLE(kWookaApplicationMySQLPanel, wxPanel)
////Manual Code Start
EVT_BUTTON(ID_WXBUTTON_BROWSE, kWookaApplicationMySQLPanel::OnBrowse)
EVT_BUTTON(ID_WXBUTTON_BROWSE_RESTORE, kWookaApplicationMySQLPanel::OnBrowseRestore)
EVT_BUTTON(ID_WXBUTTON_TEST, kWookaApplicationMySQLPanel::OnTest)
EVT_BUTTON(ID_WXBUTTON_APPLY, kWookaApplicationMySQLPanel::OnApply)

////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()



kWookaApplicationMySQLPanel::kWookaApplicationMySQLPanel(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationMySQLPanel::~kWookaApplicationMySQLPanel()
{
    wxLogDebug(wxT("kWookaApplicationMySQLPanel destroyed."));
}

void kWookaApplicationMySQLPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();

    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationMySQLPanel::CreateContentPanel() {
    
    WxSource = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    kBtnBrowse = new wxButton(this, ID_WXBUTTON_BROWSE, _("Browse"), wxDefaultPosition, wxSize(100, 20), 0);

    m_MySQLRemote = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    kBtnTest = new wxButton(this, ID_WXBUTTON_TEST, _("Test"), wxDefaultPosition, wxSize(100, 20), 0);
    kBtnApply = new wxButton(this, ID_WXBUTTON_APPLY, _("Apply"), wxDefaultPosition, wxSize(100, 20), 0);

    m_MySQLServer = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_MySQLPort = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_MySQLDatabase = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_UserName = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_Password = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_Advance = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_chkRestore = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    m_ClearDataDir = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    
    m_Restore = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    kBtnBrowseRestore = new wxButton(this, ID_WXBUTTON_BROWSE_RESTORE, _("Browse"), wxDefaultPosition, wxSize(100, 20), 0);

    m_chkDump = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    
    m_MySQLOptions = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24), wxTE_MULTILINE);
    

    WxSource->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnSourceChange), NULL, this);
    m_MySQLServer->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnServerChange), NULL, this);
    m_MySQLPort->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnPortChange), NULL, this);
    m_MySQLDatabase->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnDatabaseChange), NULL, this);
    m_UserName->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnUsernameChange), NULL, this);
    m_Password->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnPasswordChange), NULL, this);
    m_Advance->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnAdvanceChange), NULL, this);
    m_MySQLOptions->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnOptionsChange), NULL, this);
    m_MySQLRemote->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationMySQLPanel::OnRemoteChange), NULL, this);
    m_chkRestore->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMySQLPanel::OnRestoreChange), NULL, this);
    m_chkDump->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationMySQLPanel::OnCheckRestoreChange), NULL, this);
    m_chkRestore->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationMySQLPanel::OnCheckDumpChange), NULL, this);
    m_ClearDataDir->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationMySQLPanel::OnClearDataDirChange), NULL, this);

    wxStaticText* lblSource = new wxStaticText(this, wxID_ANY, _("MySQL Home"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblServer = new wxStaticText(this, wxID_ANY, _("Server"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblPort = new wxStaticText(this, wxID_ANY, _("Port"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblDatabase = new wxStaticText(this, wxID_ANY, _("Database"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblUsername = new wxStaticText(this, wxID_ANY, _("Username"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblPassword = new wxStaticText(this, wxID_ANY, _("Password"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblAdvance = new wxStaticText(this, wxID_ANY, _("Advance"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblOptions = new wxStaticText(this, wxID_ANY, _("Server Options"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRemote = new wxStaticText(this, wxID_ANY, _("Remote Connect"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRestore = new wxStaticText(this, wxID_ANY, _("Restore On Startup"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblDump = new wxStaticText(this, wxID_ANY, _("Dump On Shutdown"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblEmpty = new wxStaticText(this, wxID_ANY, _(""), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblEmpty2 = new wxStaticText(this, wxID_ANY, _(""), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblEmpty3 = new wxStaticText(this, wxID_ANY, _(""), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblEmpty4 = new wxStaticText(this, wxID_ANY, _(""), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblClearDataDir = new wxStaticText(this, wxID_ANY, _("Clear Data"), wxPoint(20, 8), wxSize(140, 24));
    
    wxBoxSizer* bsSource = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsRemote = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsServer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsAuth = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsAdvance = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsRestore = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsDump = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsOptions = new wxBoxSizer(wxHORIZONTAL);

    bsSource->Add(lblSource, 0, wxEXPAND | wxALL, 5);
    bsSource->Add(WxSource, 1, wxEXPAND | wxALL, 5);
    bsSource->Add(kBtnBrowse, 0, wxEXPAND | wxALL, 5);

    bsRemote->Add(lblRemote, 0, wxEXPAND | wxALL, 5);
    bsRemote->Add(m_MySQLRemote, 1, wxEXPAND |  wxALL, 5);
    bsRemote->Add(kBtnTest, 0, wxEXPAND | wxALL, 5);
    bsRemote->Add(kBtnApply, 0, wxEXPAND | wxALL, 5);
    
    // bsJvmHeap->Add(bsJvmHeap1, 3, wxSHRINK | | wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap2, 1, wxSHRINK | wxALIGN_CENTER | wxALL, 5);

    bsServer->Add(lblServer, 0, wxEXPAND | wxALL, 5);
    bsServer->Add(m_MySQLServer, 1, wxEXPAND | wxALL, 5);

    bsServer->Add(lblPort, 0, wxEXPAND |  wxALL, 5);
    bsServer->Add(m_MySQLPort, 1, wxEXPAND |  wxALL, 5);

    bsServer->Add(lblDatabase, 0, wxEXPAND | wxALL, 5);
    bsServer->Add(m_MySQLDatabase, 1, wxEXPAND | wxALL, 5);

    
    bsAuth->Add(lblUsername, 0, wxEXPAND | wxALL, 5);
    bsAuth->Add(m_UserName, 1, wxEXPAND | wxALL, 5);

    bsAuth->Add(lblPassword, 0, wxEXPAND | wxALL, 5);
    bsAuth->Add(m_Password, 1, wxEXPAND |  wxALL, 5);
    bsAuth->Add(lblEmpty, 0, wxEXPAND | wxALL, 5);
    bsAuth->Add(lblEmpty2, 1, wxEXPAND | wxALL, 5);

    bsAdvance->Add(lblAdvance, 0, wxEXPAND | wxALL, 5);
    bsAdvance->Add(m_Advance, 1, wxEXPAND | wxALL, 5);

    bsRestore->Add(lblRestore, 0, wxEXPAND | wxALL, 5);
    bsRestore->Add(m_chkRestore, 0, wxEXPAND | wxALL, 5);
    bsRestore->Add(m_Restore, 1, wxEXPAND | wxALL, 5);
    bsRestore->Add(kBtnBrowseRestore, 0, wxEXPAND | wxALL, 5);
    
    bsDump->Add(lblDump, 0, wxEXPAND | wxALL, 5);
    bsDump->Add(m_chkDump, 0, wxEXPAND | wxALL, 5);
    bsDump->Add(lblEmpty4, 0, wxEXPAND | wxALL, 5);
    bsDump->Add(lblClearDataDir, 0, wxEXPAND | wxALL, 5);
    bsDump->Add(m_ClearDataDir, 0, wxEXPAND | wxALL, 5);
    bsDump->Add(lblEmpty3, 1, wxEXPAND | wxALL, 5);
    

    bsOptions->Add(lblOptions, 0, wxEXPAND | wxALL, 5);
    bsOptions->Add(m_MySQLOptions, 1, wxEXPAND | wxALL, 5);

    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    boxSizer->Add(bsSource, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsRemote, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsServer, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsAuth, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsAdvance, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsRestore, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsDump, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsOptions, 1, wxEXPAND | wxALL, 5);
    
    this->SetSizerAndFit(boxSizer);
}

void kWookaApplicationMySQLPanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}

void kWookaApplicationMySQLPanel::UpdateAppItem(kWookaProjectInfo* proj, kWookaAppItemInfo* item) {
    m_project = proj;
    m_appItem = item;
    if (m_appItem != NULL) {
        m_MySQLServer->SetValue(m_appItem->GetItemValue(wxT("MySQLServer")));
        m_MySQLPort->SetValue(m_appItem->GetItemValue(wxT("MySQLPort")));
        m_MySQLDatabase->SetValue(m_appItem->GetItemValue(wxT("MySQLDatabase")));
        m_UserName->SetValue(m_appItem->GetItemValue(wxT("UserName")));
        m_Password->SetValue(m_appItem->GetItemValue(wxT("Password")));
        m_Advance->SetValue(m_appItem->GetItemValue(wxT("Advance")));
        m_MySQLOptions->SetValue(m_appItem->GetItemValue(wxT("MySQLOptions")));
        m_MySQLRemote->SetValue(m_appItem->GetItemValue(wxT("MySQLRemote")) == wxT("remote") ? true : false);
        WxSource->SetValue(m_appItem->source);
        m_Restore->SetValue(m_appItem->GetItemValue(wxT("RestoreFile")));
        
        m_chkDump->SetValue(m_appItem->GetItemValue(wxT("Restore")) == wxT("restore") ? true : false);
        m_chkRestore->SetValue(m_appItem->GetItemValue(wxT("Dump")) == wxT("dump") ? true : false);
        m_ClearDataDir->SetValue(m_appItem->GetItemValue(wxT("ClearDataDir")) == wxT("clear") ? true : false);
    }
    else {
        m_MySQLServer->SetValue(wxT(""));
        m_MySQLPort->SetValue(wxT(""));
        m_MySQLDatabase->SetValue(wxT(""));
        m_UserName->SetValue(wxT(""));
        m_Password->SetValue(wxT(""));
        m_Advance->SetValue(wxT(""));
        m_MySQLOptions->SetValue(wxT(""));
        m_Restore->SetValue(wxT(""));
        m_MySQLRemote->SetValue(false);
        m_chkDump->SetValue(false);
        m_chkRestore->SetValue(false);
        WxSource->SetValue(wxT(""));
        m_ClearDataDir->SetValue(false);
    }
}

void kWookaApplicationMySQLPanel::OnTest(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        kWookaProcessExecutor* executor = m_project->runtime->GetExecutor(m_appItem->id);
        if (!executor->Test()) {
            wxMessageBox(_("The nginx configuration is bad. The detail information please lookup the log view."));
        }
        else {
            wxMessageBox(_("The mysql configuration test is successful."));
        }
    }
}

void kWookaApplicationMySQLPanel::OnApply(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        if (wxYES == wxMessageBox(_("Do you want to apply the current MySQL connection properties into all projects?"), wxT("Confirm"), wxYES_NO)) {
            //chimes.datasource.driverClassName: com.mysql.cj.jdbc.Driver
            //chimes.datasource.url : jdbc : mysql ://127.0.0.1:3306/chimesbase?characterEncoding=utf8&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai&useSSL=false 
            //chimes.datasource.username : chimes
            //chimes.datasource.password : ks123456
            for (int i = 0; i < 4; i++) {
                wxArrayInt toDeletes;
                for (size_t s = 0; s < m_project->appConfigs.Count(); s++) {
                    kWookaAppConfigItemInfo& it = m_project->appConfigs.Item(s);
                    if (it.configKey == wxT("chimes.datasource.driverClassName") || it.configKey == wxT("chimes.datasource.driver-class-name")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                    else if (it.configKey == wxT("chimes.datasource.url")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                    else if (it.configKey == wxT("chimes.datasource.username")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                    else if (it.configKey == wxT("chimes.datasource.password")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                }
                for (size_t s = 0; s < toDeletes.Count(); s++) {
                    m_project->appConfigs.RemoveAt(toDeletes.Item(s));
                }
            }
            kWookaAppConfigItemInfo itdcn;
            kWookaAppConfigItemInfo iturl;
            kWookaAppConfigItemInfo itusr;
            kWookaAppConfigItemInfo itpwd;
            itdcn.configKey = wxT("chimes.datasource.driverClassName");
            itdcn.configType = "";
            itdcn.configValue = wxT("com.mysql.cj.jdbc.Driver");

            iturl.configKey = wxT("chimes.datasource.url");
            iturl.configType = "";
            wxString advdef = wxT("characterEncoding=utf8&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai&useSSL=false");
            wxString adv = m_appItem->GetItemValue(wxT("Advance"));
            wxString host = m_appItem->GetItemValue(wxT("MySQLServer"));
            wxString port = m_appItem->GetItemValue(wxT("MySQLPort"));
            wxString db = m_appItem->GetItemValue(wxT("MySQLDatabase"));
            if (adv.Trim().IsEmpty()) {
                iturl.configValue = wxString::Format(wxT("jdbc:mysql://%s:%s/%s?%s"), host, port, db, advdef);
            }
            else {
                iturl.configValue = wxString::Format(wxT("jdbc:mysql://%s:%s/%s?%s"), host, port, db, adv);
            }
            
            itusr.configKey = wxT("chimes.datasource.username");
            itusr.configType = "";
            itusr.configValue = m_appItem->GetItemValue(wxT("UserName"));
            itpwd.configKey = wxT("chimes.datasource.password");
            itpwd.configType = "";
            itpwd.configValue = m_appItem->GetItemValue(wxT("Password"));
            m_project->appConfigs.Add(itdcn);
            m_project->appConfigs.Add(iturl);
            m_project->appConfigs.Add(itusr);
            m_project->appConfigs.Add(itpwd);
        }
    }
}

void kWookaApplicationMySQLPanel::OnServerChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("MySQLServer"), event.GetString());
        m_appItem->name = event.GetString() + wxT("_") + m_appItem->GetItemValue(wxT("MySQLPort"));
    }
}

void kWookaApplicationMySQLPanel::OnPortChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("MySQLPort"), event.GetString());
        m_appItem->name = m_appItem->GetItemValue(wxT("MySQLServer")) + wxT("_") + event.GetString();
    }
}

void kWookaApplicationMySQLPanel::OnDatabaseChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("MySQLDatabase"), event.GetString());
    }
}

void kWookaApplicationMySQLPanel::OnUsernameChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("UserName"), event.GetString());
    }
}

void kWookaApplicationMySQLPanel::OnPasswordChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("Password"), event.GetString());
    }
}

void kWookaApplicationMySQLPanel::OnAdvanceChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("Advance"), event.GetString());
    }
}

void kWookaApplicationMySQLPanel::OnOptionsChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("MySQLOptions"), event.GetString());
    }
}

void kWookaApplicationMySQLPanel::OnRemoteChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("MySQLRemote"), event.IsChecked() ? wxT("remote"): wxT("local"));
    }
}

void kWookaApplicationMySQLPanel::OnRestoreChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RestoreFile"), event.GetString());
    }
}

void kWookaApplicationMySQLPanel::OnCheckRestoreChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("Restore"), event.IsChecked() ? wxT("restore") : wxT("none"));
    }
}

void kWookaApplicationMySQLPanel::OnCheckDumpChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("Dump"), event.IsChecked() ? wxT("dump") : wxT("none"));
    }
}

void kWookaApplicationMySQLPanel::OnClearDataDirChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("ClearDataDir"), event.IsChecked() ? wxT("clear") : wxT("none"));
    }
}

void kWookaApplicationMySQLPanel::OnSourceChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->source = event.GetString();
    }
}


void kWookaApplicationMySQLPanel::OnBrowse(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        wxDirDialog  dirdlg(this, _("Open the MySQL HOME directory"));

        if (dirdlg.ShowModal() == wxID_OK) {
            this->WxSource->SetValue(dirdlg.GetPath());
            m_appItem->source = dirdlg.GetPath();
        }
    }
}

void kWookaApplicationMySQLPanel::OnBrowseRestore(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        wxFileDialog  filedlg(this, _("Open a SQL file to restore"));

        if (filedlg.ShowModal() == wxID_OK) {
            this->m_Restore->SetValue(filedlg.GetPath());
            m_appItem->UpdateItem(wxT("RestoreFile"), filedlg.GetPath());
        }
    }
}
