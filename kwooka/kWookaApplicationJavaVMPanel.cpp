﻿/********************************************************************************
*                                                                               *
* kWookaApplicationJavaVMPanel.cpp -- JVM configuration editor                  *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationJavaVMPanel.hpp"

#include "kWooka_ids.h"
#include "kWookaProjectInfo.hpp"

BEGIN_EVENT_TABLE(kWookaApplicationJavaVMPanel, wxPanel)
////Manual Code Start
////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()



kWookaApplicationJavaVMPanel::kWookaApplicationJavaVMPanel(wxWindow* parent, wxWindowID id, const wxString& WXUNUSED(title), const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationJavaVMPanel::~kWookaApplicationJavaVMPanel()
{
    wxLogDebug(wxT("kWookaApplicationJavaVMPanel destroyed."));
}

void kWookaApplicationJavaVMPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();

    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationJavaVMPanel::CreateContentPanel() {
    
    m_JvmMinHeap = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_JvmMaxHeap = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_JvmMaxPermSize = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_JvmMaxDirectMemorySize = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_JvmGCOptions = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_JvmOtherOptions = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    
    m_JvmEnableDebug = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    m_JvmDebugAddress = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));

    m_JvmMinHeap->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnHeapChange), NULL, this);
    m_JvmMaxHeap->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnMaxheapChange), NULL, this);
    m_JvmMaxPermSize->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnMaxPermSizeChange), NULL, this);
    m_JvmMaxDirectMemorySize->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnMaxDirectSizeChange), NULL, this);
    m_JvmGCOptions->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnJvmGCOptionsChange), NULL, this);
    m_JvmOtherOptions->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnJvmOtherOptionChange), NULL, this);
    m_JvmDebugAddress->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnJvmDebugAddressChange), NULL, this);
    m_JvmEnableDebug->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationJavaVMPanel::OnJvmEnableDebugChange), NULL, this);

    wxStaticText* lblJvmMinHeap = new wxStaticText(this, wxID_ANY, _("Min-Heap(-Xms)"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmMaxHeap = new wxStaticText(this, wxID_ANY, _("Max-Heap(-Xmx)"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmMaxPermSize = new wxStaticText(this, wxID_ANY, _("Max-PermSize"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmMaxDirectMemorySize = new wxStaticText(this, wxID_ANY, _("Max DirectMemorySize"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmOtherOptions = new wxStaticText(this, wxID_ANY, _("Other Options"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmGCOptions = new wxStaticText(this, wxID_ANY, _("GC Options"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmDebugOptions = new wxStaticText(this, wxID_ANY, _("Enable Debug"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblJvmDebugAddress = new wxStaticText(this, wxID_ANY, _("Debug Port"), wxPoint(20, 8), wxSize(140, 24));

    wxBoxSizer* bsJvmHeap = new wxBoxSizer(wxHORIZONTAL);
    //wxBoxSizer* bsJvmHeap1 = new wxBoxSizer(wxHORIZONTAL);
    //wxBoxSizer* bsJvmHeap2 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsJvmPerm = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsJvmDirect = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsJvmGC = new wxBoxSizer(wxHORIZONTAL);

    wxBoxSizer* bsJvmDbg = new wxBoxSizer(wxHORIZONTAL);

    bsJvmHeap->Add(lblJvmMinHeap, 0, wxEXPAND | wxALL, 5);
    bsJvmHeap->Add(m_JvmMinHeap, 1, wxEXPAND | wxALL, 5);
    bsJvmHeap->Add(lblJvmMaxHeap, 0, wxEXPAND | wxALL, 5);
    bsJvmHeap->Add(m_JvmMaxHeap, 1, wxEXPAND |  wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap1, 3, wxSHRINK | | wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap2, 1, wxSHRINK | wxALIGN_CENTER | wxALL, 5);

    bsJvmPerm->Add(lblJvmMaxPermSize, 0, wxEXPAND | wxALL, 5);
    bsJvmPerm->Add(m_JvmMaxPermSize, 1, wxEXPAND | wxALL, 5);

    bsJvmPerm->Add(lblJvmMaxDirectMemorySize, 0, wxEXPAND |  wxALL, 5);
    bsJvmPerm->Add(m_JvmMaxDirectMemorySize, 1, wxEXPAND |  wxALL, 5);

    bsJvmDirect->Add(lblJvmOtherOptions, 0, wxEXPAND | wxALL, 5);
    bsJvmDirect->Add(m_JvmOtherOptions, 5, wxEXPAND |  wxALL, 5);
    bsJvmGC->Add(lblJvmGCOptions, 0, wxEXPAND | wxALL, 5);
    bsJvmGC->Add(m_JvmGCOptions, 5, wxEXPAND | wxALL, 5);

    bsJvmDbg->Add(lblJvmDebugOptions, 0, wxEXPAND | wxALL, 5);
    bsJvmDbg->Add(m_JvmEnableDebug, 5, wxEXPAND | wxALL, 5);
    bsJvmDbg->Add(lblJvmDebugAddress, 0, wxEXPAND | wxALL, 5);
    bsJvmDbg->Add(m_JvmDebugAddress, 5, wxEXPAND | wxALL, 5);

    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    boxSizer->Add(bsJvmHeap, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsJvmPerm, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsJvmDirect, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsJvmGC, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsJvmDbg, 0, wxEXPAND | wxALL, 5);
    
    this->SetSizerAndFit(boxSizer);
}

void kWookaApplicationJavaVMPanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}

void kWookaApplicationJavaVMPanel::UpdateJvmConfig(kWookaJvmConfigInfo* jvm) {
    m_jvmConfig = jvm;
    if (m_jvmConfig != NULL) {
        m_JvmMinHeap->SetValue(m_jvmConfig->jvmHeap);
        m_JvmMaxHeap->SetValue(m_jvmConfig->jvmMaxHeap);
        m_JvmMaxPermSize->SetValue(m_jvmConfig->jvmMaxPermSize);
        m_JvmMaxDirectMemorySize->SetValue(m_jvmConfig->jvmMaxDirectMemorySize);        
        m_JvmGCOptions->SetValue(m_jvmConfig->jvmGCOptions);
        m_JvmOtherOptions->SetValue(m_jvmConfig->jvmOtherOptions);
        m_JvmDebugAddress->SetValue(m_jvmConfig->jvmDebugAddress);
        m_JvmEnableDebug->SetValue(m_jvmConfig->jvmDebugEnabled);
    }
    else {
        m_JvmMinHeap->SetValue(wxT(""));
        m_JvmMaxHeap->SetValue(wxT(""));
        m_JvmMaxPermSize->SetValue(wxT(""));
        m_JvmMaxDirectMemorySize->SetValue(wxT(""));
        m_JvmOtherOptions->SetValue(wxT(""));
        m_JvmGCOptions->SetValue(wxT(""));
        m_JvmDebugAddress->SetValue(wxT(""));
        m_JvmEnableDebug->SetValue(false);
    }
}


void kWookaApplicationJavaVMPanel::OnJvmOtherOptionChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmOtherOptions = event.GetString();
    }
}

void kWookaApplicationJavaVMPanel::OnJvmGCOptionsChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmGCOptions = event.GetString();
    }
}

void kWookaApplicationJavaVMPanel::OnHeapChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmHeap = event.GetString();
    }
}

void kWookaApplicationJavaVMPanel::OnMaxheapChange(wxCommandEvent& event)
{
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmMaxHeap = event.GetString();
    }
}

void kWookaApplicationJavaVMPanel::OnJvmDebugAddressChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmDebugAddress = event.GetString();
    }
}

void kWookaApplicationJavaVMPanel::OnJvmEnableDebugChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmDebugEnabled = event.IsChecked();
    }
}


void kWookaApplicationJavaVMPanel::OnMaxPermSizeChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmMaxPermSize = event.GetString();
    }
}


void kWookaApplicationJavaVMPanel::OnMaxDirectSizeChange(wxCommandEvent& event) {
    if (m_jvmConfig != NULL) {
        m_jvmConfig->jvmMaxDirectMemorySize = event.GetString();
    }
}

