﻿/********************************************************************************
*                                                                               *
* kWookaApplicationNginxServicePanel.cpp -- a custom config for nginx server node*
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationNginxServicePanel.hpp"

#include "kWooka_ids.h"
#include "kWookaProjectInfo.hpp"

BEGIN_EVENT_TABLE(kWookaApplicationNginxServicePanel, wxPanel)
////Manual Code Start
EVT_BUTTON(ID_WXBUTTON_BROWSE, kWookaApplicationNginxServicePanel::OnBrowse)
////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()



kWookaApplicationNginxServicePanel::kWookaApplicationNginxServicePanel(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationNginxServicePanel::~kWookaApplicationNginxServicePanel()
{
    wxLogDebug(wxT("kWookaApplicationNginxServicePanel destroyed."));
}

void kWookaApplicationNginxServicePanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();

    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationNginxServicePanel::CreateContentPanel() {

    m_NginxServerName = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_NginxServerOptions = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24), wxTE_MULTILINE);
   
    m_NginxServerOptions->SetMaxLength(wxINT16_MAX);

    m_NginxServerName->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServicePanel::OnServerNameChange), NULL, this);
    m_NginxServerOptions->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServicePanel::OnServerOptionsChange), NULL, this);
    
    WxSource = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    WxSource->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServicePanel::OnSourceChange), NULL, this);

    wxStaticText* lblServerName = new wxStaticText(this, wxID_ANY, _("Service Name"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblServerOptions = new wxStaticText(this, wxID_ANY, _("Service Options"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblNginxSource = new wxStaticText(this, wxID_ANY, _("Location"), wxPoint(20, 8), wxSize(140, 24));
    wxBoxSizer* bsSourceInfo = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsServerInfo = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsServerOptions = new wxBoxSizer(wxHORIZONTAL);

    bsSourceInfo->Add(lblNginxSource, 0, wxEXPAND | wxALL, 5);
    bsSourceInfo->Add(WxSource, 1, wxEXPAND | wxALL, 5);

    bsServerInfo->Add(lblServerName, 0, wxEXPAND | wxALL, 5);
    bsServerInfo->Add(m_NginxServerName, 2, wxEXPAND | wxALL, 5);
   
    // bsJvmHeap->Add(bsJvmHeap1, 3, wxSHRINK | | wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap2, 1, wxSHRINK | wxALIGN_CENTER | wxALL, 5);

    bsServerOptions->Add(lblServerOptions, 0, wxEXPAND | wxALL, 5);
    bsServerOptions->Add(m_NginxServerOptions, 1, wxEXPAND | wxALL, 5);

    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    
    boxSizer->Add(bsServerInfo, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsSourceInfo, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsServerOptions, 1, wxEXPAND | wxALL, 5);


    this->SetSizerAndFit(boxSizer);
}

void kWookaApplicationNginxServicePanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}


void kWookaApplicationNginxServicePanel::OnBrowse(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        wxDirDialog  dirdlg(this, _("Open the NGINX HOME directory"));


        if (dirdlg.ShowModal() == wxID_OK) {
            this->WxSource->SetValue(dirdlg.GetPath());
            m_appItem->source = dirdlg.GetPath();
        }
    }
}

void kWookaApplicationNginxServicePanel::UpdateNginxConfig(kWookaAppItemInfo* appItem) {
    m_appItem = appItem;
    if (m_appItem != NULL) {
        m_NginxServerName->SetValue(m_appItem->name);
        m_NginxServerOptions->SetValue(m_appItem->GetItemValue(wxT("ServiceOptions")));
        WxSource->SetValue(m_appItem->source);
    }
    else {
        m_NginxServerName->SetValue(wxT(""));
        m_NginxServerOptions->SetValue(wxT(""));
        WxSource->SetValue(wxT(""));
    }
}

void kWookaApplicationNginxServicePanel::OnSourceChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->source = event.GetString();
    }
}

void kWookaApplicationNginxServicePanel::OnServerNameChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->name = event.GetString();
    }
}


void kWookaApplicationNginxServicePanel::OnServerOptionsChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("ServiceOptions"), event.GetString());
    }
}
