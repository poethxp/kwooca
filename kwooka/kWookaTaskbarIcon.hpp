/********************************************************************************
*                                                                               *
* kWookaTaskbarIcon.hpp -- The taskbar support                                  *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaTaskbarIcon_hpp
#define kWookaTaskbarIcon_hpp

#include <stdio.h>
#include "wx/minifram.h"
#include "wx/stack.h"
#include "wx/clipbrd.h"
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/statline.h"
#include "wx/wfstream.h"
#include "wx/filedlg.h"
#include "wx/stockitem.h"
#include "wx/dcbuffer.h"
#include "wx/tipwin.h"
#include "wx/metafile.h"
#include "wx/treectrl.h"
#include "wx/aui/aui.h"
#include "wx/choice.h"
#include "kWooka_ids.h"
#include <map>

#include "wx/taskbar.h"

class kWookaFrame;

class kWookaTaskbarIcon : public wxTaskBarIcon
{
public:
        // ctor and dtor
#if defined(__WXOSX__) && wxOSX_USE_COCOA
    kWookaTaskbarIcon(wxTaskBarIconType iconType = wxTBI_DEFAULT_TYPE)
        : wxTaskBarIcon(iconType)
#else
    kWookaTaskbarIcon()
#endif
    {}

    virtual ~kWookaTaskbarIcon() {}

    void SetWookaFrame(kWookaFrame* lf) {
        m_wookaframe = lf;
    }
public:
    enum {
        ID_TIMER = 1001,
        ID_MENU_LOAD = 1010,
        ID_MENU_STARTALL,
        ID_MENU_STOPALL,
        ID_MENU_SHOW,
        ID_MENU_CLOSE,
        ID_MENU_EXIT,
        ID_CMD_NEW_VERSION
    };
private:
    wxBitmap m_bmp;
    bool m_hasShape;
    bool m_doingCrop;
	wxPoint m_endPoint;
    wxPoint m_delta;
    kWookaFrame* m_wookaframe;
    
    
    void OnLeftButtonDClick(wxTaskBarIconEvent&);
    void OnMenuLoad(wxCommandEvent& event);
    void OnMenuStartAll(wxCommandEvent& event);
    void OnMenuStopAll(wxCommandEvent& event);
    void OnMenuClose(wxCommandEvent& event);
    void OnMenuPrefs(wxCommandEvent& event);
	void OnMenuExit(wxCommandEvent& event);
    void OnMenuShow(wxCommandEvent& event);
    
   virtual wxMenu* CreatePopupMenu() wxOVERRIDE;

    #ifdef __WXGTK__
    void OnWindowCreate(wxWindowCreateEvent& event);
    #endif
    DECLARE_EVENT_TABLE()
};

#endif /* kPosShapeFrame_hpp */
