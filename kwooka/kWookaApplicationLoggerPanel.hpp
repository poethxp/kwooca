/********************************************************************************
*                                                                               *
* kWookaApplicationLoggerPanel.hpp -- Show logger information                   *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaApplicationLoggerPanel_hpp
#define kWookaApplicationLoggerPanel_hpp


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include "wx/srchctrl.h"
#include "wx/spinctrl.h"

////Dialog Style Start
#undef kWookaApplicationLoggerPanel_STYLE
#define kWookaApplicationLoggerPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

class kWookaApplicationViewPanel;


class kWookaApplicationLoggerPanel : public wxPanel
{
    private:
        DECLARE_EVENT_TABLE();
        
    public:
        kWookaApplicationLoggerPanel(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("kpos"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationLoggerPanel_STYLE);
        virtual ~kWookaApplicationLoggerPanel();
    
        void SetViewPanel(kWookaApplicationViewPanel* vp);
        kWookaApplicationViewPanel* GetViewPanel();

        void UpdateLoggerUI();

    private:
        //Do not add custom control declarations between
        //GUI Control Declaration Start and GUI Control Declaration End.
        //wxDev-C++ will remove them. Add custom code after the block.
        ////GUI Control Declaration Start
    
        wxSpinCtrl* kWookaSpinCtrl;
        wxRadioButton* kWookaListCtrl;
        wxRadioButton* kWookaTextCtrl;
        wxSearchCtrl* m_searchCtrl;
        wxListBox* m_loglistList;
        wxTextCtrl* m_loglistText;

        ////GUI Control Declaration End
    private:
        //Note: if you receive any error with these enum IDs, then you need to
        //change your old form code that are based on the #define control IDs.
        //#defines may replace a numeric value for the enum names.
        //Try copy and pasting the below block in your old form header files.
        enum
        {
            ////GUI Enum Control ID Start
            ID_WXBUTTON_DELETE_RECORD = 2039,
            ID_WXEDIT8 = 2029,
            ID_WXSTATICTEXT_SPACE = 2020,
            ID_WXEDIT7 = 2019,
            ID_WXSTATICTEXT_MEMBER_NAME = 2018,
            ID_WXEDIT6 = 2017,
            ID_RADIO_LIST_CTRL = 2016,
            ID_RADIO_TEXT_CTRL = 2015,
            ID_WXSTATICTEXT_ORDERNAME = 2013,
            ID_WXBTN_SEARCH = 2012,
            ID_WXBTN_CLEAR = 2011,
            ID_SPIN_CACHELINES = 2010,
            ID_WXSTATICLABEL_STATUS = 2008,
            ID_WXBUTTON_ADDTOCART = 2005,
            ID_WX_LOGGER_LISTBOX = 2004,
            ID_WXSTATICLABEL_ORDERNO = 2003,
            ID_SEARCH_LOGS = 2002,
            ID_WXGRID1 = 2001,
            ////GUI Enum Control ID End
            ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
        };
        
    private:
        void OnKeyCharEvent(wxKeyEvent& event);
        void OnClose(wxCloseEvent& event);
        void CreateGUIControls();
        void CreateContentPanel();
        void OnClear(wxCommandEvent& evt);
        void OnSearch(wxCommandEvent& evt);
        void OnLoggingEvent(wxThreadEvent& te);
        void OnCacheLineChanged(wxSpinEvent& evt);
        void OnRadioListChanged(wxCommandEvent& evt);
        void OnRadioTextChanged(wxCommandEvent& evt);


        kWookaApplicationViewPanel* m_viewpanel;
        bool m_toggleList;
        int  m_logscrolled;
        int  m_cachedlines;
};


#endif /* kPosAccountingPanel_hpp */
