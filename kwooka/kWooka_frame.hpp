/********************************************************************************
*                                                                               *
* kwooka_frame.h --       The main frame                                        *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kwooka_frame_hpp
#define kwooka_frame_hpp

#include <stdio.h>
#include "wx/minifram.h"
#include "wx/stack.h"
#include "wx/clipbrd.h"
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/statline.h"
#include "wx/wfstream.h"
#include "wx/filedlg.h"
#include "wx/stockitem.h"
#include "wx/dcbuffer.h"
#include "wx/tipwin.h"
#include "wx/metafile.h"
#include "wx/treectrl.h"
#include "wx/aui/aui.h"
#include "wx/choice.h"
#include "kWooka_ids.h"
#include "kWookaProjectInfo.hpp"

#include "kWookaApplicationListPanel.hpp"

class wxTreeCtrl;
class wxHtmlWindow;
class wxAuiNotebook;
class kPosEventObject;
class kWookaApplicationViewPanel;
class kWookaTaskbarIcon;

class MyStatusBar:public wxStatusBar
{
public:
    MyStatusBar(wxWindow *parent,wxWindowID id = wxID_ANY,long style = wxSTB_DEFAULT_STYLE,const wxString& name = wxStatusBarNameStr);
    virtual ~MyStatusBar() {
        wxLogDebug(wxT("MyStatusBar destoryed."));
    }
    void setGaugeValue(int n);
    int getGaugeValue();
private:
    void OnSize(wxSizeEvent& event);
    wxGauge* gauge;
    int gaugeRange = 100;
};


class kWookaFrame : public wxFrame
{
public:
    // ctor and dtor
    kWookaFrame();
    virtual ~kWookaFrame();
    
    void UpdateUI();
    void BackendOpenFile(const wxString& file, uint32_t fc = 0);

#if !defined(MAP_USE_DIALOG) || MAP_USE_DIALOG == 0
    void OnThumbSize(wxSizeEvent& event);
#endif
    //virtual wxThread::ExitCode Entry();
    
public:
    kWookaApplicationListPanel* GetListPanel();
    kWookaApplicationViewPanel* GetViewPanel();
    kWookaProjectInfo* GetProjectInfo() {
        return m_wooka;
    }

public:
    void OnOpenProject(wxCommandEvent& event);
    void CloseProject();
    void OnAppStartAll(wxCommandEvent& event);
    void OnAppStopAll(wxCommandEvent& event);
    void OnSalesAnalyst(wxCommandEvent& event);
    void OnPreferences(wxCommandEvent& event);

private:
    // any class wishing to process wxWidgets events must use this macro
    wxDECLARE_EVENT_TABLE();
    
    // event handlers
    void OnMenu(wxCommandEvent& event);
    void OnSaveAS(wxCommandEvent& event);
    void OnDonate(wxCommandEvent& event);
    void OnCreateProject(wxCommandEvent& event);
    void OnClose(wxCloseEvent& event);
    
    virtual wxToolBar* OnCreateToolBar(long style, wxWindowID winid, const wxString &name);
    
    wxWindow*   CreateNotebook();
    
    virtual void   AUICreate();
    
    MyStatusBar*    m_statusbar;
    wxTimer         *m_Timer;
    
    kWookaProjectInfo* m_wooka;
    bool            m_running;
    bool            m_topspeed;
    long            m_interval;
    long            m_tics;
    
    wxWindow*  m_notebook;
    //AUI Section
    wxAuiManager m_mgr;

    wxAuiToolBar* m_toolbar;
        
    kWookaApplicationListPanel* m_listPanel;
    kWookaApplicationViewPanel* m_ViewPanel;
    kWookaTaskbarIcon* m_taskbarIcon;

    wxString m_projectFile;
    wxString m_projectPath;
};

#endif /* life_frame_hpp */
