/********************************************************************************
*                                                                               *
* life_ids.h --          The common IDS for some windows                        *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/



#ifndef life_ids_h
#define life_ids_h

#include "wx/defs.h"

#ifndef MIN
#define MIN(x,y)   (x > y ? y : x)
#endif

#ifndef MAX
#define MAX(x,y)   (x > y ? x : y)
#endif

#define  USE_SQLITE3     0

#define  SQLITE_SCHEMAV2 0

#define  MAP_USE_DIALOG  0

#define  USE_THUMB_V2    1

enum
{
    // timer
    ID_TIMER = wxID_HIGHEST,
    ID_TIMER_HOOK,
    ID_TIMER_FRAME,
    THREAD_RESIZE_UPDATE,
    THREAD_PROGRESS_EVENT,
    THREAD_COMPLETED_OK_EVENT,
    THREAD_COMPLETED_FAILED_EVENT,
    THREAD_DISABLECANCEL_EVENT,
    THREAD_ENABLECANCEL_EVENT,
	THREAD_SCROLLBAR_EVENT,
    // file menu
    ID_SAMPLES,
    
    // view menu
    ID_SHOWNAV,
    ID_INFO,
    
    ID_ZOOM_IN,
    ID_ZOOM_OUT,
    
    ID_COPY,
    ID_EXPORT,
    ID_SELECT_ALL,
    ID_SHOW_HIDDEN_DIE,
    ID_NOTEBOOK,
    ID_SelectAll,
    ID_UnSelect,
    ID_Inverted,
    ID_CMD_SAVE,
    ID_CMD_CLOSE,
    ID_CMD_OPEN,
    ID_CMD_CREATE,
    ID_CMD_PERFS,
    ID_START_ALL,
    ID_INVOTORY,
    ID_STOCK,
    ID_DAILY_ACCOUNT,
    ID_STOP_ALL,
    EVENT_LOAD_ORDER,
    //ID of panel
    ID_PANEL_NGINX_CONFIG,
    ID_PANEL_NGINX_SERVICE_CONFIG,
    ID_PANEL_MAVEN_CONFIG,
    ID_PANEL_JAVA_CONFIG,
    ID_PANEL_MYSQL_CONFIG,
    ID_PANEL_REDIS_CONFIG,
    ID_PANEL_SPRING_CONFIG,
    ID_PANEL_DONATE,
    ID_PANEL_NGINX_SERVICE,
    ID_PANEL_NGINX_SERVER,
    ID_PANEL_GENERIC_JAVA,
    // speed selection slider
    ID_SLIDER,
    ID_Sample,
    ID_MAX_OF_LIFE
};


#ifdef WIN32
#define WXSTRDUP(x) _strdup(x)
#else
#define WXSTRDUP(x) strdup(x)
#endif

#define kPosDefaultFont(fs)  wxFont(fs, wxFontFamily::wxFONTFAMILY_SWISS, wxFontStyle::wxFONTSTYLE_NORMAL, wxFontWeight::wxFONTWEIGHT_NORMAL, false)

#define kPosLabelSize()   wxSize(180, 35)

class kPosEventObject : public wxObject {
    
private:
    wxString kEventContent;
    wxInt32  kEventId;

public:
    kPosEventObject(wxString& ec, wxInt32 eid = 0){
        kEventContent = ec;
        kEventId = eid;
    }
    
    wxString& GetEventContent() {
        return kEventContent;
    }
    
    wxInt32 GetEventId() {
        return kEventId;
    }
};

#endif /* life_ids_h */
