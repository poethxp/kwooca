/////////////////////////////////////////////////////////////////////////////
// Name:        xwafer/life_readline.hpp
// Purpose:     XWafer
// Author:      Long Zou
// Modified by:
// Created:     July/2018
// Copyright:   (c) 2018, Kiloseed Co.,Ltd
// Licence:     Commeric Licence
/////////////////////////////////////////////////////////////////////////////

#ifndef life_readline_h
#define life_readline_h

#include <stdio.h>
#ifdef WIN32
#include <windows.h>
#endif



#ifdef __cplusplus
extern "C" {
#endif

typedef struct _LifeFile{
#ifdef WIN32
	HANDLE fd;
	HANDLE maphandle;
	size_t  file_size;
	size_t  offset;
#else
    int fd;
	off_t file_size;
	off_t offset;
#endif
    void* map_addr;
    unsigned char* pointer;
} LifeFile, *LifeFilePointer;

    
bool path_of_file(char* pbuf, size_t buflen, const char* psrc);

LifeFile* life_fileopen(const char* filename);
 
void life_fileclose(LifeFile* lf);

int life_fileend(LifeFile* lf);

size_t  life_readline(LifeFile* lf, unsigned char* buff, size_t len);
    
int life_startwith(const char* mbstr, const char* compared);
    
int life_progress(LifeFile* lf);

/**
 * The separator should be a array of string and the last element should be NULL.
 **/
int life_split_ns(char ***dest, int *count, char *s_str,const char **separator);
    
int life_split(char ***dest, int *count, char *s_str,const char **separator, int number_separators, int compress_separator, int keep_separator);

int life_freesplit(char **dest, int count);
    
#ifdef __cplusplus
}
#endif
#endif /* life_readline_h */
