/********************************************************************************
*                                                                               *
* kwooka.h --       The kwooka app                                              *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/



#ifndef _KWOOKA_APP_H_
#define _KWOOKA_APP_H_

#include "wx/language.h"
#include "wx/minifram.h"
#include "wx/stack.h"
#include "wx/clipbrd.h"
#include "wx/preferences.h"
#include "wx/scopedptr.h"

#include <wx/snglinst.h>

#ifdef WIN32
#define PATH_SEPERATOR wxT("\\")
#define PATH_SEPERATOR_CHAR wxT('\\')
#else
#define PATH_SEPERATOR wxT("/")
#define PATH_SEPERATOR_CHAR wxT('/')
#endif

#include <stdlib.h>    
#include <crtdbg.h>

class kWookaFrame;
class kWookaJvmConfigInfo;
class kWookaProjectInfo;
// --------------------------------------------------------------------------
// LifeApp
// --------------------------------------------------------------------------

class kWookaApp : public wxApp
{
private:
    wxScopedPtr<wxPreferencesEditor> m_prefEditor;
    wxLocale* m_locale;
    wxSingleInstanceChecker* m_checker;
    kWookaFrame* m_mainFrame;
    
public:
    virtual bool OnInit();
	virtual int  OnExit();
        
    // void ShowPreferencesEditor(wxWindow* parent);
    void ShowPreferencesEditor(wxWindow* parent, kWookaProjectInfo* proj);
    void DismissPreferencesEditor();
    
    void SelectLanguage(int lang);
    
    void ShowMainFrame();
    
    void OnMainFrameClosed();

};

extern kWookaApp& wxGetApp();

#endif  // _LIFE_APP_H_
