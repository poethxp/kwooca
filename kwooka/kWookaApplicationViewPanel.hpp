/********************************************************************************
*                                                                               *
* kWookaApplicationViewPanel.hpp -- Major view panel                            *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef kWookaApplicationViewPanel_HPP
#define kWookaApplicationViewPanel_HPP


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include "wx/aui/aui.h"

class kWookaFrame;
class kWookaAppItemInfo;
class kWookaProjectInfo;
class kWookaApplicationMonitorPanel;
class kWookaApplicationLoggerPanel;
class kWookaApplicationConfigPanel;

////Dialog Style Start
#undef kWookaApplicationViewPanel_STYLE
#define kWookaApplicationViewPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End


class kWookaApplicationViewPanel : public wxPanel
{
    private:
        DECLARE_EVENT_TABLE();
        
    public:
        kWookaApplicationViewPanel(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("kWooca"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationViewPanel_STYLE);
        virtual ~kWookaApplicationViewPanel();
    
        wxAuiNotebook* CreateNotebook();
    
        void SetWookaFrame(kWookaFrame* lf);
        kWookaFrame* GetWookaFrame();

        void UpdateAppItem(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem);

        bool AddPanel(wxPanel* panel, const wxString title);
        bool ExistPanel(wxWindowID winId, bool moveCurrent = false);

        wxWindow* FindOrCreatePanel(wxWindowID id);

        kWookaAppItemInfo* GetAppItem() {
            return m_appitem;
        }

    private:
        //Do not add custom control declarations between
        //GUI Control Declaration Start and GUI Control Declaration End.
        //wxDev-C++ will remove them. Add custom code after the block.
        ////GUI Control Declaration Start
    
        ////GUI Control Declaration End
    private:
        //Note: if you receive any error with these enum IDs, then you need to
        //change your old form code that are based on the #define control IDs.
        //#defines may replace a numeric value for the enum names.
        //Try copy and pasting the below block in your old form header files.
        enum
        {
            ////GUI Enum Control ID Start
            ID_WXBUTTON_DELETE_RECORD = 2039,
            ID_WXEDIT8 = 2029,
            ID_NOTEBOOK = 2022,
            ID_WXSTATICTEXT_SPACE = 2020,
            ID_WXEDIT7 = 2019,
            ID_WXSTATICTEXT_MEMBER_NAME = 2018,
            ID_WXEDIT6 = 2017,
            ID_WXSTATICTEXT_MEMBER_CARD = 2016,
            ID_WXEDIT5 = 2015,
            ID_WXSTATICTEXT_ORDERNAME = 2013,
            ID_WXEDIT4 = 2012,
            ID_WXSTATICLABEL_DATE = 2011,
            ID_WXEDIT3 = 2010,
            ID_WXEDIT2 = 2009,
            ID_WXSTATICLABEL_STATUS = 2008,
            ID_WXBUTTON_ADDTOCART = 2005,
            ID_WXEDIT1 = 2004,
            ID_WXSTATICLABEL_ORDERNO = 2003,
            ID_WXSTATICBOX1 = 2002,
            ID_WXGRID1 = 2001,
            ////GUI Enum Control ID End
            ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
        };
        
    private:
        void OnKeyCharEvent(wxKeyEvent& event);
        void OnClose(wxCloseEvent& event);
        void OnNotePageChange(wxAuiNotebookEvent& event);
        void OnNotePageClose(wxAuiNotebookEvent& event);
        void CreateGUIControls();
        wxAuiNotebook* m_notebook;
        void CreateContentPanel();

        kWookaFrame* m_wookaframe;
        kWookaAppItemInfo* m_appitem;
        kWookaProjectInfo* m_project;

        kWookaApplicationMonitorPanel* m_monitorpanel;
        // kWookaApplicationConfigPanel* m_configpanel;
        kWookaApplicationLoggerPanel* m_loggerpanel;

        wxVector<wxPanel*> m_panels;
};


#endif /* kPosAccountingPanel_hpp */
