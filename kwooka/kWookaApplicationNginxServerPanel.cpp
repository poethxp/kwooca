﻿/********************************************************************************
*                                                                               *
* kWookaApplicationNginxServerPanel.cpp -- a custom config for nginx server node*
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationNginxServerPanel.hpp"

#include "kWooka_ids.h"
#include "kWookaExecuteProcess.hpp"
#include "kWookaProjectInfo.hpp"

BEGIN_EVENT_TABLE(kWookaApplicationNginxServerPanel, wxPanel)
////Manual Code Start
EVT_BUTTON(ID_WXBUTTON_BROWSE, kWookaApplicationNginxServerPanel::OnBrowse)
EVT_BUTTON(ID_WXBUTTON_TEST, kWookaApplicationNginxServerPanel::OnTest)

////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()



kWookaApplicationNginxServerPanel::kWookaApplicationNginxServerPanel(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationNginxServerPanel::~kWookaApplicationNginxServerPanel()
{
    wxLogDebug(wxT("kWookaApplicationNginxServerPanel destroyed."));
}

void kWookaApplicationNginxServerPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();

    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationNginxServerPanel::CreateContentPanel() {
    
    m_NginxServerName = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_NginxListenPort = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_NginxServerOptions = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24), wxTE_MULTILINE);
    m_NginxIPv6 = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    m_NginxServerOptions->SetMaxLength(wxINT16_MAX);
    m_NginxServerName->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServerPanel::OnServerNameChange), NULL, this);
    m_NginxListenPort->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServerPanel::OnListenPortChange), NULL, this);
    m_NginxServerOptions->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServerPanel::OnServerOptionsChange), NULL, this);
    m_NginxIPv6->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationNginxServerPanel::OnIPV6Change), NULL, this);

    WxSource = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    kBtnBrowse = new wxButton(this, ID_WXBUTTON_BROWSE, _("Browse"), wxDefaultPosition, wxSize(100, 20), 0);
    kBtnTest = new wxButton(this, ID_WXBUTTON_TEST, _("Test"), wxDefaultPosition, wxSize(100, 20), 0);
    WxSource->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationNginxServerPanel::OnSourceChange), NULL, this);

    wxStaticText* lblServerName = new wxStaticText(this, wxID_ANY, _("Server Name"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblListenPort = new wxStaticText(this, wxID_ANY, _("Listen Port"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblIPv6Support = new wxStaticText(this, wxID_ANY, _("IPv6 Support"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblServerOptions = new wxStaticText(this, wxID_ANY, _("Server Options"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblNginxSource = new wxStaticText(this, wxID_ANY, _("Nginx HOME"), wxPoint(20, 8), wxSize(140, 24));
    wxBoxSizer* bsSourceInfo = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsServerInfo = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsServerOptions = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsIPv6Support = new wxBoxSizer(wxHORIZONTAL);
    
    bsSourceInfo->Add(lblNginxSource, 0, wxEXPAND | wxALL, 5);
    bsSourceInfo->Add(WxSource, 1, wxEXPAND | wxALL, 5);
    bsSourceInfo->Add(kBtnBrowse, 0, wxEXPAND | wxALL, 5);

    bsServerInfo->Add(lblServerName, 0, wxEXPAND | wxALL, 5);
    bsServerInfo->Add(m_NginxServerName, 2, wxEXPAND | wxALL, 5);
    bsServerInfo->Add(lblListenPort, 0, wxEXPAND | wxALL, 5);
    bsServerInfo->Add(m_NginxListenPort, 2, wxEXPAND |  wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap1, 3, wxSHRINK | | wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap2, 1, wxSHRINK | wxALIGN_CENTER | wxALL, 5);

    bsIPv6Support->Add(lblIPv6Support, 0, wxEXPAND | wxALL, 5);
    bsIPv6Support->Add(m_NginxIPv6, 1, wxEXPAND | wxALL, 5);
    bsIPv6Support->Add(kBtnTest, 0, wxEXPAND | wxALL, 5);

    bsServerOptions->Add(lblServerOptions, 0, wxEXPAND | wxALL, 5);
    bsServerOptions->Add(m_NginxServerOptions, 1, wxEXPAND | wxALL, 5);

    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    boxSizer->Add(bsSourceInfo, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsServerInfo, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsIPv6Support, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsServerOptions, 1, wxEXPAND | wxALL, 5);
    
    
    this->SetSizerAndFit(boxSizer);
}

void kWookaApplicationNginxServerPanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}


void kWookaApplicationNginxServerPanel::OnBrowse(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        wxDirDialog  dirdlg(this, _("Open the NGINX HOME directory"));
        

        if (dirdlg.ShowModal() == wxID_OK) {
            this->WxSource->SetValue(dirdlg.GetPath());
            m_appItem->source = dirdlg.GetPath();
        }
    }
}

void kWookaApplicationNginxServerPanel::OnTest(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        kWookaProcessExecutor* executor = m_project->runtime->GetExecutor(m_appItem->id);
        if (!executor->Test()) {
            wxMessageBox(_("The nginx configuration is bad. The detail information please lookup the log view."));
        }
        else {
            wxMessageBox(_("The nginx configuration test is successful."));
        }
    }
}

void kWookaApplicationNginxServerPanel::UpdateNginxConfig(kWookaProjectInfo* proj, kWookaAppItemInfo* appItem) {
    m_project = proj;
    m_appItem = appItem;
    if (m_appItem != NULL) {
        m_NginxServerName->SetValue(m_appItem->name);
        m_NginxListenPort->SetValue(m_appItem->GetItemValue(wxT("ListenPort")));
        m_NginxServerOptions->SetValue(m_appItem->GetItemValue(wxT("ServerOptions")));
        WxSource->SetValue(m_appItem->source);
        m_NginxIPv6->SetValue(m_appItem->GetItemValue(wxT("IPv6Support")) == wxT("true"));
    }
    else {
        m_NginxServerName->SetValue(wxT(""));
        m_NginxListenPort->SetValue(wxT(""));
        m_NginxServerOptions->SetValue(wxT(""));
        WxSource->SetValue(wxT(""));
        m_NginxIPv6->SetValue(false);
    }
}

void kWookaApplicationNginxServerPanel::OnSourceChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->source = event.GetString();
    }
}

void kWookaApplicationNginxServerPanel::OnServerNameChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->name = event.GetString();    }
}

void kWookaApplicationNginxServerPanel::OnListenPortChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("ListenPort"), event.GetString());
    }
}

void kWookaApplicationNginxServerPanel::OnIPV6Change(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("IPv6Support"), event.IsChecked() ? wxT("true") : wxT("false"));
    }
}

void kWookaApplicationNginxServerPanel::OnServerOptionsChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("ServerOptions"), event.GetString());
    }
}
