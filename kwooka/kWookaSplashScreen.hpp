//
//  kPosSplashScreen.hpp
//  kpos
//
//  Created by ZOU YUNLONG on 2019/10/28.
//  Copyright © 2019 LongZou. All rights reserved.
//

#ifndef kWookaSplashScreen_hpp
#define kWookaSplashScreen_hpp


#include <stdio.h>
#include "wx/minifram.h"
#include "wx/stack.h"
#include "wx/clipbrd.h"
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/statline.h"
#include "wx/wfstream.h"
#include "wx/filedlg.h"
#include "wx/stockitem.h"
#include "wx/dcbuffer.h"
#include "wx/tipwin.h"
#include "wx/metafile.h"
#include "wx/treectrl.h"
#include "wx/aui/aui.h"
#include "wx/choice.h"
#include "kWooka_ids.h"
#include <map>


class kWookaInitialModuleTask : public wxThread {
private:
	bool running;
	wxEvtHandler* m_handler;
public:
    kWookaInitialModuleTask(wxEvtHandler* hh = NULL);
	~kWookaInitialModuleTask();


	virtual wxThread::ExitCode Entry();
};

class kWookaSplashScreenFrame : public wxFrame
{
public:
        // ctor and dtor
        kWookaSplashScreenFrame();
        virtual ~kWookaSplashScreenFrame();
    
        // member functions
        void UpdateUI();
public:
    enum {
        ID_TIMER = 1001,
		ID_COMMAND_INIT_OK,
        ID_MENU_EXIT
    };
private:
    wxBitmap m_bmp;
    bool m_hasShape;

    void OnPaint(wxPaintEvent& event);
    
	void OnInitializedCompleted(wxCommandEvent& event);
    
    void SetWindowShape();
    #ifdef __WXGTK__
    void OnWindowCreate(wxWindowCreateEvent& event);
    #endif
    DECLARE_EVENT_TABLE()
};


#endif /* kPosSplashScreen_hpp */
