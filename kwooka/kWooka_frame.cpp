/********************************************************************************
*                                                                               *
* kwooka_frame.cpp --       The main frame                                      *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWooka_frame.hpp"

#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/statline.h"
#include "wx/wfstream.h"
#include "wx/filedlg.h"
#include "wx/stockitem.h"
#include "wx/dcbuffer.h"
#include "wx/tipwin.h"
#include "wx/metafile.h"
#include "wx/artprov.h"
#include "wx/wxhtml.h"
#include "wx/tglbtn.h"
#include <stdio.h>
#include "kWooka.h"
#include "kWooka_aboutdlg.h"
#include "kWooka_perferences.hpp"
#include <wx/xrc/xmlres.h>

#if defined(__WXGTK__) || defined(__WXMOTIF__) || defined(__WXMAC__) || defined(__WXMGL__) || defined(__WXX11__) || defined(__WXMSW__)
// application icon
#include "kWooka.xpm"

#endif

#include "wx/config.h"
#include "kWooka_ids.h"
#include <shellapi.h>
#include "wx/taskbar.h"

#include "kWookaApplicationListPanel.hpp"
#include "kWookaApplicationViewPanel.hpp"
#include "kWookaTaskbarIcon.hpp"
#include "kWookaDevelopmentPlanPanel.hpp"

#include "wx/webview.h"
#include "wx/msw/webview_edge.h"



// IDs for the controls and the menu commands. Exluding those already defined
// by wxWidgets, such as wxID_NEW.

// some shortcuts
#define ADD_TOOL(id, bmp, tooltip, help) \
    toolBar->AddTool(id, tooltip, bmp, wxNullBitmap, wxITEM_NORMAL, tooltip, help)


// Event tables
wxBEGIN_EVENT_TABLE(kWookaFrame, wxFrame)
    #if wxUSE_FILEDLG
    EVT_MENU            (wxID_OPEN, kWookaFrame::OnOpenProject)
    EVT_MENU            (wxID_NEW, kWookaFrame::OnCreateProject)
    #endif
    EVT_MENU            (wxID_ABOUT, kWookaFrame::OnMenu)
    EVT_MENU            (wxID_EXIT, kWookaFrame::OnMenu)
    EVT_MENU            (wxID_PREFERENCES, kWookaFrame::OnMenu)
    EVT_MENU            (wxID_SAVEAS, kWookaFrame::OnSaveAS)
    EVT_MENU            (wxID_SETUP, kWookaFrame::OnMenu)
    EVT_MENU            (ID_SHOW_HIDDEN_DIE, kWookaFrame::OnMenu)
    EVT_MENU            (ID_SELECT_ALL, kWookaFrame::OnMenu)
    EVT_MENU            (ID_INFO, kWookaFrame::OnMenu)
    EVT_CLOSE           (kWookaFrame::OnClose)
    // EVT_AUINOTEBOOK_PAGE_CHANGED(ID_NOTEBOOK, kWookaFrame::OnPageChanged)
    
    EVT_MENU          (ID_CMD_CREATE, kWookaFrame::OnCreateProject)
    EVT_MENU          (ID_CMD_OPEN, kWookaFrame::OnOpenProject)
    EVT_MENU          (ID_CMD_SAVE, kWookaFrame::OnSaveAS)
    // EVT_MENU          (ID_CMD_CLOSE,  kWookaFrame::OnCloseProject)
    EVT_MENU            (ID_START_ALL, kWookaFrame::OnAppStartAll)
    EVT_MENU            (ID_STOP_ALL, kWookaFrame::OnAppStopAll)
    EVT_MENU            (ID_STOCK, kWookaFrame::OnDonate)
    
wxEND_EVENT_TABLE()

// --------------------------------------------------------------------------
// LifeFrame
// --------------------------------------------------------------------------

// frame constructor
kWookaFrame::kWookaFrame() :
    wxFrame( (wxFrame *) NULL, wxID_ANY, _("kWooca"), wxDefaultPosition, wxSize(1000, 600) )
{
    m_notebook = NULL;
    m_wooka = NULL;
    AUICreate();
    // frame icon
    SetIcon(wxICON(mondrian));
    // menu bar
    wxMenu *menuFile = new wxMenu(wxMENU_TEAROFF);
    wxMenu *menuApps = new wxMenu(wxMENU_TEAROFF);
    //wxMenu *menuView = new wxMenu(wxMENU_TEAROFF);
    //wxMenu *menuEdit = new wxMenu(wxMENU_TEAROFF);
    wxMenu *menuHelp = new wxMenu(wxMENU_TEAROFF);
    

    menuFile->Append(wxID_OPEN, _("&Open\tCtrl-O"), _("&Open\tCtrl-O"));
    menuFile->Append(wxID_NEW, _("&Create &\tCtrl-O"), _("&Create\tCtrl-N"));
    //menuFile->AppendSeparator();
    menuFile->Append(wxID_SAVEAS, _("&Save\tCtrl-S"), _("&Save\tCtrl-S"));
    menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT, _("&Quit\tCtrl-Q"), _("&Quit\tCtrl-Q"));
    
    menuApps->Append(ID_START_ALL, _("Start &All\tCtrl-T"), _("Start &All\tCtrl-T"));
    menuApps->Append(ID_STOP_ALL, _("S&top All\tCtrl-P"), _("S&top All\tCtrl-P"));
    menuApps->Append(wxID_PREFERENCES, _("Preferences"));
    
    
    menuHelp->Append(wxID_ABOUT, _("&About\tCtrl-Shift-A"), _("Show kWooca About"));
    
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(menuFile, _("&File"));
    menuBar->Append(menuApps, _("&Applications"));
    menuBar->Append(menuHelp, _("&Help"));
    SetMenuBar(menuBar);
    
    

    Connect(wxID_PREFERENCES,
            wxEVT_MENU,
            wxCommandEventHandler(kWookaFrame::OnPreferences), NULL, this);

    m_statusbar = new MyStatusBar(this, wxID_ANY);
    m_statusbar->setGaugeValue(0);
    this->SetStatusBar(m_statusbar);
    
    SetStatusText(_("Welcome kWooca! Ready."));
    
    this->Maximize();

    m_taskbarIcon = new kWookaTaskbarIcon();
    m_taskbarIcon->SetWookaFrame(this);
    if (!m_taskbarIcon->SetIcon(wxICON(mondrian),
        _("kWooca manages SpringBoot runtime enviouments")))
    {
        wxLogError("Could not set icon.");
    }
    

    

#if defined(__WXOSX__) && wxOSX_USE_COCOA
    m_dockIcon = new kWookaTaskbarIcon(wxTBI_DOCK);
    if (!m_dockIcon->SetIcon(wxICON(mondrian)))
    {
        wxLogError("Could not set icon.");
    }
#endif

    UpdateUI();
}

    kWookaFrame::~kWookaFrame()
{
    delete m_taskbarIcon;

    if (m_wooka != NULL) {
        delete m_wooka;
        m_wooka = NULL;
    }
    if (m_notebook != NULL) {
        delete m_notebook;
        m_notebook = NULL;
    }
    if (m_toolbar != NULL) {
        delete m_toolbar;
        m_toolbar = NULL;
    }

    m_mgr.UnInit();
}


// Enable or disable tools and menu entries according to the current
// state. See also wxEVT_UPDATE_UI events for a slightly different
// way to do this.
void kWookaFrame::UpdateUI()
{
    if (m_wooka != NULL) {
        m_toolbar->EnableTool(ID_START_ALL, true);
        m_toolbar->EnableTool(ID_STOP_ALL, true);
        m_toolbar->EnableTool(wxID_PREFERENCES, true);
        m_toolbar->EnableTool(ID_CMD_SAVE, true);
        m_toolbar->EnableTool(ID_CMD_CLOSE, true);
        m_mgr.Update();

        GetMenuBar()->Enable(wxID_PREFERENCES, true);
        GetMenuBar()->Enable(ID_START_ALL, true);
        GetMenuBar()->Enable(ID_STOP_ALL, true);
    }
    else {
        m_toolbar->EnableTool(ID_START_ALL, false);
        m_toolbar->EnableTool(ID_STOP_ALL, false);
        m_toolbar->EnableTool(wxID_PREFERENCES, false);
        m_toolbar->EnableTool(ID_CMD_SAVE, false);
        m_toolbar->EnableTool(ID_CMD_CLOSE, false);
        m_mgr.Update();
        GetMenuBar()->Enable(wxID_PREFERENCES, false);
        GetMenuBar()->Enable(ID_START_ALL, false);
        GetMenuBar()->Enable(ID_STOP_ALL, false);
    }

}

// Event handlers -----------------------------------------------------------
void kWookaFrame::OnPreferences(wxCommandEvent& WXUNUSED(event)) {
    if (m_wooka != NULL) {
        wxGetApp().ShowPreferencesEditor(this, m_wooka);
    }
}

// OnMenu handles all events which don't have their own event handler
void kWookaFrame::OnMenu(wxCommandEvent& event)
{
    switch (event.GetId())
    {
            
        case wxID_ABOUT:
        {
            kWookaAboutDialog dialog(this);
            dialog.ShowModal();
            break;
        }
        case wxID_EXIT:
        {
            // true is to force the frame to close
            if (GetProjectInfo() != NULL) {
                if (GetProjectInfo()->runtime->IsRunning()) {
                    if (wxYES == wxMessageBox(_("There is at least one apps running. Do you want to stop these applications when you quit kWooca?"), wxT("Quit kWooca"), wxYES_NO | wxCENTRE)) {
                        OnAppStopAll(event);
                        wxGetApp().Exit();
                    }
                    else {
                        Close(true);
                        return;
                    }
                }
            }
            wxGetApp().Exit();
            break;
        }
    }
}


void kWookaFrame::OnSaveAS(wxCommandEvent& WXUNUSED(event)){
    if (m_projectFile.Length() > 0 && m_wooka != NULL) {
        m_wooka->Save(m_projectFile);
    }
}



#if wxUSE_FILEDLG


void kWookaFrame::BackendOpenFile(const wxString& WXUNUSED(file), uint32_t WXUNUSED(fc)){
    
}

void kWookaFrame::OnOpenProject(wxCommandEvent& event)
{
    // We should do some prepare works
    if (GetProjectInfo() != NULL) {
        if (GetProjectInfo()->runtime->IsRunning()) {
            wxMessageBox(_("There is at least one application running. Please stop all applications before this operation."), wxT("Create kWooca"), wxOK | wxCENTRE);
            return;
        }
        OnSaveAS(event);
        CloseProject();
    }

    wxDirDialog  dirdlg(this, _("Select a project directory"));
    if (dirdlg.ShowModal() == wxID_OK) {
        wxString filePath = dirdlg.GetPath();
        wxString fst = wxFindFirstFile(filePath + "\\wooka.json", wxFILE | wxDIR);
        wxString nxt = wxFindNextFile();
        if (fst.length() > 0 ) {
            wxString projfile = filePath + "\\wooka.json";
            this->m_wooka = new kWookaProjectInfo();
            m_projectFile = projfile;
            m_projectPath = filePath;
            m_wooka->Load(m_projectFile);
            m_listPanel->SetProjectInfo(this->m_wooka);
            m_listPanel->UpdatePanels();
            UpdateUI();
        }
        else {
            wxMessageBox(_("There is no wooka.json file found. Please choose another workspace."));
        }
    }
}
#endif


void kWookaFrame::OnClose(wxCloseEvent& WXUNUSED(event))
{
    // Stop if it was running; this is absolutely needed because
    // the frame won't be actually destroyed until there are no
    // more pending events, and this in turn won't ever happen
    // if the timer is running faster than the window can redraw.
    this->Hide();
}

wxToolBar* kWookaFrame::OnCreateToolBar(long WXUNUSED(style), wxWindowID WXUNUSED(winid), const wxString& WXUNUSED(name)){
    return new wxToolBar(this, wxID_ANY,wxDefaultPosition,wxDefaultSize,wxTB_HORIZONTAL|wxNO_BORDER | wxTB_TEXT);
}


void kWookaFrame::AUICreate(){
    m_mgr.SetManagedWindow(this);
    //kPosAuiToolBarArt
    wxAuiToolBar* tb1 = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
                                         wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_TEXT | wxAUI_TB_PLAIN_BACKGROUND);
    tb1->SetToolBitmapSize(wxSize(48,48));
    //wxBitmap cashBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_ICON_CASH"));// wxBitmap(_T("images/kpos_cash_icon.png"), wxBITMAP_TYPE_PNG);
    //wxBitmap alipayBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_ICON_CASH"));// wxBitmap(_T("images/kpos_alipay_icon.png"), wxBITMAP_TYPE_PNG);
    //wxBitmap weixinBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_ICON_CASH"));// wxBitmap(_T("images/kpos_weixin_icon.png"), wxBITMAP_TYPE_PNG);
    //wxBitmap accountBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_ICON_ACCOUNT"));// wxBitmap(_T("images/kpos_account_icon.png"), wxBITMAP_TYPE_PNG);
    //wxBitmap analystBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_ICON_ANALYST"));// wxBitmap(_T("images/kpos_analyst_icon.png"), wxBITMAP_TYPE_PNG);
    //wxBitmap prefsBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_ICON_PREFS"));// wxBitmap(_T("images/kpos_prefs_icon.png"), wxBITMAP_TYPE_PNG);

    wxBitmap startBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_APP_START_X24"));// wxBitmap(_T("images/kpos_prefs_icon.png"), wxBITMAP_TYPE_PNG);
    wxBitmap stopBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_APP_STOP_X24"));// wxBitmap(_T("images/kpos_prefs_icon.png"), wxBITMAP_TYPE_PNG);
    wxBitmap donateBmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_APP_DONATE_ICON"));// wxBitmap(_T("images/kpos_prefs_icon.png"), wxBITMAP_TYPE_PNG);
    
    tb1->SetBackgroundColour(wxColour(123, 223, 33));
    
    

    tb1->AddTool(ID_CMD_CREATE, _("CREATE"), wxArtProvider::GetBitmap(wxART_NEW_DIR));
    tb1->AddTool(ID_CMD_OPEN, _("OPEN"), wxArtProvider::GetBitmap(wxART_FILE_OPEN));
    tb1->AddTool(ID_CMD_SAVE, _("SAVE"), wxArtProvider::GetBitmap(wxART_FILE_SAVE_AS));
    // tb1->AddTool(ID_CMD_CLOSE, _("CLOSE"), cashBmp);
    
    tb1->AddSeparator();
    tb1->AddTool(ID_START_ALL, _("START ALL"), startBmp); //wxArtProvider::GetBitmap(wxART_GO_HOME));
    tb1->AddTool(ID_STOP_ALL, _("STOP ALL"), stopBmp);// wxArtProvider::GetBitmap(wxART_FILE_SAVE_AS));

    tb1->AddSeparator();
    tb1->AddTool(wxID_PREFERENCES, _("Preferences"), wxArtProvider::GetBitmap(wxART_PRINT)); //wxArtProvider::GetBitmap(wxART_EXECUTABLE_FILE));
    //tb1->AddTool(ID_COPY, wxT("Copy"), wxArtProvider::GetBitmap(wxART_GO_FORWARD));
    //tb1->SetCustomOverflowItems(prepend_items, append_items);
    tb1->AddSeparator();
    

    tb1->AddTool(ID_STOCK, _("Donate"), donateBmp);

    tb1->Realize();
   
    m_toolbar = tb1;
    
    m_mgr.AddPane(tb1, wxAuiPaneInfo().
                  Name(wxT("tb1")).Caption(wxT("Toolbar")).
                  ToolbarPane().Top().Row(1).Floatable(false).Dockable(false).DockFixed(true));
   
    /*
     *  Web Support for Ad, add it later.
     */
#ifdef KWOOKA_USE_EDGE_VIEW
     wxWebViewEdge* webView = new wxWebViewEdge(this, wxID_ANY, wxT("https://www.enjoylost.com/views/ad.html"), wxDefaultPosition, wxSize(600, 60), 0, wxWebViewNameStr);

     webView->AlwaysShowScrollbars(false, false);
     m_mgr.AddPane(webView, wxAuiPaneInfo().Name(wxT("webad")).Caption(wxT("WebADView"))
        .ToolbarPane().Top().Row(1).Maximize().Floatable(false).Dockable(false).DockFixed(true));
#endif
    /**/
    // m_notebook = CreateNotebook();

    CreateNotebook();
    
    //m_mgr.AddPane(CreateLeftPanel(), wxAuiPaneInfo().
    //              Name(wxT("test_panel")).Caption(wxT("Wafer Test")).
    //              Left().Layer(1).Position(1).
    //              CloseButton(false).MaximizeButton(false));
    
    //m_mgr.AddPane(CreateHTMLCtrl(this), wxAuiPaneInfo().Name(wxT("html_content")).
    //              CenterPane());
    
    //m_mgr.AddPane(m_notebook, wxAuiPaneInfo().Name(wxT("notebook_content")).
    //              CenterPane().PaneBorder(false));

    
    //CreateStatusBar(3);
    //SetStatusText(_("Welcome to X-Wafer Inspector!"));
    
    m_mgr.Update();
}

/*
wxWindow* LifeFrame::CreateNotebook(){
    wxSize client_size = GetClientSize();

    wxBitmap page_bmp = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16));
    
    //m_wafercanvas = CreateWaferCanvasCtrl(ctrl);
    //kPosReceiveFrame* kPosRecPanel = new kPosReceiveFrame(ctrl);
    
    //ctrl->AddPage(page, "");
    wxSplitterWindow* splitter = new wxSplitterWindow(this, wxID_ANY,
        wxDefaultPosition, wxDefaultSize,
        wxSP_THIN_SASH | wxSP_LIVE_UPDATE |
        wxCLIP_CHILDREN);
    splitter->SetSashPosition(100);
    splitter->SetSashGravity(1.0);
    kWookaApplicationViewPanel* appView = new kWookaApplicationViewPanel(splitter);
    
    m_ViewPanel = appView;
    m_ViewPanel->SetLifeFrame(this);

    m_listPanel = new kWookaApplicationListPanel(splitter);
    m_listPanel->SetLifeFrame(this);

    splitter->SplitVertically(m_listPanel, appView, 100);


    // ctrl->AddPage(splitter, _("Alipay") , true, page_bmp);
    
    // ctrl->Thaw();
    return splitter;
}
*/

wxWindow* kWookaFrame::CreateNotebook() {
    wxSize client_size = GetClientSize();

    wxBitmap page_bmp = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16, 16));

    //m_wafercanvas = CreateWaferCanvasCtrl(ctrl);
    //kPosReceiveFrame* kPosRecPanel = new kPosReceiveFrame(ctrl);

    //ctrl->AddPage(page, "");
    kWookaApplicationViewPanel* appView = new kWookaApplicationViewPanel(this);

    m_ViewPanel = appView;
    m_ViewPanel->SetWookaFrame(this);

    m_listPanel = new kWookaApplicationListPanel(this);
    m_listPanel->SetWookaFrame(this);

    m_mgr.AddPane(m_listPanel, wxAuiPaneInfo().Name(wxT("left_content")).Left().PaneBorder(false).CloseButton(false).CaptionVisible(false));
    m_mgr.AddPane(m_ViewPanel, wxAuiPaneInfo().Name(wxT("notebook_content")).
        CenterPane().PaneBorder(false).CloseButton(false));

    return NULL;
}

void kWookaFrame::CloseProject() {
    // check the the project was loaded
    // check the application was running
    if (GetProjectInfo() != NULL) {
        if (GetProjectInfo()->runtime->IsRunning()) {
            wxMessageBox(_("There is at least one application running. Please stop all applications before this operation."), wxT("Create kWooca"), wxOK | wxCENTRE);
            return;
        } else {
            kWookaProjectInfo* wooka = m_wooka;
            m_wooka = NULL;
            GetListPanel()->SetProjectInfo(NULL);
            GetViewPanel()->UpdateAppItem(NULL, NULL);
            UpdateUI();
            delete wooka;
        }
    }
}



void kWookaFrame::OnAppStartAll(wxCommandEvent& WXUNUSED(event)) {
    wxLogDebug(wxT("Application to be started all"));
    if (m_wooka != NULL) {
        GetListPanel()->StartAllApps();
    }
}

void kWookaFrame::OnAppStopAll(wxCommandEvent& WXUNUSED(event)) {
    wxLogDebug(wxT("Application to be stopped all"));
    if (m_wooka != NULL) {
        GetListPanel()->StopAllApps();
    }
}

void kWookaFrame::OnDonate(wxCommandEvent& WXUNUSED(event)) {
    if (this->m_ViewPanel != NULL) {
        if (!m_ViewPanel->ExistPanel(ID_PANEL_DONATE, true)) {
            kWookaDevelopmentPlanPanel* plan = new kWookaDevelopmentPlanPanel(m_ViewPanel);
            if (!m_ViewPanel->AddPanel(plan, _("Donate"))) {
                delete plan;
            }
        }
    }
}

void kWookaFrame::OnSalesAnalyst(wxCommandEvent& WXUNUSED(event)) {

}

kWookaApplicationViewPanel* kWookaFrame::GetViewPanel() {
    return this->m_ViewPanel;
}

kWookaApplicationListPanel* kWookaFrame::GetListPanel() {
    return this->m_listPanel;
}

/**
 * Order Query and WAITING Pay Order
 */
void kWookaFrame::OnCreateProject(wxCommandEvent& event) {
    if (GetProjectInfo() != NULL) {
        if (GetProjectInfo()->runtime->IsRunning()) {
            wxMessageBox(_("There is at least one application running. Please stop all applications before this operation."), wxT("Create kWooca"), wxOK | wxCENTRE);
            return;
        }
        OnSaveAS(event);
        CloseProject();
    }

    wxDirDialog  dirdlg(this, _("Select a project directory"));
    if (dirdlg.ShowModal() == wxID_OK) {
        wxString filePath = dirdlg.GetPath();
        wxString fst = wxFindFirstFile(filePath + "\\*", wxFILE | wxDIR);
        wxString nxt = wxFindNextFile();
        if (fst.length() > 0 || nxt.length() > 0) {
            wxMessageBox(_("This directory contains some file or sub-directory. Please choose an empty directory."));
        }
        else {
            // create a json file into this folder
            wxString projfile = filePath + "\\wooka.json";
            this->m_wooka = new kWookaProjectInfo();
            m_projectFile = projfile;
            m_projectPath = filePath;
            m_wooka->workspace = filePath;
            this->m_wooka->Save(projfile);
            m_listPanel->SetProjectInfo(this->m_wooka);
            UpdateUI();
        }
    }
    
}

#if !defined(MAP_USE_DIALOG) || MAP_USE_DIALOG == 0
void kWookaFrame::OnThumbSize(wxSizeEvent& WXUNUSED(event)){
        
}
#endif


MyStatusBar::MyStatusBar(wxWindow *parent, wxWindowID id /*= wxID_ANY*/, long style /*= wxSTB_DEFAULT_STYLE*/, const wxString& name /*= wxStatusBarNameStr*/)
:wxStatusBar(parent,id,style,name)
{
    this->SetFieldsCount(3);
    int patch[3] = { -1, -1, 240 };
    this->SetStatusWidths(3, patch);
    
    this->gauge = new wxGauge(this, -1, gaugeRange, wxDefaultPosition, wxDefaultSize, wxGA_HORIZONTAL | wxGA_PROGRESS | wxGA_TEXT | wxBG_STYLE_TRANSPARENT);
    this->Bind(wxEVT_SIZE, &MyStatusBar::OnSize, this);
    //this->gauge->SetBackgroundStyle(wxBG_STYLE_TRANSPARENT);
    //this->gauge->SetBackgroundColour(*wxGREEN);
}


void MyStatusBar::setGaugeValue(int n)
{
    if ((n >= 0 && n <= gaugeRange))
    {
        this->gauge->SetValue(n);
    }
}


int MyStatusBar::getGaugeValue()
{
    return this->gauge->GetValue();
}


void MyStatusBar::OnSize(wxSizeEvent& WXUNUSED(event))
{
    wxRect rect;
    this->GetFieldRect(2, rect);
    this->gauge->SetPosition(wxPoint(rect.x + 140, rect.y + 1));
    this->gauge->SetSize(wxSize(rect.width - 140, rect.height - 4));
}

