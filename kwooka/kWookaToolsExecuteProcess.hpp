/********************************************************************************
*                                                                               *
* kWookaToolsExecuteProcess.hpp -- some tools execute factory                   *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaToolsExecuteProcess_hpp
#define kWookaToolsExecuteProcess_hpp

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "kWookaExecuteProcess.hpp"

class kWookaRedisExecutorFactory : public kWookaExecutorFactory {
private:
	bool m_isTestOK;
public:
	kWookaRedisExecutorFactory() {
		m_isTestOK = false;
	}

	virtual bool IsReady() wxOVERRIDE {
		return m_isTestOK;
	}

	virtual bool Prepared(kWookaProcessOutputRender* executor, kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;

	virtual wxString BuildCommandLine(kWookaProjectInfo* proj, kWookaAppItemInfo* app);

	virtual wxProcess* Execute(kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;
};

class kWookaMySQLServerThread : public wxThread, public wxEvtHandler {
private:
	kWookaProcessOutputRender* m_render;
	wxProcess* m_proc;
	bool running;
	wxString configFile;


public:
	kWookaMySQLServerThread(kWookaProcessOutputRender* executor, wxProcess* proc);
	
	~kWookaMySQLServerThread() {
	}

	void OnProcessEnded(wxProcessEvent& evt) {
		m_proc = NULL;
		running = false;
	}

	virtual void* Entry();
};

class kWookaMySQLExecutorFactory : public kWookaExecutorFactory {
private:
	bool m_isTestOK;
public:
	kWookaMySQLExecutorFactory() {
		m_isTestOK = false;
	}

	virtual bool IsReady() wxOVERRIDE {
		return m_isTestOK;
	}

	virtual bool Prepared(kWookaProcessOutputRender* executor, kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;

	virtual wxString BuildCommandLine(kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;

	virtual wxProcess* Execute(kWookaProcessOutputRender* render, kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;
	virtual wxProcess* Execute(kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;

	// void PrepareDatabase(kWookaProjectInfo* proj, kWookaAppItemInfo* app, int flag = 0);

	virtual bool Shutdown(kWookaProcessOutputRender* render, kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;

};

class kWookaNginxExecutorFactory : public kWookaExecutorFactory {
private:
	bool m_isTestOK;
public:
	kWookaNginxExecutorFactory(){
		m_isTestOK = false;
	}

	virtual bool IsReady() wxOVERRIDE {
		return m_isTestOK;
	}

	virtual bool IsMultiProcess() wxOVERRIDE  {
		return true;
	}

	virtual bool Prepared(kWookaProcessOutputRender* executor, kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;

	virtual wxString BuildCommandLine(kWookaProjectInfo* proj, kWookaAppItemInfo* app);

	virtual wxProcess* Execute(kWookaProjectInfo* proj, kWookaAppItemInfo* app) wxOVERRIDE;
};

#endif /* kWookaExecuteProcess_hpp */
