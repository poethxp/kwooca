/********************************************************************************
*                                                                               *
* kWookaApplicationMonitorPanel.hpp -- Show monitor information                 *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef kWookaApplicationMonitorPanel_hpp
#define kWookaApplicationMonitorPanel_hpp


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>


////Dialog Style Start
#undef kWookaApplicationMonitorPanel_STYLE
#define kWookaApplicationMonitorPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

#define ID_EVENT_THREAD_MONITOR  11098

class kWookaAppItemInfo;
class kWookaProcessExecutor;
class kWookaApplicationViewPanel;
class kWookaApplicationMonitorPanel;
class wxLineChartCtrl;
class kWookaProcessPerformanceInfo;


class kWookaMonitorThread : public wxThread {
public:
    kWookaMonitorThread(kWookaApplicationMonitorPanel* monitorpanel): wxThread(wxTHREAD_JOINABLE) {
        m_monitorpanel = monitorpanel;
    }
public:
    virtual wxThread::ExitCode Entry();
    void Shutdown();
private:
    kWookaApplicationMonitorPanel* m_monitorpanel;
    bool m_running;
};

class kWookaApplicationMonitorPanel : public wxPanel
{
    private:
        DECLARE_EVENT_TABLE();
        
    public:
        kWookaApplicationMonitorPanel(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("kpos"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationMonitorPanel_STYLE);
        virtual ~kWookaApplicationMonitorPanel();
        
        void SetViewPanel(kWookaApplicationViewPanel* vp);
        kWookaApplicationViewPanel* GetViewPanel();

        void UpdateAppItem(kWookaAppItemInfo* appItem);
        void DoGetPerformanceInfo();
    private:
        //Do not add custom control declarations between
        //GUI Control Declaration Start and GUI Control Declaration End.
        //wxDev-C++ will remove them. Add custom code after the block.
        ////GUI Control Declaration Start
    
        ////GUI Control Declaration End
                
    private:
        //Note: if you receive any error with these enum IDs, then you need to
        //change your old form code that are based on the #define control IDs.
        //#defines may replace a numeric value for the enum names.
        //Try copy and pasting the below block in your old form header files.
        enum
        {
            ////GUI Enum Control ID Start
            ID_WXBUTTON_DELETE_RECORD = 2039,
            ID_WXEDIT8 = 2029,
            ID_WXSTATICTEXT_SPACE = 2020,
            ID_WXEDIT7 = 2019,
            ID_WXSTATICTEXT_MEMBER_NAME = 2018,
            ID_WXEDIT6 = 2017,
            ID_WXSTATICTEXT_MEMBER_CARD = 2016,
            ID_WXEDIT5 = 2015,
            ID_WXSTATICTEXT_ORDERNAME = 2013,
            ID_WXEDIT4 = 2012,
            ID_WXSTATICLABEL_DATE = 2011,
            ID_WXEDIT3 = 2010,
            ID_WXEDIT2 = 2009,
            ID_WXSTATICLABEL_STATUS = 2008,
            ID_WXBUTTON_ADDTOCART = 2005,
            ID_WXEDIT1 = 2004,
            ID_WXSTATICLABEL_ORDERNO = 2003,
            ID_WXSTATICBOX1 = 2002,
            ID_WXGRID1 = 2001,
            ////GUI Enum Control ID End
            ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
        };
        
    private:
        void OnKeyCharEvent(wxKeyEvent& event);
        void OnClose(wxCloseEvent& event);
        void CreateGUIControls();
    
        void CreateContentPanel();
        void OnPerformanceEvent(wxThreadEvent& event);
        void UpdateMonitorChartData(wxVector<kWookaProcessPerformanceInfo>& perms);

        wxCriticalSection criticalSection;

        kWookaAppItemInfo* m_appitem;
        kWookaMonitorThread m_monitorThread;

        kWookaProcessExecutor* m_executor;
        wxLineChartCtrl* m_memoryLineChartCtrl;
        wxLineChartCtrl* m_cpuUsageLineChartCtrl;
        wxLineChartCtrl* m_diskLineChartCtrl;
        wxLineChartCtrl* m_networkLineChartCtrl;
        wxLineChartCtrl* m_threadLineChartCtrl;
        wxLineChartCtrl* m_handlerLineChartCtrl;

        kWookaApplicationViewPanel* m_viewpanel;
};


#endif /* kPosAccountingPanel_hpp */
