/********************************************************************************
*                                                                               *
* kWookaTaskbarIcon.cpp -- The taskbar support                                  *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#include "kWookaTaskbarIcon.hpp"
#include "kWooka.h"
#include "kWooka_perferences.hpp"
#include <map>
#include "wx/process.h"
#include <wx/xrc/xmlres.h>
#include "kWooka_frame.hpp"

BEGIN_EVENT_TABLE(kWookaTaskbarIcon, wxTaskBarIcon)
    EVT_MENU(ID_MENU_SHOW, kWookaTaskbarIcon::OnMenuShow)
    EVT_MENU(ID_MENU_LOAD, kWookaTaskbarIcon::OnMenuLoad)
    EVT_MENU(wxID_PREFERENCES, kWookaTaskbarIcon::OnMenuPrefs)
    EVT_MENU(ID_MENU_CLOSE, kWookaTaskbarIcon::OnMenuClose)
    EVT_MENU(ID_MENU_STARTALL, kWookaTaskbarIcon::OnMenuStartAll)
    EVT_MENU(ID_MENU_STOPALL, kWookaTaskbarIcon::OnMenuStopAll)
    EVT_MENU(ID_MENU_EXIT, kWookaTaskbarIcon::OnMenuExit)
    EVT_TASKBAR_LEFT_DCLICK(kWookaTaskbarIcon::OnLeftButtonDClick)
END_EVENT_TABLE()


void kWookaTaskbarIcon::OnLeftButtonDClick(wxTaskBarIconEvent&)
{
    if (!m_wookaframe->IsActive()) {
        m_wookaframe->Maximize(true);
        m_wookaframe->Show(true);
    }
}


wxMenu* kWookaTaskbarIcon::CreatePopupMenu()
{
    wxMenu* m_contextMenu = new wxMenu();
    wxMenuItem* menuItemShow = new wxMenuItem(m_contextMenu, ID_MENU_SHOW, _("Show Window"));
    wxMenuItem *menuItemLoad = new wxMenuItem(m_contextMenu, ID_MENU_LOAD, _("Open Project"));
    wxMenuItem *menuItemStart = new wxMenuItem(m_contextMenu, ID_MENU_STARTALL, _("Start All"));
    wxMenuItem *menuItemStop = new wxMenuItem(m_contextMenu, ID_MENU_STOPALL, _("Stop All"));
    wxMenuItem* menuItemPref = new wxMenuItem(m_contextMenu, wxID_PREFERENCES, _("Preference"));
    wxMenuItem* menuItemClose = new wxMenuItem(m_contextMenu, ID_MENU_CLOSE, _("Close Project"));
    wxMenuItem *menuItemExit = new wxMenuItem(m_contextMenu, ID_MENU_EXIT, _("Exit"));
    if (m_wookaframe != NULL) {
        m_contextMenu->Append(menuItemShow);
        m_contextMenu->AppendSeparator();
        m_contextMenu->Append(menuItemLoad);
        menuItemLoad->Enable(m_wookaframe->GetProjectInfo() == NULL);
        m_contextMenu->Append(menuItemClose);
        menuItemClose->Enable(m_wookaframe->GetProjectInfo() != NULL);
        m_contextMenu->Append(menuItemPref);
        menuItemPref->Enable(m_wookaframe->GetProjectInfo() != NULL);
        m_contextMenu->AppendSeparator();
        menuItemStart->Enable(m_wookaframe->GetProjectInfo() != NULL);
        m_contextMenu->Append(menuItemStart);
        menuItemStop->Enable(m_wookaframe->GetProjectInfo() != NULL);
        m_contextMenu->Append(menuItemStop);
        m_contextMenu->AppendSeparator();
        m_contextMenu->Append(menuItemExit);
    }
    return m_contextMenu;
}


void kWookaTaskbarIcon::OnMenuShow(wxCommandEvent& WXUNUSED(event)) {
    if (!m_wookaframe->IsActive()) {
        m_wookaframe->Maximize(true);
        m_wookaframe->Show(true);
    }
}

void kWookaTaskbarIcon::OnMenuLoad(wxCommandEvent& event) {
    if (!m_wookaframe->IsActive()) {
        m_wookaframe->OnOpenProject(event);
    }
}

void kWookaTaskbarIcon::OnMenuStartAll(wxCommandEvent& event) {
    // wxGetApp().ShowPreferencesEditor(this);
    m_wookaframe->OnAppStartAll(event);
}

void kWookaTaskbarIcon::OnMenuExit(wxCommandEvent& event) {
    if (m_wookaframe->GetProjectInfo() != NULL) {
        if (m_wookaframe->GetProjectInfo()->runtime->IsRunning()) {
            if (wxYES == wxMessageBox(_("There is at least one apps running. Do you want to stop these applications when you quit kWooca?"), wxT("Quit kWooca"), wxYES_NO | wxCENTRE)) {
                m_wookaframe->OnAppStopAll(event);
                wxGetApp().Exit();
            }
            else {
                return;
            }
        }
    }
    wxGetApp().Exit();
}



void kWookaTaskbarIcon::OnMenuStopAll(wxCommandEvent& event) {
    m_wookaframe->OnAppStopAll(event);
}

void kWookaTaskbarIcon::OnMenuClose(wxCommandEvent& WXUNUSED(event)) {
    m_wookaframe->CloseProject();
}

void kWookaTaskbarIcon::OnMenuPrefs(wxCommandEvent& event) {
    m_wookaframe->OnPreferences(event);
}
