/********************************************************************************
*                                                                               *
* kWookaDevelopementPlanPanel.cpp -- Show development plan                      *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaDevelopmentPlanPanel.hpp"

#include "kWooka_ids.h"
#include "wxtimeserieschartctrl.h"
#include "wxcharts.h"

#include "wx/wxhtml.h"
#include <wx/xrc/xmlres.h>
#include <wx/xml/xml.h>

#include "kWookaApplicationViewPanel.hpp"
#include "kWooka_frame.hpp"

BEGIN_EVENT_TABLE(kWookaDevelopmentPlanPanel, wxPanel)
    ////Manual Code Start
    ////Manual Code End
    //EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()


kWookaDevelopmentPlanPanel::kWookaDevelopmentPlanPanel(wxWindow *parent, wxWindowID id, const wxString& WXUNUSED(title), const wxPoint &position, const wxSize& size, long style)
: wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaDevelopmentPlanPanel::~kWookaDevelopmentPlanPanel()
{
	wxLogDebug(wxT("kWookaDevelopmentPlanPanel destroyed."));
}

void kWookaDevelopmentPlanPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();
    
    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaDevelopmentPlanPanel::CreateContentPanel() {
	
    wxBoxSizer* boxSizer = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* boxSizer1 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* boxSizer2 = new wxBoxSizer(wxHORIZONTAL);

    char* pt = wxLoadUserResource(wxT("release_node"), wxT("HTMLFILE"));

    wxBitmap bmp = wxXmlResource::Get()->LoadBitmap(wxT("IDB_APP_DONATE"));
    wxString html;
    if (pt != NULL) {
        html = wxString(pt, wxMBConvStrictUTF8());
        delete[] pt;
    }

    
    m_donateImage = new wxStaticBitmap(this, wxID_ANY, bmp, wxDefaultPosition, wxSize(320, 344));
    wxStaticText* textRemard = new wxStaticText(this, wxID_ANY, _("Welcome to use kWooca. And Thanks for you donate this project. We will not leave your support for continie this project. By the way, please look our development plans. Thank you very much again."), wxDefaultPosition, wxSize(300, 344));

    m_htmlPanel = new wxHtmlWindow(this);

    m_htmlPanel->AppendToPage(html);

    boxSizer1->Add(m_donateImage, 0, wxEXPAND | wxTOP, 0);
    boxSizer2->Add(textRemard, 1, wxEXPAND | wxALL, 10);
    boxSizer1->Add(boxSizer2, 1, wxEXPAND | wxALL, 0);
    boxSizer->Add(boxSizer1, 0, wxEXPAND | wxALL, 0);
    boxSizer->Add(m_htmlPanel, 1, wxEXPAND | wxALL, 5);


    this->SetSizerAndFit(boxSizer);

}

void kWookaDevelopmentPlanPanel::OnKeyCharEvent(wxKeyEvent& event){
    if (event.GetKeyCode() == WXK_RETURN){
    }
}

void kWookaDevelopmentPlanPanel::SetViewPanel(kWookaApplicationViewPanel* vp) {
	m_viewpanel = vp;
}

kWookaApplicationViewPanel* kWookaDevelopmentPlanPanel::GetViewPanel() {
	return m_viewpanel;
}

