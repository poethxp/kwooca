﻿/********************************************************************************
*                                                                               *
* kWookaApplicationGenericJavaPanel.cpp -- a generic java config                *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/
#include "kWookaApplicationGenericJavaPanel.hpp"

#include "kWooka_ids.h"
#include "kWookaProjectInfo.hpp"
#include "kWookaApplicationJavaVMPanel.hpp"
#include "kWookaApplicationViewPanel.hpp"
#include <wx/zipstrm.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/dir.h>
#include "kWookaTypedResourceInfo.hpp"


BEGIN_EVENT_TABLE(kWookaApplicationGenericJavaPanel, wxPanel)
////Manual Code Start
////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
EVT_BUTTON(ID_WXBUTTON_ADD_RECORD, kWookaApplicationGenericJavaPanel::OnAddRecord)
EVT_BUTTON(ID_WXBUTTON_DELETE_RECORD, kWookaApplicationGenericJavaPanel::OnDeleteRecord)
EVT_BUTTON(ID_WXBUTTON_APPLY_RECORD, kWookaApplicationGenericJavaPanel::OnApplyGrid)
EVT_BUTTON(ID_WXBUTTON_BROWSE, kWookaApplicationGenericJavaPanel::OnBrowse)

EVT_BUTTON(ID_WXBUTTON_ADDLIB, kWookaApplicationGenericJavaPanel::OnAddLibBrowse)
EVT_BUTTON(ID_WXBUTTON_DELLIB, kWookaApplicationGenericJavaPanel::OnDelLib)

EVT_SIZING(kWookaApplicationGenericJavaPanel::OnResize)
END_EVENT_TABLE()



kWookaApplicationGenericJavaPanel::kWookaApplicationGenericJavaPanel(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationGenericJavaPanel::~kWookaApplicationGenericJavaPanel()
{
    wxLogDebug(wxT("kWookaApplicationGenericJavaPanel destroyed."));
}


void kWookaApplicationGenericJavaPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start
    kSource = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    kBtnBrowse = new wxButton(this, ID_WXBUTTON_BROWSE, _("Browse"), wxDefaultPosition, wxSize(100, 20), 0);
    wxStaticText* lblFatJar = new wxStaticText(this, wxID_STATIC, _("Application Home"), wxDefaultPosition, wxSize(140, 24), 0);
    kSource->SetEditable(false);

    m_AppName = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_Version = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_HomeVariable = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_StartClass = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));

    m_AppName->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationGenericJavaPanel::OnAppNameChange), NULL, this);
    m_HomeVariable->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationGenericJavaPanel::OnHomeVariableChange), NULL, this);
    m_Version->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationGenericJavaPanel::OnAppVersionChange), NULL, this);
    m_StartClass->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationGenericJavaPanel::OnStartClassChange), NULL, this);


    CreateSearchPanel();

    wxSize size = this->GetClientSize();

    WxGrid1 = new wxGrid(this, ID_WXGRID1, wxPoint(10, 370), wxDefaultSize);
    WxGrid1->SetDefaultColSize(100);
    WxGrid1->SetDefaultRowSize(25);
    WxGrid1->SetRowLabelSize(50);
    WxGrid1->SetColLabelSize(25);
    WxGrid1->CreateGrid(0, 3, wxGrid::wxGridSelectRowsOrColumns);
    WxGrid1->SetColLabelValue(0, _("CONFIG KEY"));
    WxGrid1->SetColLabelValue(1, _("VALUE"));
    WxGrid1->SetColLabelValue(2, _("ENV VARIABLE"));
    WxGrid1->SetVirtualSize(0, 2000);



    WxGrid1->SetColSize(0, 450);
    WxGrid1->SetColSize(1, 450);
    WxGrid1->SetColSize(1, 120);
    WxGrid1->EnableEditing(true);

    //WxGrid1->SetColSize(9, 0);

    wxStaticText* lblAppName = new wxStaticText(this, wxID_STATIC, _("Application Name"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblAppVersion = new wxStaticText(this, wxID_STATIC, _("Version"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblHomeVariable = new wxStaticText(this, wxID_STATIC, _("Home Variable"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblStartClass = new wxStaticText(this, wxID_STATIC, _("Start Class"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblEmpty2 = new wxStaticText(this, wxID_STATIC, _(""), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblLibrary = new wxStaticText(this, wxID_STATIC, _("Library"), wxDefaultPosition, wxSize(240, 24), 0);

    
    wxLibList = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, NULL, wxLB_EXTENDED);

    WxBtnAddLib = new wxButton(this, ID_WXBUTTON_ADDLIB, _("ADD"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonADDRECORD"));
    WxBtnDelLib = new wxButton(this, ID_WXBUTTON_DELLIB, _("DELETE"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonADDRECORD"));

    WxBtnDeleteRecord = new wxButton(this, ID_WXBUTTON_DELETE_RECORD, _("DELETE"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonDELETERECORD"));
    WxBtnAddRecord = new wxButton(this, ID_WXBUTTON_ADD_RECORD, _("ADD"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonADDRECORD"));
    WxBtnApplyRecord = new wxButton(this, ID_WXBUTTON_APPLY_RECORD, _("APPLY"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonADDRECORD"));
    wxStaticText* lblConfigList = new wxStaticText(this, wxID_ANY, _("CONFIGURATIONS FOR STARTING THE APPLICATION"), wxPoint(20, 8), wxSize(240, 24));

    wxBoxSizer* itemBoxSizer1 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* itemBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* itemBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* itemBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* itemBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);

    wxBoxSizer* itemBoxSizer8 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* itemBoxSizer9 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* itemBoxSizer10 = new wxBoxSizer(wxHORIZONTAL);


    itemBoxSizer6->Add(lblAppName, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer6->Add(m_AppName, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer6->Add(lblAppVersion, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer6->Add(m_Version, 1, wxEXPAND | wxALL, 5);

    itemBoxSizer7->Add(lblHomeVariable, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer7->Add(m_HomeVariable, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer7->Add(lblStartClass, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer7->Add(m_StartClass, 1, wxEXPAND | wxALL, 5);

    itemBoxSizer4->Add(lblFatJar, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer4->Add(kSource, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer4->Add(kBtnBrowse, 0, wxEXPAND | wxALL, 5);

    itemBoxSizer2->Add(lblConfigList, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer2->Add(WxGrid1, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer3->Add(WxBtnApplyRecord, 0, wxSTRETCH_NOT | wxLEFT, 12);
    itemBoxSizer3->Add(WxBtnAddRecord, 0, wxSTRETCH_NOT | wxLEFT, 12);
    itemBoxSizer3->Add(WxBtnDeleteRecord, 0, wxSTRETCH_NOT | wxLEFT, 12);
    itemBoxSizer2->Add(itemBoxSizer3, 0, wxALIGN_RIGHT | wxRIGHT, 5);
    
    itemBoxSizer10->Add(WxBtnAddLib, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer10->Add(WxBtnDelLib, 1, wxEXPAND | wxALL, 5);

    itemBoxSizer9->Add(lblLibrary, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer9->Add(wxLibList, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer9->Add(itemBoxSizer10, 0, wxEXPAND | wxALL, 5);

    itemBoxSizer8->Add(itemBoxSizer9, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer8->Add(itemBoxSizer2, 1, wxEXPAND | wxALL, 5);
    
    itemBoxSizer1->Add(itemBoxSizer4, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer1->Add(itemBoxSizer6, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer1->Add(itemBoxSizer7, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer1->Add(m_JavaVMConfigPanel, 0, wxEXPAND | wxALL, 0);
    itemBoxSizer1->Add(itemBoxSizer8, 1, wxEXPAND | wxALL, 5);
    //itemBoxSizer1->Add(WxStaticBox2, 0, wxEXPAND | wxTOP,  5);

    this->SetSizerAndFit(itemBoxSizer1);
    //    SetTitle(_("kpos"));
    //    SetIcon(wxNullIcon);
    SetSize(8, 8, 1238, 530);
    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationGenericJavaPanel::CreateSearchPanel() {
    m_JavaVMConfigPanel = new kWookaApplicationJavaVMPanel(this);
    //wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    //boxSizer->Add(m_JavaVMConfigPanel, 0, wxEXPAND | wxALIGN_CENTER | wxALL, 5);
}

void kWookaApplicationGenericJavaPanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}


void kWookaApplicationGenericJavaPanel::OnResize(wxSizeEvent& WXUNUSED(event)) {
    wxSize size = this->GetClientSize();
    WxGrid1->SetColSize(0, 30);
    WxGrid1->SetColSize(1, size.x / 3);
    WxGrid1->SetColSize(2, size.x / 3);
    WxGrid1->SetColSize(3, 150);
}

void kWookaApplicationGenericJavaPanel::SetViewPanel(kWookaApplicationViewPanel* vp) {
    m_viewpanel = vp;
}

kWookaApplicationViewPanel* kWookaApplicationGenericJavaPanel::GetViewPanel() {
    return m_viewpanel;
}

void kWookaApplicationGenericJavaPanel::UpdateAppItem(kWookaAppItemInfo* appitem) {
    m_appItem = appitem;
    wxLibList->Clear();
    if (m_appItem == NULL) {
        this->kSource->SetValue(wxT(""));
        this->m_JavaVMConfigPanel->UpdateJvmConfig(NULL);
        m_AppName->SetValue(wxT(""));
        m_HomeVariable->SetValue(wxT(""));
        m_Version->SetValue(wxT(""));
        m_StartClass->SetValue(wxT(""));
    }
    else {
        this->kSource->SetValue(m_appItem->source);
        m_AppName->SetValue(m_appItem->name);
        m_HomeVariable->SetValue(m_appItem->groupId);
        m_Version->SetValue(m_appItem->version);
        m_StartClass->SetValue(m_appItem->startClass);

        this->m_JavaVMConfigPanel->UpdateJvmConfig(&m_appItem->jvmConfig);
        for (size_t t = 0; t < m_appItem->libraries.Count(); t++) {
            wxString& libfile = m_appItem->libraries[t];
            wxLibList->AppendString(libfile);
        }
    }

    if (WxGrid1 != NULL) {
        if (WxGrid1->GetNumberRows() > 0) {
            WxGrid1->DeleteRows(0, WxGrid1->GetNumberRows());
        }
        if (m_appItem != NULL) {
            for (size_t t = 0; t < m_appItem->appConfigs.Count(); t++) {
                kWookaAppConfigItemInfo ci = m_appItem->appConfigs[t];
                WxGrid1->AppendRows();
                WxGrid1->SetCellValue(t, 0, ci.configKey);
                WxGrid1->SetCellValue(t, 1, ci.configValue);
                WxGrid1->SetCellValue(t, 2, wxT("env") == ci.configType ? wxT("1") : wxT(""));
                WxGrid1->SetCellEditor(t, 2, new wxGridCellBoolEditor());
                WxGrid1->SetCellRenderer(t, 2, new wxGridCellBoolRenderer());
            }
        }
    }
}

void kWookaApplicationGenericJavaPanel::OnAddRecord(wxCommandEvent& WXUNUSED(event)) {
    if (WxGrid1 != NULL) {
        WxGrid1->AppendRows();
        WxGrid1->SetCellEditor(WxGrid1->GetNumberRows() - 1, 2, new wxGridCellBoolEditor());
        WxGrid1->SetCellRenderer(WxGrid1->GetNumberRows() - 1, 2, new wxGridCellBoolRenderer());
    }
}

void kWookaApplicationGenericJavaPanel::OnDeleteRecord(wxCommandEvent& WXUNUSED(event)) {
    if (WxGrid1 != NULL) {
        if (WxGrid1->GetGridCursorRow() >= 0) {
            WxGrid1->DeleteRows(WxGrid1->GetGridCursorRow());
        }
    }
}

void kWookaApplicationGenericJavaPanel::OnApplyGrid(wxCommandEvent& WXUNUSED(event)) {

    if (WxGrid1 != NULL) {
        this->m_appItem->appConfigs.Clear();
        for (int i = 0; i < WxGrid1->GetNumberRows(); i++) {
            wxString key = WxGrid1->GetCellValue(i, 0);
            wxString value = WxGrid1->GetCellValue(i, 1);
            wxString env = WxGrid1->GetCellValue(i, 2);
            if (key.Length() > 0) {
                kWookaAppConfigItemInfo configItem;
                configItem.configKey = key;
                configItem.configValue = value;
                configItem.configType = env == wxT("1") ? wxT("env") : wxT("other");
                this->m_appItem->appConfigs.Insert(configItem, 0);
            }
        }
    }
}


void kWookaApplicationGenericJavaPanel::OnHomeVariableChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->groupId = event.GetString();
    }
}

void kWookaApplicationGenericJavaPanel::OnAppNameChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->name = event.GetString();
    }
}

void kWookaApplicationGenericJavaPanel::OnAppVersionChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->version = event.GetString();    
    }
}

void kWookaApplicationGenericJavaPanel::OnStartClassChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->startClass = event.GetString();
    }
}


void kWookaApplicationGenericJavaPanel::OnAddLibBrowse(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        wxFileDialog fd(this, _("Select Jar Library"), wxEmptyString, wxEmptyString, _("JAR (*.jar)|*.jar|" "All files (*.*)|*.*"));
        if (fd.ShowModal() == wxID_OK) {
            // m_appItem->AddLibrary(fd.GetPath());
            wxArrayString files;
            fd.GetPaths(files);
            for (size_t s = 0; s < files.Count(); s++) {
                wxString libfile = files[s];
                wxLibList->AppendString(libfile);
                m_appItem->AddLibrary(libfile);
            }
        }
    }
}


void kWookaApplicationGenericJavaPanel::OnDelLib(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        wxArrayInt sels;
        wxLibList->GetSelections(sels);
        if (sels.Count() > 0) {
            for (ssize_t s = sels.Count(); s > 0; s --) {
                int pos = sels[s - 1];
                wxString str = wxLibList->GetString(pos);
                m_appItem->DelLibrary(str);
                wxLibList->Delete(pos);
            }
        }
    }
}


void kWookaApplicationGenericJavaPanel::OnBrowse(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        wxDirDialog fd(this, _("Select a Java Application Home"));

        if (fd.ShowModal() == wxID_OK) {
            m_appItem->source = fd.GetPath();
            kSource->SetValue(m_appItem->source);
            wxString libpath = fd.GetPath() + wxT("\\lib");
            wxString extpath = fd.GetPath() + wxT("\\ext");
            if (DirectoryExist(libpath)) {
                wxArrayString files;
                wxDir::GetAllFiles(libpath, &files);
                for (size_t s = 0; s < files.Count(); s ++)
                {
                    wxString libfile = files[s];
                    if (libfile.Lower().EndsWith(wxT(".jar"))) {
                        m_appItem->AddLibrary(libfile);
                        wxLibList->AppendString(libfile);
                    }
                }
            }
            if (DirectoryExist(extpath)) {
                wxArrayString files;
                wxDir::GetAllFiles(extpath, &files);
                for (size_t s = 0; s < files.Count(); s++)
                {
                    wxString libfile = files[s];
                    if (libfile.Lower().EndsWith(wxT(".jar"))) {
                        m_appItem->AddLibrary(libfile);
                        wxLibList->AppendString(libfile);
                    }
                }
            }
        }
    }
}

