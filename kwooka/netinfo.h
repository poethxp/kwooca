/********************************************************************************
*                                                                               *
* netinfo.h --       The net performance                                        *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef __NET_INFO_H__
#define __NET_INFO_H__


#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

class NetworkPerformanceItem
{
public:
    NetworkPerformanceItem() {
        ProcessId = 0;
        State = 0;
        LocalPort = 0;
        RemotePort = 0;
        BytesIn = 0;
        BytesOut = 0;
        dwConnState = 0;
        OutboundBandwidth = 0;
        InboundBandwidth = 0;
    }
    ~NetworkPerformanceItem() {}

    unsigned long ProcessId;
    int State;
    int dwConnState;
    std::wstring LocalAddress;
    std::wstring RemoteAddress;
    int LocalPort;
    int RemotePort;
    unsigned __int64 BytesOut;
    unsigned __int64 BytesIn;
    unsigned __int64 OutboundBandwidth;
    unsigned __int64 InboundBandwidth;
    int Pass = -1;
};

void GetProcessNetworkPerformance(NetworkPerformanceItem& item, std::vector<std::wstring>* ptr, unsigned long dwProcessId);


#endif

