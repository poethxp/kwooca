/********************************************************************************
*                                                                               *
* kwooka_perferences_utils.hpp -- Some tools for perferences function           *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef kwooka_perferences_utils_hpp
#define kwooka_perferences_utils_hpp

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include <list>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include "wx/preferences.h"
#include "enum_process.hpp"

wxUint32 GetIntPreference(const wxString& bt);
wxUint32 GetAndIncreaseIntPreference(const wxString& bt);
void SetIntPreference(const wxString& bt, wxUint32 newValue);


void kWookaPerferenceInit(bool ods = true);

wxString kKookaGetPreference(const wxString& bt);
bool kWookaSetPreference(const wxString& bt, const wxString& val);
bool kWookaDeletePreference(const wxString& bt);


void kWookaSetShapeFramePosition(const wxPoint& rc);
wxPoint kWookaGetShapeFramePosition();


/**
 Return the full name with path
 */
wxString kWookaGetPluggedInExecutableFile();

void kWookaSetPluggedInExecutableFile(const wxString& fileNameWithPath, const wxString& filename = wxEmptyString);

bool kWookaIsEnablePluggedInExecutable();
void kWookaSetEnablePluggedInExecutable(bool fs);

bool kWookaIsEnableAutoStartup();
void kWookaSetEnableAutoStartup(bool fs);

bool kWookaIsEnableAutoCheckVersion();
void kWookaSetEnableAutoCheckVersion(bool fs);



long  kWookaConvertToInteger(const wxString& stp);
wxString kWookaFormatDecimal(long pc, int p = 100);


wxArrayString GetJavaHomeList();


wxString GetProcessCmdLine(DWORD dwId);

wxString GetProcessCmdLineByHandle(HANDLE hProcess);

BOOL GetProcessCmdLineByHandle(HANDLE hProcess, LPWSTR wBuf, DWORD dwBufLen, LPDWORD dwBufActLen);

BOOL GetProcessCmdLine(DWORD dwId, LPWSTR wBuf, DWORD dwBufLen, LPDWORD dwBufActLen);

wxVector<DWORD> kWookaGetProcessListByCmdline(const wxString& cmdline);

wxString* wxArrayStringJoin(wxArrayString& lines, const wxString& joinable);

wxString wxNewGuid();

#endif /* life_perferences_hpp */
