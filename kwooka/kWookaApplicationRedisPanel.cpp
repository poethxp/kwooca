﻿/********************************************************************************
*                                                                               *
* kWookaApplicationMavenPanel.cpp -- a custom config for maven project          *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationRedisPanel.hpp"

#include "kWooka_ids.h"
#include "kWookaProjectInfo.hpp"
#include "kWookaExecuteProcess.hpp"

BEGIN_EVENT_TABLE(kWookaApplicationRedisPanel, wxPanel)
////Manual Code Start
EVT_BUTTON(ID_WXBUTTON_BROWSE, kWookaApplicationRedisPanel::OnBrowse)
EVT_BUTTON(ID_WXBUTTON_TEST, kWookaApplicationRedisPanel::OnTest)
EVT_BUTTON(ID_WXBUTTON_APPLY, kWookaApplicationRedisPanel::OnApply)
////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()



kWookaApplicationRedisPanel::kWookaApplicationRedisPanel(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationRedisPanel::~kWookaApplicationRedisPanel()
{
    wxLogDebug(wxT("kWookaApplicationRedisPanel destroyed."));
}

void kWookaApplicationRedisPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();

    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationRedisPanel::CreateContentPanel() {
    WxSource = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    kBtnBrowse = new wxButton(this, ID_WXBUTTON_BROWSE, _("Browse"), wxDefaultPosition, wxSize(100, 20), 0);
    m_RedisRemote = new wxCheckBox(this, wxID_ANY, wxEmptyString);
    kBtnTest = new wxButton(this, ID_WXBUTTON_TEST, _("Test"), wxDefaultPosition, wxSize(100, 20), 0);
    kBtnApply = new wxButton(this, ID_WXBUTTON_APPLY, _("Apply"), wxDefaultPosition, wxSize(100, 20), 0);
    m_RedisServer = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_RedisPort = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_RedisDatabase = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_RedisAuthPassword = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_RedisOptions = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24), wxTE_MULTILINE);
    m_RedisServer->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationRedisPanel::OnRedisServerChange), NULL, this);
    m_RedisPort->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationRedisPanel::OnRedisPortChange), NULL, this);
    m_RedisDatabase->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationRedisPanel::OnRedisDatabaseChange), NULL, this);
    m_RedisAuthPassword->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationRedisPanel::OnRedisAuthPasswordChange), NULL, this);
    m_RedisOptions->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationRedisPanel::OnRedisOptionsChange), NULL, this);
    m_RedisRemote->Connect(wxEVT_CHECKBOX, wxTextEventHandler(kWookaApplicationRedisPanel::OnRedisRemoteChange), NULL, this);
    wxStaticText* lblRedisSource = new wxStaticText(this, wxID_ANY, _("Redis Source"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRedisPort = new wxStaticText(this, wxID_ANY, _("Redis Port"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRedisServer = new wxStaticText(this, wxID_ANY, _("Redis Server"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRedisDatabase = new wxStaticText(this, wxID_ANY, _("Database"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblAuthPassword = new wxStaticText(this, wxID_ANY, _("Auth Password"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRedisRemote = new wxStaticText(this, wxID_ANY, _("Use Remote Redis"), wxPoint(20, 8), wxSize(140, 24));
    wxStaticText* lblRedisOptions = new wxStaticText(this, wxID_ANY, _("Redis Options"), wxPoint(20, 8), wxSize(140, 24));

    wxBoxSizer* bsRedisSource = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsRedisRemote = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsRedisPort = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsRedisDb = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* bsRedisOption = new wxBoxSizer(wxHORIZONTAL);
    
    bsRedisSource->Add(lblRedisSource, 0, wxEXPAND | wxALL, 5);
    bsRedisSource->Add(WxSource, 1, wxEXPAND | wxALL, 5);
    bsRedisSource->Add(kBtnBrowse, 0, wxEXPAND | wxALL, 5);
    
    // bsJvmHeap->Add(bsJvmHeap1, 3, wxSHRINK | | wxALL, 5);
    // bsJvmHeap->Add(bsJvmHeap2, 1, wxSHRINK | wxALIGN_CENTER | wxALL, 5);

    bsRedisRemote->Add(lblRedisRemote, 0, wxEXPAND | wxALL, 5);
    bsRedisRemote->Add(m_RedisRemote, 1, wxEXPAND | wxALL, 5);
    bsRedisRemote->Add(kBtnTest, 0, wxEXPAND | wxALL, 5);
    bsRedisRemote->Add(kBtnApply, 0, wxEXPAND | wxALL, 5);
    
    bsRedisPort->Add(lblRedisServer, 0, wxEXPAND | wxALL, 5);
    bsRedisPort->Add(m_RedisServer, 1, wxEXPAND | wxALL, 5);
    bsRedisPort->Add(lblRedisPort, 0, wxEXPAND |  wxALL, 5);
    bsRedisPort->Add(m_RedisPort, 1, wxEXPAND |  wxALL, 5);

    bsRedisDb->Add(lblRedisDatabase, 0, wxEXPAND | wxALL, 5);
    bsRedisDb->Add(m_RedisDatabase, 1, wxEXPAND |  wxALL, 5);
    bsRedisDb->Add(lblAuthPassword, 0, wxEXPAND | wxALL, 5);
    bsRedisDb->Add(m_RedisAuthPassword, 1, wxEXPAND | wxALL, 5);

    bsRedisOption->Add(lblRedisOptions, 0, wxEXPAND | wxALL, 5);
    bsRedisOption->Add(m_RedisOptions, 1, wxEXPAND | wxALL, 5);

    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    boxSizer->Add(bsRedisSource, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsRedisRemote, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsRedisPort, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsRedisDb, 0, wxEXPAND | wxALL, 5);
    boxSizer->Add(bsRedisOption, 1, wxEXPAND | wxALL, 5);
    
    this->SetSizerAndFit(boxSizer);
}

void kWookaApplicationRedisPanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}

void kWookaApplicationRedisPanel::OnTest(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        kWookaProcessExecutor* executor = m_project->runtime->GetExecutor(m_appItem->id);
        if (!executor->Test()) {
            wxMessageBox(_("The redis configuration is bad. The detail information please lookup the log view."));
        }
        else {
            wxMessageBox(_("The redis configuration test is successful."));
        }
    }
}

void kWookaApplicationRedisPanel::OnApply(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        if (wxYES == wxMessageBox(_("Do you want to apply the current Redis connection properties into all projects?"), wxT("Confirm"), wxYES_NO)) {
            //chimes.redis.password: Kiloseed2019
            //chimes.redis.host : 127.0.0.1
            //chimes.redis.port : 6379
            //chimes.redis.database : 1
            for (int i = 0; i < 4; i++) {
                wxArrayInt toDeletes;
                for (size_t s = 0; s < m_project->appConfigs.Count(); s++) {
                    kWookaAppConfigItemInfo& it = m_project->appConfigs.Item(s);
                    if (it.configKey == wxT("chimes.redis.password")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                    else if (it.configKey == wxT("chimes.redis.host")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                    else if (it.configKey == wxT("chimes.redis.port")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                    else if (it.configKey == wxT("chimes.redis.database")) {
                        toDeletes.Add(m_project->appConfigs.Index(it));
                        break;
                    }
                }
                for (size_t s = 0; s < toDeletes.Count(); s++) {
                    m_project->appConfigs.RemoveAt(toDeletes.Item(s));
                }
            }
            kWookaAppConfigItemInfo itdbn;
            kWookaAppConfigItemInfo ithost;
            kWookaAppConfigItemInfo itport;
            kWookaAppConfigItemInfo itpwd;
            itpwd.configKey = wxT("chimes.redis.password");
            itpwd.configType = "";
            itpwd.configValue = m_appItem->GetItemValue(wxT("RedisAuthPassword"));

            ithost.configKey = wxT("chimes.redis.host");
            ithost.configType = "";
            ithost.configValue = m_appItem->GetItemValue(wxT("RedisServer"));
            
            itport.configKey = wxT("chimes.redis.port");
            itport.configType = "";
            itport.configValue = m_appItem->GetItemValue(wxT("RedisPort"));

            itdbn.configKey = wxT("chimes.redis.database");
            itdbn.configType = "";
            itdbn.configValue = m_appItem->GetItemValue(wxT("RedisDatabase"));

            m_project->appConfigs.Add(ithost);
            m_project->appConfigs.Add(itport);
            m_project->appConfigs.Add(itpwd);
            m_project->appConfigs.Add(itdbn);
        }
    }
}

void kWookaApplicationRedisPanel::UpdateAppItem(kWookaProjectInfo* proj, kWookaAppItemInfo* item) {
    m_project = proj;
    m_appItem = item;
    m_jvmConfig = &item->jvmConfig;
    if (m_appItem != NULL) {
        m_RedisServer->SetValue(m_appItem->GetItemValue(wxT("RedisServer")));
        m_RedisPort->SetValue(m_appItem->GetItemValue(wxT("RedisPort")));
        m_RedisDatabase->SetValue(m_appItem->GetItemValue(wxT("RedisDatabase")));
        m_RedisAuthPassword->SetValue(m_appItem->GetItemValue(wxT("RedisAuthPassword")));
        m_RedisOptions->SetValue(m_appItem->GetItemValue(wxT("RedisOptions")));
        m_RedisRemote->SetValue(m_appItem->GetItemValue(wxT("RedisRemote")) == wxT("remote") ? true : false);
        WxSource->SetValue(m_appItem->source);
    }
    else {
        m_RedisServer->SetValue(wxT(""));
        m_RedisPort->SetValue(wxT(""));
        m_RedisDatabase->SetValue(wxT(""));
        m_RedisAuthPassword->SetValue(wxT(""));
        m_RedisOptions->SetValue(wxT(""));
        m_RedisRemote->SetValue(false);
        WxSource->SetValue(wxT(""));
    }
}


void kWookaApplicationRedisPanel::OnRedisOptionsChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RedisOptions"), event.GetString());
    }
}

void kWookaApplicationRedisPanel::OnRedisRemoteChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RedisRemote"), event.IsChecked() ? wxT("remote") : wxT("local"));
    }
}

void kWookaApplicationRedisPanel::OnRedisServerChange(wxCommandEvent& event)
{
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RedisServer"), event.GetString());
        m_appItem->name = event.GetString() + wxT("_") + m_appItem->GetItemValue(wxT("RedisPort"));
    }
}

void kWookaApplicationRedisPanel::OnRedisPortChange(wxCommandEvent& event)
{
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RedisPort"), event.GetString());
        m_appItem->name = m_appItem->GetItemValue(wxT("RedisServer")) + wxT("_") + event.GetString();
    }
}


void kWookaApplicationRedisPanel::OnRedisAuthPasswordChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RedisAuthPassword"), event.GetString());
    }
}


void kWookaApplicationRedisPanel::OnRedisDatabaseChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->UpdateItem(wxT("RedisDatabase"), event.GetString());
    }
}


void kWookaApplicationRedisPanel::OnBrowse(wxCommandEvent& WXUNUSED(event)) {
    if (m_appItem != NULL) {
        wxDirDialog  dirdlg(this, _("Open the Redis HOME directory"));

        if (dirdlg.ShowModal() == wxID_OK) {
            this->WxSource->SetValue(dirdlg.GetPath());
            m_appItem->source = dirdlg.GetPath();
        }
    }
}
