/********************************************************************************
*                                                                               *
* kWookaApplicationItemPanel.hpp -- Application Item panel display in the list  *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef kWookaApplicationItemPanel_hpp
#define kWookaApplicationItemPanel_hpp


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>

#include "kWookaProjectInfo.hpp"
#include "kWoocaStaticText.hpp"

#define EVT_UPDATE_EXECUTE_STATE  5998

#define COLOURE_SELECTED        wxColour(0x33, 0x99, 0x66, 0xFF)
#define COLOURE_ENTER_WINDOW    wxColour(0x00, 0x66, 0xFF, 0xFF)
#define COLOURE_LEAVE_WINDOW    wxColour(0x44, 0x99, 0xFF, 0xFF)
#define COLOURE_MAJOR_WHITE      wxColour(0xFF, 0xFF, 0xFF, 0xFF)
#define COLOURE_MINOR_WHITE      wxColour(0xF0, 0xF0, 0xF0, 0xFF)

class kWookaProcessExecutor;
class kWookaApplicationListPanel;

////Dialog Style Start
#undef kWookaApplicationItemPanel_STYLE
#define kWookaApplicationItemPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

class kWookaApplicationItemPanel : public wxPanel
{
    private:
        DECLARE_EVENT_TABLE();
        
    public:
        kWookaApplicationItemPanel(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("kWooca"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationItemPanel_STYLE);
        virtual ~kWookaApplicationItemPanel();
    
        void SetListPanel(kWookaApplicationListPanel* lp);
        kWookaApplicationListPanel* GetListPanel();

        virtual void SetAppItemInfo(kWookaAppItemInfo* item);
        virtual void SetProjectInfo(kWookaProjectInfo* proj) {
            this->m_project = proj;
        }

        virtual kWookaAppItemInfo* GetAppItemInfo() {
            return m_item;
        }

        virtual kWookaProjectInfo* GetProjectInfo() {
            return m_project;
        }

        virtual kWookaProcessExecutor* GetExecutor() {
            return m_executor;
        }

        void SetSelected(bool sel = true) {
            m_selected = sel;
            UpdateState();
            Refresh();
        }

        bool IsSelected() {
            return m_selected;
        }

        void UpdateButtonState();

    private:
        //Do not add custom control declarations between
        //GUI Control Declaration Start and GUI Control Declaration End.
        //wxDev-C++ will remove them. Add custom code after the block.
        ////GUI Control Declaration Start
    
        wxStaticBox *kLeftBox;
        wxStaticBox *kRightBox;

        wxBitmapButton*  WxBtnDeleteApp;
        wxBitmapButton*  WxBtnRestartApp;

        wxBitmapButton*  WxBtnStopApp;

        kWoocaStaticText*kWookaAppName;
        kWoocaStaticText* kWookaAppDesc;
        kWoocaStaticText* kWookaAppState;
        // wxHtmlWindow* kWookaHtmlDesc;

        wxStaticText* kWookaAppStatus;
        wxStaticText* kWookaAppProcessId;

        bool m_selected;
        _CrtMemState s1;
        _CrtMemState s2;
        _CrtMemState s3;
        kWookaProcessExecutor* m_executor;
        kWookaAppItemInfo* m_item;
        kWookaProjectInfo* m_project;

        wxColor m_backgroundColor;
    public:
        //Note: if you receive any error with these enum IDs, then you need to
        //change your old form code that are based on the #define control IDs.
        //#defines may replace a numeric value for the enum names.
        //Try copy and pasting the below block in your old form header files.
        enum
        {
            ////GUI Enum Control ID Start
            ID_WXBUTTON_DELETE_RECORD = 2039,
            ID_WXEDIT8 = 2029,
            ID_WXSTATICTEXT_APP_STATE = 2020,
            ID_WXEDIT7 = 2019,
            ID_WXSTATICTEXT_APP_NAME = 2018,
            ID_WXEDIT6 = 2017,
            ID_WXSTATICTEXT_APP_DESC = 2016,
            ID_WXEDIT5 = 2015,
            ID_WXSTATICTEXT_ORDERNAME = 2013,
            ID_WXEDIT4 = 2012,
            ID_WXSTATICLABEL_DATE = 2011,
            ID_WXEDIT3 = 2010,
            ID_WXEDIT2 = 2009,
            ID_WXSTATICLABEL_STATUS = 2008,
            ID_WXBUTTON_STARTAPP = 3007,
            ID_WXBUTTON_STOPAPP = 3006,
            ID_WXBUTTON_DELAPP = 3005,
            ID_WXEDIT1 = 2004,
            ID_WXSTATICLABEL_ORDERNO = 2003,
            ID_WXSTATICBOX1 = 2002,
            ////GUI Enum Control ID End
            ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
        };
        
    private:
        void OnKeyCharEvent(wxKeyEvent& event);
        void OnClose(wxCloseEvent& event);
        void CreateGUIControls();
        void CreateContentPanel();
        
        void UpdateState();

        void OnStartApp(wxCommandEvent& event);
        void OnStopApp(wxCommandEvent& event);
        void OnDeleteApp(wxCommandEvent& event);

        void OnUpdateExecuteState(wxThreadEvent& event);

        void OnMouseEnter(wxMouseEvent& event);
        void OnMouseLeave(wxMouseEvent& event);
        void OnDblClick(wxMouseEvent& event);
        kWookaApplicationListPanel* m_listpanel;
};


#endif /* kPosAccountingPanel_hpp */
