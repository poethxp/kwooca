/********************************************************************************
*                                                                               *
* kWookaApplicationViewPanel.cpp -- Major view panel                            *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationViewPanel.hpp"

#include "kWooka_ids.h"
#include "wx/metafile.h"
#include "wx/artprov.h"

#include "kWookaApplicationConfigPanel.hpp"
#include "kWookaApplicationLoggerPanel.hpp"
#include "kWookaApplicationMonitorPanel.hpp"
#include "kWookaProjectInfo.hpp"
#include "kWookaDevelopmentPlanPanel.hpp"

#include "kWookaApplicationNginxServerPanel.hpp"
// #include "kWookaApplicationNginxServicePanel.hpp"
#include "kWookaApplicationMavenPanel.hpp"
#include "kWookaApplicationGenericJavaPanel.hpp"
#include "kWookaApplicationMySQLPanel.hpp"
#include "kWookaApplicationRedisPanel.hpp"

BEGIN_EVENT_TABLE(kWookaApplicationViewPanel, wxPanel)
    ////Manual Code Start
    ////Manual Code End
    EVT_AUINOTEBOOK_PAGE_CLOSE(ID_NOTEBOOK, kWookaApplicationViewPanel::OnNotePageClose)
    EVT_AUINOTEBOOK_PAGE_CHANGED(ID_NOTEBOOK, kWookaApplicationViewPanel::OnNotePageChange)
    //EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
END_EVENT_TABLE()



kWookaApplicationViewPanel::kWookaApplicationViewPanel(wxWindow *parent, wxWindowID id, const wxString& WXUNUSED(title), const wxPoint &position, const wxSize& size, long style)
: wxPanel(parent, id, position, size, style)
{
    m_appitem = NULL;
    m_notebook = NULL;
    CreateGUIControls();
}

kWookaApplicationViewPanel::~kWookaApplicationViewPanel()
{
    wxLogDebug(wxT("kWookaApplicationViewPanel destroyed."));
}

void kWookaApplicationViewPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    CreateContentPanel();
    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationViewPanel::CreateContentPanel() {
    wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    this->m_notebook = this->CreateNotebook();
    boxSizer->Add(m_notebook, 1, wxALL | wxEXPAND, 5);
    this->SetSizerAndFit(boxSizer);
}

wxAuiNotebook* kWookaApplicationViewPanel::CreateNotebook() {

    wxSize client_size = GetClientSize();

    wxAuiNotebook* ctrl = new wxAuiNotebook(this, ID_NOTEBOOK,
        wxPoint(client_size.x, client_size.y),
        wxSize(430, 200),
        wxAUI_NB_TOP |
        wxAUI_NB_TAB_SPLIT |
        wxAUI_NB_TAB_MOVE |
        wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_TAB_EXTERNAL_MOVE | wxNO_BORDER);
    ctrl->Freeze();

    m_notebook = ctrl;

    wxBitmap page_bmp = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16, 16));
    kWookaApplicationMonitorPanel* monitor = new kWookaApplicationMonitorPanel(ctrl);
    kWookaApplicationConfigPanel* configer = new kWookaApplicationConfigPanel(ctrl);
    kWookaApplicationLoggerPanel* logger = new kWookaApplicationLoggerPanel(ctrl);
    kWookaDevelopmentPlanPanel* plan = new kWookaDevelopmentPlanPanel(this);

    monitor->SetViewPanel(this);
    configer->SetViewPanel(this);
    logger->SetViewPanel(this);

    ctrl->AddPage(logger, _("Logging") , true, page_bmp);
    ctrl->AddPage(monitor, _("Monitor"), true, page_bmp);
    // ctrl->AddPage(configer, _("Config"), true, page_bmp);
    // ctrl->AddPage(plan, _("Plan"), true, page_bmp);

    this->AddPanel(configer, _("Config"));
    this->AddPanel(plan, _("Donate"));
    
    m_monitorpanel = monitor;
    m_loggerpanel = logger;
    // m_configpanel = configer;
    
    ctrl->Thaw();
    return ctrl;

}

void kWookaApplicationViewPanel::OnKeyCharEvent(wxKeyEvent& event){
    if (event.GetKeyCode() == WXK_RETURN){
    }
}


void kWookaApplicationViewPanel::SetWookaFrame(kWookaFrame* lf) {
    m_wookaframe = lf;
}

kWookaFrame* kWookaApplicationViewPanel::GetWookaFrame() {
    return m_wookaframe;
}

wxWindow* kWookaApplicationViewPanel::FindOrCreatePanel(wxWindowID id) {
    for (wxVector<wxPanel*>::iterator it = m_panels.begin(); it != m_panels.end(); ++it) {
        wxWindow* pwnd = (wxWindow*)*it;
        if (id == pwnd->GetId()) {
            /// this will be deleted
            return pwnd;
        }
    }
    // here to identified by there is not panel found.
    // So, create it
    return NULL;
}

void kWookaApplicationViewPanel::UpdateAppItem(kWookaProjectInfo* proj, kWookaAppItemInfo* appitem) {
    this->m_project = proj;
    this->m_appitem = appitem;
    m_loggerpanel->UpdateLoggerUI();
    m_monitorpanel->UpdateAppItem(m_appitem);
    if (m_appitem == NULL) {
        return;
    }
    if (m_appitem->appType == wxT("springboot")) {
        kWookaApplicationConfigPanel* m_configpanel = (kWookaApplicationConfigPanel*)this->FindOrCreatePanel(ID_PANEL_SPRING_CONFIG);
        if (m_configpanel != NULL) {
            m_configpanel->UpdateConfigItem(m_appitem);
        }
        else {
            m_configpanel = new kWookaApplicationConfigPanel(this);
            this->AddPanel(m_configpanel, _("Config"));
            m_configpanel->UpdateConfigItem(m_appitem);
        }
    }
    else if (m_appitem->appType == wxT("nginx-server")) {
        kWookaApplicationNginxServerPanel* nsp = (kWookaApplicationNginxServerPanel*)this->FindOrCreatePanel(ID_PANEL_NGINX_CONFIG);
        if (nsp != NULL) {
            nsp->UpdateNginxConfig(m_project, m_appitem);
        }
        else {
            nsp = new kWookaApplicationNginxServerPanel(this);
            this->AddPanel(nsp, _("Nginx Server"));
            nsp->UpdateNginxConfig(m_project, m_appitem);
        }
    }
    /*
    else if (m_appitem->appType == wxT("nginx-service")) {
        kWookaApplicationNginxServicePanel* nsvcp = (kWookaApplicationNginxServicePanel*)FindOrCreatePanel(ID_PANEL_NGINX_SERVICE_CONFIG);
        if (nsvcp != NULL) {
            nsvcp->UpdateNginxConfig(m_appitem);
        }
        else {
            nsvcp = new kWookaApplicationNginxServicePanel(this);
            this->AddPanel(nsvcp, _("Nginx Service"));
            nsvcp->UpdateNginxConfig(m_appitem);
        }
    }
    */
    else if (m_appitem->appType == wxT("mysql")) {
        kWookaApplicationMySQLPanel* nsvcp = (kWookaApplicationMySQLPanel*)FindOrCreatePanel(ID_PANEL_MYSQL_CONFIG);
        if (nsvcp != NULL) {
            nsvcp->UpdateAppItem(m_project, m_appitem);
        }
        else {
            nsvcp = new kWookaApplicationMySQLPanel(this);
            this->AddPanel(nsvcp, _("MySQL Database"));
            nsvcp->UpdateAppItem(m_project, m_appitem);
        }
    }
    else if (m_appitem->appType == wxT("redis")) {
        kWookaApplicationRedisPanel* nsvcp = (kWookaApplicationRedisPanel*)FindOrCreatePanel(ID_PANEL_REDIS_CONFIG);
        if (nsvcp != NULL) {
            nsvcp->UpdateAppItem(m_project, m_appitem);
        }
        else {
            nsvcp = new kWookaApplicationRedisPanel(this);
            this->AddPanel(nsvcp, _("Redis Support"));
            nsvcp->UpdateAppItem(m_project, m_appitem);
        }
    }
    else if (m_appitem->appType == wxT("maven")) {
        kWookaApplicationMavenPanel* nsvcp = (kWookaApplicationMavenPanel*)FindOrCreatePanel(ID_PANEL_MAVEN_CONFIG);
        if (nsvcp != NULL) {
            nsvcp->UpdateAppItem(m_appitem);
        }
        else {
            nsvcp = new kWookaApplicationMavenPanel(this);
            this->AddPanel(nsvcp, _("Maven Application"));
            nsvcp->UpdateAppItem(m_appitem);
        }
    }
    else if (m_appitem->appType == wxT("java")) {
        kWookaApplicationGenericJavaPanel* nsvcp = (kWookaApplicationGenericJavaPanel*)FindOrCreatePanel(ID_PANEL_JAVA_CONFIG);
        if (nsvcp != NULL) {
            nsvcp->UpdateAppItem(m_appitem);
        }
        else {
            nsvcp = new kWookaApplicationGenericJavaPanel(this);
            this->AddPanel(nsvcp, _("Generic Java App"));
            nsvcp->UpdateAppItem(m_appitem);
        }
    }
}


bool kWookaApplicationViewPanel::ExistPanel(wxWindowID winId, bool moveCurrent) {
    for (wxVector<wxPanel*>::iterator it = m_panels.begin(); it != m_panels.end(); ++it) {
        wxWindow* pwnd = (wxWindow*)*it;
        if (pwnd->GetId() == winId) {
            if (moveCurrent) {
                int i = this->m_notebook->GetPageIndex(pwnd);
                this->m_notebook->SetSelection(i);
            }
            return true;
        }
    }
    return false;
}

bool kWookaApplicationViewPanel::AddPanel(wxPanel* panel, const wxString title) {
    bool found = false;
    for (wxVector<wxPanel*>::iterator it = m_panels.begin(); it != m_panels.end(); ++it) {
        wxWindow* pwnd = (wxWindow*)*it;
        if (pwnd->GetId() == panel->GetId()) {
            found = true;
            return false;
        }
    }
    if (!found) {
        this->m_notebook->AddPage(panel, title, true);
        this->m_panels.push_back(panel);
        return true;
    }
    return false;
}


void kWookaApplicationViewPanel::OnNotePageChange(wxAuiNotebookEvent& event) {
    if (m_notebook != NULL) {
        wxWindow* pWnd = m_notebook->GetPage(event.GetSelection());
        long style = m_notebook->GetWindowStyle();
        if (pWnd == m_loggerpanel || pWnd == m_monitorpanel) {
            if (style & wxAUI_NB_CLOSE_ON_ACTIVE_TAB) {
                m_notebook->ToggleWindowStyle(wxAUI_NB_CLOSE_ON_ACTIVE_TAB);
            }
        }
        else {
            m_notebook->SetWindowStyle(style | wxAUI_NB_CLOSE_ON_ACTIVE_TAB);
        }
    }
}

void kWookaApplicationViewPanel::OnNotePageClose(wxAuiNotebookEvent& event) {
    wxWindow* wnd = m_notebook->GetPage(event.GetSelection());
    for (wxVector<wxPanel*>::iterator it = m_panels.begin(); it != m_panels.end(); ++it) {
        wxWindow* pwnd = (wxWindow*)*it;
        if (wnd == pwnd) {
            /// this will be deleted
            m_panels.erase(it);
            return;
        }
    }
}
