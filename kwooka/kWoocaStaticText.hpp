/********************************************************************************
*                                                                               *
* kWookaStaticTex.hpp -- a custom implementation of static text for transparent *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaStaticText_hpp
#define kWookaStaticText_hpp


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#else
    #include <wx/wxprec.h>
#endif

#include <wx/stattext.h>

class kWoocaStaticText : public wxStaticText
{
private:
    DECLARE_EVENT_TABLE();

public:
        kWoocaStaticText() { }

        kWoocaStaticText(wxWindow* parent,
            wxWindowID id,
            const wxString& label,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = 0,
            const wxString& name = wxASCII_STR(wxStaticTextNameStr))
        {
            Create(parent, id, label, pos, size, style, name);
            SetBackgroundStyle(wxBG_STYLE_PAINT);
        }


    protected:
        // OnPaint();
        void OnPaint(wxPaintEvent& event);
        void OnMouseOver(wxMouseEvent& evt);
        void OnDblClick(wxMouseEvent& evt);

        /// wxDECLARE_DYNAMIC_CLASS_NO_COPY(kWoocaStaticText);
};


#endif /* kPosAccountingPanel_hpp */
