/********************************************************************************
*                                                                               *
* kwooka_aboutdlg.cpp --       The aboud dialog                                 *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


// ==========================================================================
// headers, declarations, constants
// ==========================================================================

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include "wx/statline.h"
#include "wx/minifram.h"
#include "wx/settings.h"

#include "kWooka_aboutdlg.h"
#include "kWooka.h"


// --------------------------------------------------------------------------
// resources
// --------------------------------------------------------------------------

//#include "samples.inc"

// --------------------------------------------------------------------------
// constants
// --------------------------------------------------------------------------

// IDs for the controls and the menu commands
enum
{
    // listbox in samples dialog
    ID_LISTBOX
};




// --------------------------------------------------------------------------
// LifeAboutDialog
// --------------------------------------------------------------------------

kWookaAboutDialog::kWookaAboutDialog(wxWindow *parent)
               : wxDialog(parent, wxID_ANY, _("About kWooca"),
                          wxDefaultPosition, wxDefaultSize)
{
    // logo
    //wxStaticBitmap *sbmp = new wxStaticBitmap(this, wxID_ANY, wxBitmap(life_xpm));

    // layout components
    wxBoxSizer *sizer = new wxBoxSizer( wxVERTICAL );
    //sizer->Add( sbmp, 0, wxCENTRE | wxALL, 10 );
    sizer->Add( CreateTextSizer(_("kWooca -- It's a tool for managing the SpringBoot application to be started.\n\n version 0.1\n\n\n\n(c) 2021 Fengren Technology (Guangzhou) Co.Ltd\n\n\
<zouyl@kiloseed.com>\n\n")),
                                  0, wxCENTRE | wxRIGHT|wxLEFT|wxTOP, 20 );

    
    sizer->Add( new wxStaticLine(this, wxID_ANY), 0, wxGROW | wxLEFT | wxRIGHT, 5 );
    
    // buttons if any
    wxSizer *sizerBtns = CreateButtonSizer(wxOK);
    if ( sizerBtns )
    {
        sizer->Add(sizerBtns, wxSizerFlags().Expand().Border());
    }
    
    //wxGauge* gauge = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxSize(200, 40), wxGA_HORIZONTAL | wxGA_PROGRESS | wxGA_TEXT );
    //gauge->SetValue(60);
    //sizer-Add(gauge, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);
    //sizer->Add(gauge, wxSizerFlags().Expand().Border());
    //itemBoxSizer3->Add(itemGauge4, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5);

    // activate
    SetSizer(sizer);
    sizer->SetSizeHints(this);
    sizer->Fit(this);
    Centre(wxBOTH | wxCENTRE_ON_SCREEN);
}
