/********************************************************************************
*                                                                               *
* enum_process.hpp -- Some common api for process                               *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef enum_process_hpp
#define enum_process_hpp

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#ifdef __WXMAC__
#include <sys/sysctl.h>
#include <libproc.h>
#else
#ifdef WIN32
#include <windows.h>
#include <Tlhelp32.h>
#include <psapi.h>
#endif
#endif

wxString kWookaGetProcessNameWithPid(uint32_t pid);
wxArrayString kWookaGetProcessList();

#ifdef WIN32
DWORD GetMainThreadId(DWORD ProcessId);
BOOL MakeHookTargetProcess(LPTSTR szAppName, LPDWORD lpdwProcessId, HHOOK* lpHhk);
BOOL InitialHookModule();
BOOL CloseHookModule();
BOOL UnhookWindowProcEvent(HHOOK hk);
BOOL kWookaGetModuleFullPath(HMODULE hModule, TCHAR pszFullPath[MAX_PATH]);


BOOL FindKiloseedRootCert(wxString& szStoreName);

#endif

#endif /* enum_process_hpp */
