/********************************************************************************
*                                                                               *
* kWookaApplicationListPanel.hpp -- Show application list  for a project        *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaApplicationListPanel_hpp
#define kWookaApplicationListPanel_hpp


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
    #include <wx/frame.h>
#else
    #include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include <wx/process.h>

#include "kWookaProjectInfo.hpp"
#include "kWookaExecuteProcess.hpp"

////Dialog Style Start
#undef kWookaApplicationListPanel_STYLE
#define kWookaApplicationListPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End
class kWookaApplicationItemPanel;
class kWookaFrame;

WX_DECLARE_OBJARRAY(kWookaApplicationItemPanel*, kWookaApplicationItemArray);

class kWookaApplicationListPanel : public wxPanel
{
    private:
        DECLARE_EVENT_TABLE();
        
    public:
        kWookaApplicationListPanel(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("kWooca"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationListPanel_STYLE);
        virtual ~kWookaApplicationListPanel();
    
        virtual void SetProjectInfo(kWookaProjectInfo* project);
        virtual const kWookaProjectInfo* GetProjectInfo() const;
        void SetWookaFrame(kWookaFrame* lf);
        kWookaFrame* GetWookaFrame();
        void RemoveItemPanel(kWookaApplicationItemPanel* item);
        void UpdatePanels();
        void UpdateSelectItem(kWookaApplicationItemPanel* item);
        void StartAllApps();
        void StopAllApps();

    private:
        //Do not add custom control declarations between
        //GUI Control Declaration Start and GUI Control Declaration End.
        //wxDev-C++ will remove them. Add custom code after the block.
        ////GUI Control Declaration Start
    
        wxButton *WxBtnDeleteApp;
        wxButton* WxBtnAddApp;
        wxButton* WxBtnStartAll;
        wxButton* WxBtnStopAll;
        wxButton* WxBtnAddMore;
        wxSizer* wxSizer;
        wxScrolledWindow* m_scrollWindow;
        wxBoxSizer* m_wxVertBoxSizer;
        ////GUI Control Declaration End

        // The project info
        kWookaProjectInfo* m_project;

        kWookaApplicationItemArray m_itempanels;

    private:
        //Note: if you receive any error with these enum IDs, then you need to
        //change your old form code that are based on the #define control IDs.
        //#defines may replace a numeric value for the enum names.
        //Try copy and pasting the below block in your old form header files.
        enum
        {
            ////GUI Enum Control ID Start
            ID_MENU_MAVEN = 2065,
            ID_MENU_GENERIC_JAVA = 2064,
            ID_MENU_REDIS = 2063,
            ID_MENU_MYSQL = 2062,
            ID_MENU_NGINX_SERVICE = 2061,
            ID_MENU_NGINX_SERVER = 2060,
            ID_WXBUTTON_DELETE_RECORD = 2039,
            ID_WXEDIT8 = 2029,
            ID_WXSTATICTEXT_SPACE = 2020,
            ID_WXEDIT7 = 2019,
            ID_WXSTATICTEXT_MEMBER_NAME = 2018,
            ID_WXEDIT6 = 2017,
            ID_WXSTATICTEXT_MEMBER_CARD = 2016,
            ID_WXEDIT5 = 2015,
            ID_WXBUTTON_ADDMORE = 2014,
            ID_WXSTATICTEXT_ORDERNAME = 2013,
            ID_WXEDIT4 = 2012,
            ID_WXSTATICLABEL_DATE = 2011,
            ID_WXEDIT3 = 2010,
            ID_WXEDIT2 = 2009,
            ID_WXSTATICLABEL_STATUS = 2008,
            ID_WXBUTTON_ADDAPP = 2007,
            ID_WXBUTTON_STARTALL = 2006,
            ID_WXBUTTON_STOPALL = 2005,
            ID_WXEDIT1 = 2004,
            ID_WXSTATICLABEL_ORDERNO = 2003,
            ID_WXSTATICBOX1 = 2002,
            ID_WXGRID1 = 2001,
            ////GUI Enum Control ID End
            ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
        };
        
    private:
        void OnKeyCharEvent(wxKeyEvent& event);
        void OnClose(wxCloseEvent& event);
        
        void CreateGUIControls();
    
        void CreateSearchPanel();

        void OnProcessEnded(wxProcessEvent& event);

        void OnAddApp(wxCommandEvent& event);
        void OnStartAll(wxCommandEvent& event);
        void OnStopApps(wxCommandEvent& event);
        void OnAddMore(wxCommandEvent& event);
        void OnUpdateExecuteState(wxThreadEvent& event);
        void OnMenu(wxCommandEvent& event);
        kWookaFrame* m_wookaframe;
};


#endif /* kPosAccountingPanel_hpp */
