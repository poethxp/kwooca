﻿/********************************************************************************
*                                                                               *
* kWookaApplicationNginxServerPanel.hpp -- a custom config for nginx server node*
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef kWookaApplicationNginxServerPanel_hpp
#define kWookaApplicationNginxServerPanel_hpp


#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/frame.h>
#else
#include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include "kWooka_ids.h"

////Dialog Style Start
#undef kWookaApplicationNginxServerPanel_STYLE
#define kWookaApplicationNginxServerPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

class kWookaAppItemInfo;
class kWookaProjectInfo;

class kWookaApplicationNginxServerPanel : public wxPanel
{
private:
    DECLARE_EVENT_TABLE();

public:
    kWookaApplicationNginxServerPanel(wxWindow* parent, wxWindowID id = ID_PANEL_NGINX_CONFIG, const wxString& title = wxT("kWooca"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationNginxServerPanel_STYLE);
    virtual ~kWookaApplicationNginxServerPanel();


    void UpdateNginxConfig(kWookaProjectInfo* proj, kWookaAppItemInfo* appItem);
private:
    //Do not add custom control declarations between
    //GUI Control Declaration Start and GUI Control Declaration End.
    //wxDev-C++ will remove them. Add custom code after the block.
    ////GUI Control Declaration Start

    ////GUI Control Declaration End

    wxTextCtrl* m_NginxServerName;
    wxTextCtrl* m_NginxListenPort;
    wxTextCtrl* m_NginxServerOptions;
    wxTextCtrl* WxSource;
    wxButton*   kBtnBrowse;
    wxButton*   kBtnTest;
    wxCheckBox* m_NginxIPv6;


private:
    //Note: if you receive any error with these enum IDs, then you need to
    //change your old form code that are based on the #define control IDs.
    //#defines may replace a numeric value for the enum names.
    //Try copy and pasting the below block in your old form header files.
    enum
    {
        ////GUI Enum Control ID Start
        ID_WXBUTTON_DELETE_RECORD = 2039,
        ID_WXBUTTON_TEST = 2030,
        ID_WXEDIT8 = 2029,
        ID_WXSTATICTEXT_SPACE = 2020,
        ID_WXEDIT7 = 2019,
        ID_WXSTATICTEXT_MEMBER_NAME = 2018,
        ID_WXEDIT6 = 2017,
        ID_WXSTATICTEXT_MEMBER_CARD = 2016,
        ID_WXEDIT5 = 2015,
        ID_WXSTATICTEXT_ORDERNAME = 2013,
        ID_WXEDIT4 = 2012,
        ID_WXSTATICLABEL_DATE = 2011,
        ID_WXEDIT3 = 2010,
        ID_WXEDIT2 = 2009,
        ID_WXSTATICLABEL_STATUS = 2008,
        ID_WXBUTTON_ADDTOCART = 2005,
        ID_WXEDIT1 = 2004,
        ID_WXSTATICLABEL_ORDERNO = 2003,
        ID_WXSTATICBOX1 = 2002,
        ID_WXGRID1 = 2001,
        ID_WXBUTTON_BROWSE = 2000,
        ////GUI Enum Control ID End
        ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
    };

private:
    void OnKeyCharEvent(wxKeyEvent& event);
    void CreateGUIControls();
    void OnBrowse(wxCommandEvent& event);
    void OnTest(wxCommandEvent& event);
    void CreateContentPanel();
    void OnSourceChange(wxCommandEvent& event);
    void OnServerNameChange(wxCommandEvent& event);
    void OnListenPortChange(wxCommandEvent& event);
    void OnIPV6Change(wxCommandEvent& event);
    void OnServerOptionsChange(wxCommandEvent& event);

    kWookaAppItemInfo* m_appItem;
    kWookaProjectInfo* m_project;
};


#endif /* kWookaApplicationJavaVMPanel_hpp */
