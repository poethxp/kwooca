﻿/********************************************************************************
*                                                                               *
* kWookaApplicationMySQLPanel.hpp -- a custom config for mysql database         *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef kWookaApplicationMySQLPanel_hpp
#define kWookaApplicationMySQLPanel_hpp


#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/frame.h>
#else
#include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include "kWooka_ids.h"

////Dialog Style Start
#undef kWookaApplicationMySQLPanel_STYLE
#define kWookaApplicationMySQLPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

class kWookaJvmConfigInfo;
class kWookaAppItemInfo;
class kWookaProjectInfo;

class kWookaApplicationMySQLPanel : public wxPanel
{
private:
    DECLARE_EVENT_TABLE();

public:
    kWookaApplicationMySQLPanel(wxWindow* parent, wxWindowID id = ID_PANEL_MYSQL_CONFIG, const wxString& title = wxT("kWooca"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationMySQLPanel_STYLE);
    virtual ~kWookaApplicationMySQLPanel();

    void UpdateAppItem(kWookaProjectInfo* proj, kWookaAppItemInfo* item);
private:
    //Do not add custom control declarations between
    //GUI Control Declaration Start and GUI Control Declaration End.
    //wxDev-C++ will remove them. Add custom code after the block.
    ////GUI Control Declaration Start

    ////GUI Control Declaration End

    wxTextCtrl* m_MySQLServer;
    wxTextCtrl* m_MySQLPort;
    wxTextCtrl* m_MySQLDatabase;
    wxTextCtrl* m_UserName;
    wxTextCtrl* m_Password;
    wxTextCtrl* m_Advance;
    wxTextCtrl* m_Restore;
    wxTextCtrl* m_MySQLOptions;
    wxTextCtrl* WxSource;

    wxCheckBox* m_MySQLRemote;
    wxCheckBox* m_chkRestore;
    wxCheckBox* m_chkDump;
    wxCheckBox* m_ClearDataDir;
    wxButton*   kBtnBrowseRestore;
    wxButton*   kBtnBrowse;
    wxButton*   kBtnTest;
    wxButton*   kBtnApply;

private:
    //Note: if you receive any error with these enum IDs, then you need to
    //change your old form code that are based on the #define control IDs.
    //#defines may replace a numeric value for the enum names.
    //Try copy and pasting the below block in your old form header files.
    enum
    {
        ////GUI Enum Control ID Start
        ID_WXBUTTON_BROWSE = 2039,
        ID_WXEDIT8 = 2029,
        ID_WXSTATICTEXT_SPACE = 2020,
        ID_WXEDIT7 = 2019,
        ID_WXSTATICTEXT_MEMBER_NAME = 2018,
        ID_WXEDIT6 = 2017,
        ID_WXSTATICTEXT_MEMBER_CARD = 2016,
        ID_WXEDIT5 = 2015,
        ID_WXSTATICTEXT_ORDERNAME = 2013,
        ID_WXEDIT4 = 2012,
        ID_WXSTATICLABEL_DATE = 2011,
        ID_WXEDIT3 = 2010,
        ID_WXEDIT2 = 2009,
        ID_WXSTATICLABEL_STATUS = 2008,
        ID_WXBUTTON_TEST = 2005,
        ID_WXBUTTON_APPLY = 2006,
        ID_WXEDIT1 = 2004,
        ID_WXBUTTON_BROWSE_RESTORE = 2003,
        ID_WXSTATICBOX1 = 2002,
        ID_WXGRID1 = 2001,
        ////GUI Enum Control ID End
        ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
    };

private:
    void OnKeyCharEvent(wxKeyEvent& event);
    void OnClose(wxCloseEvent& event);
    void OnBrowse(wxCommandEvent& event);
    void OnBrowseRestore(wxCommandEvent& event);
    void CreateGUIControls();
    void OnTest(wxCommandEvent& event);
    void OnApply(wxCommandEvent& event);
    void CreateContentPanel();

    void OnServerChange(wxCommandEvent& event);
    void OnPortChange(wxCommandEvent& event);
    void OnDatabaseChange(wxCommandEvent& event);
    void OnUsernameChange(wxCommandEvent& event);
    void OnPasswordChange(wxCommandEvent& event);
    void OnAdvanceChange(wxCommandEvent& event);
    void OnOptionsChange(wxCommandEvent& event);
    void OnRemoteChange(wxCommandEvent& event);
    void OnSourceChange(wxCommandEvent& event);
    void OnCheckDumpChange(wxCommandEvent& event);
    void OnCheckRestoreChange(wxCommandEvent& event);
    void OnRestoreChange(wxCommandEvent& event);
    void OnClearDataDirChange(wxCommandEvent& event);

    kWookaProjectInfo* m_project;
    kWookaAppItemInfo* m_appItem;
    kWookaJvmConfigInfo* m_jvmConfig;
};


#endif /* kWookaApplicationJavaVMPanel_hpp */
