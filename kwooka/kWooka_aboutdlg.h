/********************************************************************************
*                                                                               *
* kWooka_aboutdlg.h --       The aboud dialo                                      *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/


#ifndef _KWOOKA_DIALOGS_H_
#define _KWOOKA_DIALOGS_H_


// --------------------------------------------------------------------------
// LifeAboutDialog
// --------------------------------------------------------------------------

class kWookaAboutDialog : public wxDialog
{
public:
    // ctor
    kWookaAboutDialog(wxWindow *parent);
};


#endif  // _LIFE_DIALOGS_H_
