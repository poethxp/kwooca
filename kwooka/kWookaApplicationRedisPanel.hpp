﻿/********************************************************************************
*                                                                               *
* kWookaApplicationMavenPanel.hpp -- a custom config for maven project          *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#ifndef kWookaApplicationRedisPanel_hpp
#define kWookaApplicationRedisPanel_hpp


#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/wx.h>
#include <wx/frame.h>
#else
#include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/grid.h>
#include <wx/datectrl.h>
#include "kWooka_ids.h"

////Dialog Style Start
#undef kWookaApplicationRedisPanel_STYLE
#define kWookaApplicationRedisPanel_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxMINIMIZE | wxMAXIMIZE
////Dialog Style End

class kWookaJvmConfigInfo;
class kWookaAppItemInfo;
class kWookaProjectInfo;

class kWookaApplicationRedisPanel : public wxPanel
{
private:
    DECLARE_EVENT_TABLE();

public:
    kWookaApplicationRedisPanel(wxWindow* parent, wxWindowID id = ID_PANEL_REDIS_CONFIG, const wxString& title = wxT("kWooca"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = kWookaApplicationRedisPanel_STYLE);
    virtual ~kWookaApplicationRedisPanel();


    void UpdateAppItem(kWookaProjectInfo* proj, kWookaAppItemInfo* item);
private:
    //Do not add custom control declarations between
    //GUI Control Declaration Start and GUI Control Declaration End.
    //wxDev-C++ will remove them. Add custom code after the block.
    ////GUI Control Declaration Start

    ////GUI Control Declaration End
    wxTextCtrl* m_RedisServer;
    wxTextCtrl* m_RedisPort;
    wxTextCtrl* m_RedisDatabase;
    wxTextCtrl* m_RedisAuthPassword;
    wxTextCtrl* m_RedisOptions;
    wxCheckBox* m_RedisRemote; //Use remote redis instance, we will don't to start it.
    wxTextCtrl* WxSource;  //The source is the path of redis. 
                           //If the redis is not installed in the system and wants to connect, 
                           //the host can be write in this field
    wxButton*   kBtnBrowse;
    wxButton*   kBtnTest;
    wxButton*   kBtnApply;


private:
    //Note: if you receive any error with these enum IDs, then you need to
    //change your old form code that are based on the #define control IDs.
    //#defines may replace a numeric value for the enum names.
    //Try copy and pasting the below block in your old form header files.
    enum
    {
        ////GUI Enum Control ID Start
        ID_WXBUTTON_TEST = 2041,
        ID_WXBUTTON_BROWSE = 2040,
        ID_WXBUTTON_DELETE_RECORD = 2039,
        ID_WXEDIT8 = 2029,
        ID_WXSTATICTEXT_SPACE = 2020,
        ID_WXEDIT7 = 2019,
        ID_WXSTATICTEXT_MEMBER_NAME = 2018,
        ID_WXEDIT6 = 2017,
        ID_WXSTATICTEXT_MEMBER_CARD = 2016,
        ID_WXEDIT5 = 2015,
        ID_WXSTATICTEXT_ORDERNAME = 2013,
        ID_WXEDIT4 = 2012,
        ID_WXSTATICLABEL_DATE = 2011,
        ID_WXEDIT3 = 2010,
        ID_WXEDIT2 = 2009,
        ID_WXSTATICLABEL_STATUS = 2008,
        ID_WXBUTTON_APPLY = 2006,
        ID_WXBUTTON_ADDTOCART = 2005,
        ID_WXEDIT1 = 2004,
        ID_WXSTATICLABEL_ORDERNO = 2003,
        ID_WXSTATICBOX1 = 2002,
        ID_WXGRID1 = 2001,
        ////GUI Enum Control ID End
        ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
    };

private:
    void OnKeyCharEvent(wxKeyEvent& event);
    void OnClose(wxCloseEvent& event);
    void CreateGUIControls();
    void OnTest(wxCommandEvent& event);
    void OnApply(wxCommandEvent& event);
    void OnBrowse(wxCommandEvent& event);

    void CreateContentPanel();

    void OnRedisOptionsChange(wxCommandEvent& event);
    void OnRedisPortChange(wxCommandEvent& event);
    void OnRedisDatabaseChange(wxCommandEvent& event);
    void OnRedisAuthPasswordChange(wxCommandEvent& event);
    void OnRedisRemoteChange(wxCommandEvent& event);
    void OnRedisServerChange(wxCommandEvent& event);

    kWookaJvmConfigInfo* m_jvmConfig;
    kWookaAppItemInfo*   m_appItem;
    kWookaProjectInfo*   m_project;
};


#endif /* kWookaApplicationJavaVMPanel_hpp */
