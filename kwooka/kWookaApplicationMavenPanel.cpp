﻿/********************************************************************************
*                                                                               *
* kWookaApplicationMavenPanel.cpp -- a custom config for maven project          *
*                                                                               *
* Copyright (c) Fengren Technology(Guangzhou) Co.LTD. All rights reserved.      *
*                                                                               *
********************************************************************************/

#include "kWookaApplicationMavenPanel.hpp"


#include "kWooka_ids.h"
#include "kWookaApplicationViewPanel.hpp"
#include "kWookaProjectInfo.hpp"
#include <wx/zipstrm.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>

BEGIN_EVENT_TABLE(kWookaApplicationMavenPanel, wxPanel)
////Manual Code Start
////Manual Code End
//EVT_KEY_DOWN(kPosSimpleCheckoutPanel::OnKeyCharEvent)
EVT_BUTTON(ID_WXBUTTON_ADD_RECORD, kWookaApplicationMavenPanel::OnAddRecord)
EVT_BUTTON(ID_WXBUTTON_DELETE_RECORD, kWookaApplicationMavenPanel::OnDeleteRecord)
EVT_BUTTON(ID_WXBUTTON_APPLY_RECORD, kWookaApplicationMavenPanel::OnApplyGrid)
// EVT_BUTTON(ID_WXBUTTON_BROWSE, kWookaApplicationMavenPanel::OnBrowse)
EVT_SIZING(kWookaApplicationMavenPanel::OnResize)
END_EVENT_TABLE()



kWookaApplicationMavenPanel::kWookaApplicationMavenPanel(wxWindow* parent, wxWindowID id, const wxString& WXUNUSED(title), const wxPoint& position, const wxSize& size, long style)
    : wxPanel(parent, id, position, size, style)
{
    CreateGUIControls();
}

kWookaApplicationMavenPanel::~kWookaApplicationMavenPanel()
{
    wxLogDebug(wxT("kWookaApplicationConfigPanel destroyed."));
}

void kWookaApplicationMavenPanel::CreateGUIControls()
{
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start
    // kSource = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_GroupId = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_ArtificateId = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));
    m_Version = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxPoint(140, 8), wxSize(140, 24));

    // kBtnBrowse = new wxButton(this, ID_WXBUTTON_BROWSE, _("Browse"), wxDefaultPosition, wxSize(100, 20), 0);
    // wxStaticText* lblFatJar = new wxStaticText(this, wxID_STATIC, _("SpringBoot JAR"), wxDefaultPosition, wxSize(140, 24), 0);
    // kSource->SetEditable(false);

    m_GroupId->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMavenPanel::OnGroupIdChange), NULL, this);
    m_ArtificateId->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMavenPanel::OnArtifactIdChange), NULL, this);
    m_Version->Connect(wxEVT_TEXT, wxTextEventHandler(kWookaApplicationMavenPanel::OnVersionChange), NULL, this);



    wxStaticText* lblGroupId = new wxStaticText(this, wxID_STATIC, _("Group Id"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblArtifactId = new wxStaticText(this, wxID_STATIC, _("Artifact Id"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblVersion = new wxStaticText(this, wxID_STATIC, _("Version"), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblEmpty = new wxStaticText(this, wxID_STATIC, _(""), wxDefaultPosition, wxSize(140, 24), 0);
    wxStaticText* lblEmpty2 = new wxStaticText(this, wxID_STATIC, _(""), wxDefaultPosition, wxSize(140, 24), 0);


    CreateSearchPanel();

    wxSize size = this->GetClientSize();

    WxGrid1 = new wxGrid(this, ID_WXGRID1, wxPoint(10, 370), wxSize(699, 160));
    WxGrid1->SetDefaultColSize(100);
    WxGrid1->SetDefaultRowSize(25);
    WxGrid1->SetRowLabelSize(50);
    WxGrid1->SetColLabelSize(25);
    WxGrid1->CreateGrid(0, 3, wxGrid::wxGridSelectRowsOrColumns);
    WxGrid1->SetColLabelValue(0, _("CONFIG KEY"));
    WxGrid1->SetColLabelValue(1, _("VALUE"));
    WxGrid1->SetColLabelValue(2, _("ENV VARIABLE"));
    WxGrid1->SetVirtualSize(0, 2000);



    WxGrid1->SetColSize(0, 450);
    WxGrid1->SetColSize(1, 450);
    WxGrid1->SetColSize(1, 120);
    WxGrid1->EnableEditing(true);

    //WxGrid1->SetColSize(9, 0);



    WxBtnDeleteRecord = new wxButton(this, ID_WXBUTTON_DELETE_RECORD, _("DELETE"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonDELETERECORD"));
    WxBtnAddRecord = new wxButton(this, ID_WXBUTTON_ADD_RECORD, _("ADD"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonADDRECORD"));
    WxBtnApplyRecord = new wxButton(this, ID_WXBUTTON_APPLY_RECORD, _("APPLY"), wxPoint(324, 472), wxSize(120, 33), 0, wxDefaultValidator, _T("WxButtonADDRECORD"));
    wxStaticText* lblConfigList = new wxStaticText(this, wxID_ANY, _("CONFIGURATIONS FOR STARTING THE APPLICATION"), wxPoint(20, 8), wxSize(240, 24));

    wxBoxSizer* itemBoxSizer1 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* itemBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* itemBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    // wxBoxSizer* itemBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    // wxBoxSizer* itemBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* itemBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* itemBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);

    itemBoxSizer6->Add(lblGroupId, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer6->Add(m_GroupId, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer6->Add(lblArtifactId, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer6->Add(m_ArtificateId, 1, wxEXPAND | wxALL, 5);
    
    itemBoxSizer7->Add(lblVersion, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer7->Add(m_Version, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer7->Add(lblEmpty, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer7->Add(lblEmpty2, 1, wxEXPAND | wxALL, 5);

    // itemBoxSizer5->Add(kSource, 1, wxEXPAND | wxALL, 5);
    // itemBoxSizer5->Add(kBtnBrowse, 0, wxEXPAND | wxALL, 5);

    // itemBoxSizer4->Add(lblFatJar, 0, wxEXPAND | wxALL, 5);
    // itemBoxSizer4->Add(itemBoxSizer5, 1, wxEXPAND | wxALL, 5);

    itemBoxSizer2->Add(lblConfigList, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer2->Add(WxGrid1, 1, wxEXPAND | wxALL, 5);
    itemBoxSizer3->Add(WxBtnApplyRecord, 0, wxSTRETCH_NOT | wxLEFT, 12);
    itemBoxSizer3->Add(WxBtnAddRecord, 0, wxSTRETCH_NOT | wxLEFT, 12);
    itemBoxSizer3->Add(WxBtnDeleteRecord, 0, wxSTRETCH_NOT | wxLEFT, 12);
    itemBoxSizer2->Add(itemBoxSizer3, 0, wxALIGN_RIGHT | wxRIGHT, 5);
    // itemBoxSizer1->Add(itemBoxSizer4, 0, wxEXPAND | wxALIGN_TOP | wxLEFT, 5);
    itemBoxSizer1->Add(itemBoxSizer6, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer1->Add(itemBoxSizer7, 0, wxEXPAND | wxALL, 5);
    itemBoxSizer1->Add(m_JavaVMConfigPanel, 0, wxEXPAND | wxALIGN_TOP | wxLEFT, 0);
    itemBoxSizer1->Add(itemBoxSizer2, 1, wxEXPAND | wxALL, 5);
    //itemBoxSizer1->Add(WxStaticBox2, 0, wxEXPAND | wxTOP,  5);

    this->SetSizerAndFit(itemBoxSizer1);
    //    SetTitle(_("kpos"));
    //    SetIcon(wxNullIcon);
    SetSize(8, 8, 1238, 530);
    Center();

    ////GUI Items Creation End
}

/**
    Create the Search panel
    1. Status, 2. Order No, 3. Order Name, 4. Pay Time
 */
void kWookaApplicationMavenPanel::CreateSearchPanel() {
    m_JavaVMConfigPanel = new kWookaApplicationJavaVMPanel(this);
    //wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
    //boxSizer->Add(m_JavaVMConfigPanel, 0, wxEXPAND | wxALIGN_CENTER | wxALL, 5);
}

void kWookaApplicationMavenPanel::OnKeyCharEvent(wxKeyEvent& event) {
    if (event.GetKeyCode() == WXK_RETURN) {
    }
}


void kWookaApplicationMavenPanel::OnResize(wxSizeEvent& WXUNUSED(event)) {
    wxSize size = this->GetClientSize();
    WxGrid1->SetColSize(0, 30);
    WxGrid1->SetColSize(1, size.x / 3);
    WxGrid1->SetColSize(2, size.x / 3);
    WxGrid1->SetColSize(3, 150);
}

void kWookaApplicationMavenPanel::SetViewPanel(kWookaApplicationViewPanel* vp) {
    m_viewpanel = vp;
}

kWookaApplicationViewPanel* kWookaApplicationMavenPanel::GetViewPanel() {
    return m_viewpanel;
}

void kWookaApplicationMavenPanel::UpdateAppItem(kWookaAppItemInfo* appitem) {
    m_appItem = appitem;
    if (m_appItem == NULL) {
        // this->kSource->SetValue(wxT(""));
        this->m_GroupId->SetValue(wxT(""));
        this->m_ArtificateId->SetValue(wxT(""));
        this->m_Version->SetValue(wxT(""));
        this->m_JavaVMConfigPanel->UpdateJvmConfig(NULL);
    }
    else {
        this->m_GroupId->SetValue(m_appItem->groupId);
        this->m_ArtificateId->SetValue(m_appItem->name);
        this->m_Version->SetValue(m_appItem->version);
        // this->kSource->SetValue(m_appItem->source);
        this->m_JavaVMConfigPanel->UpdateJvmConfig(&m_appItem->jvmConfig);
    }

    if (WxGrid1 != NULL) {
        if (WxGrid1->GetNumberRows() > 0) {
            WxGrid1->DeleteRows(0, WxGrid1->GetNumberRows());
        }
        if (m_appItem != NULL) {
            for (size_t t = 0; t < m_appItem->appConfigs.Count(); t++) {
                kWookaAppConfigItemInfo ci = m_appItem->appConfigs[t];
                WxGrid1->AppendRows();
                WxGrid1->SetCellValue(t, 0, ci.configKey);
                WxGrid1->SetCellValue(t, 1, ci.configValue);
                WxGrid1->SetCellValue(t, 2, wxT("env") == ci.configType ? wxT("1") : wxT(""));
                WxGrid1->SetCellEditor(t, 2, new wxGridCellBoolEditor());
                WxGrid1->SetCellRenderer(t, 2, new wxGridCellBoolRenderer());
            }
        }
    }
}

void kWookaApplicationMavenPanel::OnAddRecord(wxCommandEvent& WXUNUSED(event)) {
    if (WxGrid1 != NULL) {
        WxGrid1->AppendRows();
        WxGrid1->SetCellEditor(WxGrid1->GetNumberRows() - 1, 2, new wxGridCellBoolEditor());
        WxGrid1->SetCellRenderer(WxGrid1->GetNumberRows() - 1, 2, new wxGridCellBoolRenderer());

    }
}

void kWookaApplicationMavenPanel::OnDeleteRecord(wxCommandEvent& WXUNUSED(event)) {
    if (WxGrid1 != NULL) {
        if (WxGrid1->GetGridCursorRow() >= 0) {
            WxGrid1->DeleteRows(WxGrid1->GetGridCursorRow());
        }
    }
}

void kWookaApplicationMavenPanel::OnApplyGrid(wxCommandEvent& WXUNUSED(event)) {

    if (WxGrid1 != NULL) {
        this->m_appItem->appConfigs.Clear();
        for (int i = 0; i < WxGrid1->GetNumberRows(); i++) {
            wxString key = WxGrid1->GetCellValue(i, 0);
            wxString value = WxGrid1->GetCellValue(i, 1);
            wxString env = WxGrid1->GetCellValue(i, 2);
            if (key.Length() > 0) {
                kWookaAppConfigItemInfo configItem;
                configItem.configKey = key;
                configItem.configValue = value;
                configItem.configType = env == wxT("1") ? wxT("env") : wxT("other");
                this->m_appItem->appConfigs.Insert(configItem, 0);
            }
        }
    }
}

void kWookaApplicationMavenPanel::OnGroupIdChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->groupId = event.GetString();
        m_appItem->source = m_appItem->groupId + wxT(":") + m_appItem->name + wxT(":") + m_appItem->version;
    }
}

void kWookaApplicationMavenPanel::OnArtifactIdChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->name = event.GetString();
        m_appItem->source = m_appItem->groupId + wxT(":") + m_appItem->name + wxT(":") + m_appItem->version;
    }
}

void kWookaApplicationMavenPanel::OnVersionChange(wxCommandEvent& event) {
    if (m_appItem != NULL) {
        m_appItem->version = event.GetString();
        m_appItem->source = m_appItem->groupId + wxT(":") + m_appItem->name + wxT(":") + m_appItem->version;
    }
}


